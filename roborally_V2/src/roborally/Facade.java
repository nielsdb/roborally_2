package roborally;

import java.io.IOException;
import java.io.Writer;
import java.util.Set;

import roborally.entity.Robot;
import roborally.entity.Wall;
import roborally.entity.item.Battery;
import roborally.entity.item.EnergyItem;
import roborally.entity.item.Item;
import roborally.entity.item.RepairKit;
import roborally.entity.item.SurpriseBox;
import roborally.exception.IllegalDimensionException;
import roborally.exception.IllegalPositionException;
import roborally.exception.OutOfDimensionException;
import roborally.position.Board;
import roborally.position.Orientation;
import roborally.position.Position;
import roborally.program.exception.UnparsableException;
import roborally.value.EnergyAmount;
import roborally.value.EnergyUnit;
import roborally.value.WeightAmount;


public class Facade implements IFacade<Board, Robot, Wall, Battery, RepairKit, SurpriseBox>{

	@Override
	public Board createBoard(long width, long height) {
		try {
			return new Board(width, height);
		} catch (IllegalDimensionException e) {
			System.err.println(e.toString());
			return null;
		}
	}

	@Override
	public void merge(Board board1, Board board2) {
		board1.merge(board2);
	}

	@Override
	public Battery createBattery(double initialEnergy, int weight) {
		if(!WeightAmount.isValidNumeral(weight) || !EnergyAmount.isValidNumeral(initialEnergy))
			return null;

		if(new EnergyAmount(initialEnergy).compareTo(Battery.getMaximumEnergy()) > 0
				|| !Item.isValidWeight( new WeightAmount(weight)))
			return null;

		return new Battery(new EnergyAmount(initialEnergy), new WeightAmount(weight));
	}

	@Override
	public void putBattery(Board board, long x, long y, Battery battery) {
		if(battery.isPlacedOnABoard())
			battery.remove();

		try {
			battery.put(new Position(x,y), board);
		} catch (IllegalPositionException e) {
			System.err.println("The given position is not a valid position for this battery!");
			System.err.println(e.toString());
		} catch (OutOfDimensionException e) {
			System.err.println(e.toString());
		}
	}


	@Override
	public long getBatteryX(Battery battery) throws IllegalStateException {
		if(!battery.isPlacedOnABoard())
			throw new IllegalStateException();

		return battery.getPosition().getX();
	}

	@Override
	public long getBatteryY(Battery battery) throws IllegalStateException {
		if(!battery.isPlacedOnABoard())
			throw new IllegalStateException();

		return battery.getPosition().getY();
	}

	@Override
	public RepairKit createRepairKit(double repairAmount, int weight) {
		if(!WeightAmount.isValidNumeral(weight) || !EnergyAmount.isValidNumeral(repairAmount))
			return null;

		if(!Item.isValidWeight( new WeightAmount(weight)))
			return null;

		return new RepairKit(new EnergyAmount(repairAmount), new WeightAmount(weight));
	}

	@Override
	public void putRepairKit(Board board, long x, long y, RepairKit repairKit) {
		if(repairKit.isPlacedOnABoard())
			repairKit.remove();

		try {
			repairKit.put(new Position(x,y), board);
		} catch (IllegalPositionException e) {
			System.err.println("The given position is not a valid position for this repair kit!");
			System.err.println(e.toString());
		} catch (OutOfDimensionException e) {
			System.err.println(e.toString());
		}
	}

	@Override
	public long getRepairKitX(RepairKit repairKit) throws IllegalStateException {
		if(!repairKit.isPlacedOnABoard())
			throw new IllegalStateException();
		return repairKit.getPosition().getX();
	}

	@Override
	public long getRepairKitY(RepairKit repairKit) throws IllegalStateException {
		if(!repairKit.isPlacedOnABoard())
			throw new IllegalStateException();
		return repairKit.getPosition().getY();

	}

	@Override
	public SurpriseBox createSurpriseBox(int weight) {
		if(!WeightAmount.isValidNumeral(weight))
			return null;
		if(!Item.isValidWeight(new WeightAmount(weight)))
			return null;
		return new SurpriseBox(new WeightAmount(weight));
	}

	@Override
	public void putSurpriseBox(Board board, long x, long y,
			SurpriseBox surpriseBox) {
		if(surpriseBox.isPlacedOnABoard())
			surpriseBox.remove();

		try {
			surpriseBox.put(new Position(x,y), board);
		} catch (IllegalPositionException e) {
			System.err.println("The given position is not a valid position for this surprise box!");
			System.err.println(e.toString());
		} catch (OutOfDimensionException e) {
			System.err.println(e.toString());
		}
	}

	@Override
	public long getSurpriseBoxX(SurpriseBox surpriseBox)
			throws IllegalStateException {
		if(!surpriseBox.isPlacedOnABoard())
			throw new IllegalStateException();

		return surpriseBox.getPosition().getX();
	}

	@Override
	public long getSurpriseBoxY(SurpriseBox surpriseBox)
			throws IllegalStateException {
		if(!surpriseBox.isPlacedOnABoard())
			throw new IllegalStateException();

		return surpriseBox.getPosition().getY();
	}

	@Override
	public Robot createRobot(int orientation, double initialEnergy) {
		if(!EnergyAmount.isValidNumeral(initialEnergy))
			return null;
		if(new EnergyAmount(initialEnergy).compareTo(new EnergyAmount(20000)) > 0)
			return null;

		return new Robot(Orientation.values()[orientation], new EnergyAmount(initialEnergy));
	}

	@Override
	public void putRobot(Board board, long x, long y, Robot robot) {
		if(robot.isPlacedOnABoard())
			robot.remove();

		try {
			robot.put(new Position(x,y), board);
		} catch (IllegalPositionException e) {
			System.err.println("The given position is not a valid position for this robot!");
			System.err.println(e.toString());
		} catch (OutOfDimensionException e) {
			System.err.println(e.toString());
		}
	}

	@Override
	public long getRobotX(Robot robot) throws IllegalStateException {
		if(!robot.isPlacedOnABoard()) throw new IllegalStateException();
		return robot.getPosition().getX();
	}

	@Override
	public long getRobotY(Robot robot) throws IllegalStateException {
		if(!robot.isPlacedOnABoard()) throw new IllegalStateException();
		return robot.getPosition().getY();
	}

	@Override
	public int getOrientation(Robot robot) {
		return robot.getOrientation().ordinal();
	}

	@Override
	public double getEnergy(Robot robot) {
		return robot.getEnergy().toUnit(EnergyUnit.Ws).getNumeral();
	}

	@Override
	public void move(Robot robot) {
		if(robot.hasEnoughEnergyToMove())
			try {
				robot.move();
			} catch (OutOfDimensionException e) {
				System.err.println("The robot tried to move out of bounds!");
				System.err.println(e.toString());
			} catch (IllegalPositionException e) {
				System.err.println("The robot tried to move to a position it can't stand on!");
				System.err.println(e.toString());
			}
	}

	@Override
	public void turn(Robot robot) {
		if(robot.hasEnoughEnergyToTurn())
			robot.turn();
	}

	@Override
	public void pickUpBattery(Robot robot, Battery battery) {
		if(robot.canHaveAsPossession(battery) && robot.isPlacedOnABoard())
			robot.pickUp(battery);
	}

	@Override
	public void useBattery(Robot robot, Battery battery) {
		if(robot.hasAsPossession(battery))
			robot.use(battery);
	}

	@Override
	public void dropBattery(Robot robot, Battery battery) {
		if(robot.hasAsPossession(battery) && battery.canBePutOn(robot.getBoard(), robot.getPosition()))
			robot.drop(battery);
	}

	@Override
	public void pickUpRepairKit(Robot robot, RepairKit repairKit) {
		if(robot.canHaveAsPossession(repairKit) && robot.isPlacedOnABoard())
			robot.pickUp(repairKit);	
	}

	@Override
	public void useRepairKit(Robot robot, RepairKit repairKit) {
		if(robot.hasAsPossession(repairKit))
			robot.use(repairKit);
	}

	@Override
	public void dropRepairKit(Robot robot, RepairKit repairKit) {
		if(robot.hasAsPossession(repairKit) && repairKit.canBePutOn(robot.getBoard(), robot.getPosition()))
			robot.drop(repairKit);
	}

	@Override
	public void pickUpSurpriseBox(Robot robot, SurpriseBox surpriseBox) {
		if(robot.canHaveAsPossession(surpriseBox) && robot.isPlacedOnABoard())
			robot.pickUp(surpriseBox);
	}

	@Override
	public void useSurpriseBox(Robot robot, SurpriseBox surpriseBox) {
		if(robot.hasAsPossession(surpriseBox))
			robot.use(surpriseBox);
	}

	@Override
	public void dropSurpriseBox(Robot robot, SurpriseBox surpriseBox) {
		if(robot.canHaveAsPossession(surpriseBox) && robot.isPlacedOnABoard())
			robot.drop(surpriseBox);

	}

	@Override
	public void transferItems(Robot from, Robot to) {
		if(Board.getDistanceBetween(from, to) == 1)
			from.transferPossessionsTo(to);

	}

	@Override
	public int isMinimalCostToReach17Plus() {
		return 1;
	}

	@Override
	public double getMinimalCostToReach(Robot robot, long x, long y) {
		try {
			return robot.getEnergyRequiredToReach(new Position(x,y));
		} catch (OutOfDimensionException e) {
			System.err.println(e.toString());
			return -1;
		}
	}

	@Override
	public int isMoveNextTo18Plus() {
		return 1;
	}

	@Override
	public void moveNextTo(Robot robot, Robot other) {
		if(robot.isPlacedOnABoard() && other.isPlacedOnABoard() && robot.getBoard() == other.getBoard())
			robot.moveNextTo(other);		
	}

	@Override
	public void shoot(Robot robot) throws UnsupportedOperationException {
		if(robot.hasEnoughEnergyToShoot())
			robot.shoot();
	}

	@Override
	public Wall createWall() throws UnsupportedOperationException {
		return new Wall();
	}

	@Override
	public void putWall(Board board, long x, long y, Wall wall)
			throws UnsupportedOperationException {
		if(wall.isPlacedOnABoard())
			wall.remove();

		try {
			wall.put(new Position(x,y), board);
		} catch (IllegalPositionException e) {
			System.err.println("The given position is not a valid position for this wall!");
			System.err.println(e.toString());			
		} catch (OutOfDimensionException e) {
			e.toString();
		}
	}

	@Override
	public long getWallX(Wall wall) throws IllegalStateException,
	UnsupportedOperationException {
		if(!wall.isPlacedOnABoard()) throw new IllegalStateException();
		return wall.getPosition().getX();
	}

	@Override
	public long getWallY(Wall wall) throws IllegalStateException,
	UnsupportedOperationException {
		if(!wall.isPlacedOnABoard()) throw new IllegalStateException();
		return wall.getPosition().getY();
	}

	@Override
	public Set<Robot> getRobots(Board board) {
		return board.getAllEntitiesOfType(Robot.class);
	}

	@Override
	public Set<Wall> getWalls(Board board) throws UnsupportedOperationException {
		return board.getAllEntitiesOfType(Wall.class);
	}

	@Override
	public Set<RepairKit> getRepairKits(Board board) {
		return board.getAllEntitiesOfType(RepairKit.class);
	}

	@Override
	public Set<SurpriseBox> getSurpriseBoxes(Board board) {
		return board.getAllEntitiesOfType(SurpriseBox.class);
	}

	@Override
	public Set<Battery> getBatteries(Board board) {
		return board.getAllEntitiesOfType(Battery.class);
	}

	@Override
	public int loadProgramFromFile(Robot robot, String path) {
		try {
			robot.loadProgram(path);
			return 0;
		} catch (IOException e) {
			System.err.println(e.toString());
			return -1;
		} catch (UnparsableException e) {
			System.err.println(e.toString());
			return -1;
		}
	}

	@Override
	public int saveProgramToFile(Robot robot, String path) {
		if(!robot.hasProgram())
			return -1;
		try {
			robot.getProgram().save(path);
			return 0;
		} catch (IOException e) {
			System.err.println(e.toString());
			return -1;
		}
	}

	@Override
	public void prettyPrintProgram(Robot robot, Writer writer) {
		if(robot.hasProgram())
			try {
				writer.write(robot.getProgram().toString());
			} catch (IOException e) {
				System.err.println(e.toString());
			}
	}

	@Override
	public void stepn(Robot robot, int n) {
		if(robot.hasProgram() && n>= 0)
			robot.stepN(n);
	}


}
