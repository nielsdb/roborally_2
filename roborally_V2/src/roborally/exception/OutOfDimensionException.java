package roborally.exception;

import be.kuleuven.cs.som.annotate.Basic;

/**
 * A class for signaling that a position is not inside the dimensions of a board.
 * 
 * @version 1.0
 * @author 	Niels De Bock, Michael Vincken (Bachelor of Computer Science)
 *
 */
public class OutOfDimensionException extends Exception {

	/**
	 * Initialize this new illegal position exception with a given position.
	 * 
	 * @param 	x
	 * 			| The x-coordinate of the illegal position that caused this exception.
	 * @param 	y
	 * 			| The y-coordinate of the illegal position that caused this exception.
	 * @post	The position of this new out of dimension exception is equal to the given position.
	 * 			| new.getPosition() == position
	 */
	public OutOfDimensionException(long x, long y){
		this.x = x;
		this.y = y;
	}

	/**
	 * @return     the X-coordinate of the position associated with this illegal position exception.
	 */
	@Basic
	public long getX(){
		return x;
	}
	
	/**
	 * Variable registering the X-coordinate involved in this out of dimension exception.
	 */
	private final long x;
	
	/**
	 * @return     the Y-coordinate of the position associated with this out of dimension exception.
	 */
	@Basic
	public long getY(){
		return y;
	}
	
	/**
	 * Variable registering the Y-coordinate involved in this out of dimension exception.
	 */
	private final long y;
	
	@Override
	public String toString(){
		return "position out of bounds: (" + getX() + ", " + getY() + ")";
	}
	
	private static final long serialVersionUID =  2003001L;
	
}
