package roborally.exception;

import roborally.position.Position;
import be.kuleuven.cs.som.annotate.Basic;

/**
 * A class for signaling that the entity is not on a valid position.
 * 
 * @version 1.0
 * @author 	Niels De Bock, Michael Vincken (Bachelor of Computer Science)
 *
 */

public class IllegalPositionException extends Exception {

	/**
	 * Initialize this new illegal position exception with a given position.
	 * 
	 * @param 	position
	 * 			| The illegal position that caused this exception.
	 * @post	The position of this new illegal position exception is equal to the given position.
	 * 			| new.getPosition() == position
	 */
	public IllegalPositionException(Position position){
		this.position = position;
	}

	/**
	 * @return     the Position associated with this illegal position exception.
	 */
	@Basic
	public Position getPosition(){
		return position;
	}
	
	@Override
	public String toString(){
		return "Illegal position: " + getPosition().toString();
	}
	
	/**
	 * Variable registering the position involved in this illegal position exception.
	 */
	final Position position;

	private static final long serialVersionUID =  2003001L;
	
}
