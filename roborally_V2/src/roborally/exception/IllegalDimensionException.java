package roborally.exception;

import be.kuleuven.cs.som.annotate.Basic;

/**
 * A Class for signaling that the board does not have valid dimensions.
 * 
 * @version	1.0
 * @author 	Niels De Bock, Michael Vincken (Bachelor of Computer Science)
 * 
 */
public class IllegalDimensionException extends Exception{

	/**
	 * Initialize this new illegal dimension exception with a given value.
	 * 
	 * @param 	height
	 * 			The height for this new illegal dimension exception.
	 * @param 	width
	 * 			The width for this new illegal dimension exception.
	 * @post	The height of this new illegal dimension exception is equal to the given height.
	 * 			| new.getHeight() == height
	 * @post	The width of this new illegal dimension exception is equal to the given width.
	 * 			| new.getWidth() == width
	 */
	public IllegalDimensionException(long height, long width){
		this.height = height;
		this.width = width;
	}
	
	/**
	 * @return     the height for this illegal dimension exception.
	 */
	@Basic
	public long getHeight(){
		return height;
	}
	
	/**
	 * Variable registering the height in this illegal dimension exception.
	 */
	final long height;
	
	
	/**
	 * @return the width for this illegal dimension exception.
	 */
	@Basic
	public long getWidth(){
		return height;
	}
	
	/**
	 * Variable registering the width in this illegal dimension exception.
	 */
	final long width;
	
	@Override
	public String toString(){
		return "Illegal dimension: height: " + getHeight() + ", width: " + getWidth();
	}

	private static final long serialVersionUID = 2003001L;
	
}
