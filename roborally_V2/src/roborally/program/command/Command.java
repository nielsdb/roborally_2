package roborally.program.command;

import roborally.entity.Robot;
import roborally.program.ExecutionState;

/**
 * An abstract class for representing commands which can be executed.
 * 
 * @author 	Niels De Bock, Michael Vincken (Bachelor of Computer Science)
 * @version	1.0
 */
public abstract class Command {
	
	/**
	 * Executes this command.
	 * 
	 * @param 	robot
	 * 			The robot to execute this command.
	 * @param 	executionState
	 */
	public abstract void execute(Robot robot, ExecutionState executionState);
	
	/**
	 * Boolean reflecting is this command is executed.
	 * 
	 * @return	True if and only if this command is executed.
	 */
	public boolean isExecuted(){
		return executed;
	}
	
	/**
	 * Sets the boolean determining if this command has already been
	 * executed to the given boolean.
	 * @param 	isExecuted
	 * 			boolean holding whether this command already has been executed.
	 */
	protected void setExecuted(boolean isExecuted){
		this.executed = isExecuted;
	}
	
	/**
	 * A variable reflecting if this command has been executed.
	 */
	private boolean executed;

	/**
	 * Resets this command. 
	 */
	public abstract void reset();
}
