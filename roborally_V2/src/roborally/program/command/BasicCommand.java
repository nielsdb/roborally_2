package roborally.program.command;

import roborally.entity.Robot;
import roborally.program.ExecutionState;

/**
 * A class for representing basic commands, at this moment, there are 4 basic commands: move, turn, shoot and finally pickup-and-use.
 *
 * @author 	Niels De Bock, Michael Vincken (Bachelor of Computer Science)
 * @version	1.0
 *
 */
public abstract class BasicCommand extends Command {
	
	/**
	 * Executes this basic command.
	 * 
	 * @param 	robot
	 * 			The robot to execute this basic command with.
	 */
	public abstract void execute(Robot robot);
	
	/**
	 * Executes this basic command if it isn't already executed and if the next basic command
	 * should be executed.
	 * @effect	This basic command gets executed by the given robot.
	 * @post	The amount of steps left to do is decremented by one.
	 * @post	This command is executed.
	 */
	@Override
	public void execute(Robot robot, ExecutionState executionState){
		if(!isExecuted() && executionState.executeNext()){
			execute(robot);
			executionState.decrementStepsLeft();
			this.setExecuted(true);
		}
	}

	/**
	 * Resets the boolean containing whether this basic command has already
	 * been executed.
	 * @post	This basic command has not already been executed.
	 */
	@Override
	public void reset(){
		setExecuted(false);
	}
}
