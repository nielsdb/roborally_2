package roborally.program.command;

import roborally.entity.Robot;

/**
 * A class for representing a basic command: Shoot.
 * @author 	Niels De Bock, Michael vincken (Computer Science)
 * @version	1.0
 */
public final class Shoot extends BasicCommand {

	/**
	 * Executes this shoot command with the given robot.
	 * @param	robot
	 * 			The robot to execute this command with.
	 * @effect	The given robot shoots his laser if it has enough
	 * 			energy to do so.
	 */
	@Override
	public void execute(Robot robot) {
		if(robot.hasEnoughEnergyToShoot())
			robot.shoot();
	}

	/**
	 * Returns the string representation of this shoot command.
	 * @return the string representation of this shoot command.
	 */
	@Override
	public String toString(){
		return "SHOOT";				
	}
}
