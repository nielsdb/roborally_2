package roborally.program.command;

import roborally.entity.Robot;
import roborally.position.Direction;

/**
 * A class for representing a basic command: Turn.
 * @author 	Niels De Bock, Michael vincken (Computer Science)
 * @version	1.0
 */
public final class Turn extends BasicCommand {
	/**
	 * Initializes this turn command with a given direction.
	 * @param 	direction
	 * 			The direction to turn in.
	 * @post	The direction to turn in is equal to the given direction.
	 */
	public Turn(Direction direction){
		this.direction = direction;
	}
	
	/**
	 * Variable holding the direction of this turn command.
	 */
	private Direction direction;

	/**
	 * Executes this turn command with the given robot.
	 * @param	robot
	 * 			The robot to execute this turn command with.
	 * @effect	The given robot turns if it has enough energy to do so.
	 */
	@Override
	public void execute(Robot robot) {
		if(robot.hasEnoughEnergyToTurn())
			robot.turn(direction);
	}
	
	/**
	 * Returns the string representation of this turn command.
	 * @return the string representation of this turn command.
	 */
	@Override
	public String toString(){
		return "TURN";				
	}
}
