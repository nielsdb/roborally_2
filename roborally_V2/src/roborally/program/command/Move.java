package roborally.program.command;

import roborally.entity.Robot;
import roborally.exception.IllegalPositionException;
import roborally.exception.OutOfDimensionException;

/**
 * A class representing a basic command: move.
 * 
 * @author 	Niels De Bock, Michael vincken (Computer Science)
 * @version	1.0
 */
public final class Move extends BasicCommand {

	/**
	 * Executes this move command.
	 * @param	robot
	 * 			The robot to execute this move command with.
	 * @effect	The given robot moves one square forward if possible.
	 */
	@Override
	public void execute(Robot robot) {
			
			try {
				if(robot.hasEnoughEnergyToMove())
					robot.move();
			} catch (OutOfDimensionException e) {
			} catch (IllegalPositionException e) {
			}
	}

	/**
	 * Returns the string representation of this move command.
	 * @return the string representation of this move command.
	 */
	@Override
	public String toString(){
		return "MOVE";				
	}
}
