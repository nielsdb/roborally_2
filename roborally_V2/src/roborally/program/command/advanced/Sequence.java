package roborally.program.command.advanced;

import java.util.ArrayList;
import java.util.List;

import roborally.entity.Robot;
import roborally.program.ExecutionState;
import roborally.program.command.Command;
import be.kuleuven.cs.som.annotate.Basic;
import be.kuleuven.cs.som.annotate.Immutable;

/**
 * A class for representing a advanced command: Sequence.
 *	
 * @author 	Niels De Bock, Michael vincken (Computer Science)
 * @version	1.0
 */
public final class Sequence extends Command {
	/**
	 * Initializes this Sequence with a given list of commands.
	 * @param 	commands
	 * 			The given list of commands.
	 */
	public Sequence(List<Command> commands){
		this.commands = new ArrayList<Command>(commands);
	}

	/**
	 * Gets the list of commands in this Sequence.
	 * @return the list of commands in this Sequence.
	 */
	@Basic @Immutable
	public List<Command> getCommands() {
		return commands;
	}

	/**
	 * the list of commands in this Sequence.
	 */
	private final List<Command> commands;

	/**
	 * Executes all commands in this sequence until the next command should not
	 * be executed anymore.
	 * @post	If all commands are executed, this sequence is considered executed.
	 */
	@Override
	public void execute(Robot robot, ExecutionState executionState) {
		for(Command command : commands){
			if(!executionState.executeNext())
				break;

			command.execute(robot, executionState);

			if(command == commands.get(commands.size()-1))
				this.setExecuted(true);
		}
	}

	/**
	 * Returns a string representation of this sequence.
	 * @return a string representation of this sequence.
	 */
	@Override
	public String toString(){
		String result = "";
		for(Command command : commands){
			if(result.length() != 0)
				result += "\n";

			result += command.toString();
		}

		return result;
	}

	@Override
	public void reset() {
		for(Command command : commands){
			command.reset();
		}

		setExecuted(false);
	}

}
