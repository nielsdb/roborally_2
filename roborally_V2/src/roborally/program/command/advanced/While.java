package roborally.program.command.advanced;

import roborally.entity.Robot;
import roborally.program.ExecutionState;
import roborally.program.command.Command;
import roborally.program.condition.Condition;
import be.kuleuven.cs.som.annotate.Basic;
import be.kuleuven.cs.som.annotate.Immutable;

/**
 * A class for representing a advanced command: While.
 * 
 * @author 	Niels De Bock, Michael vincken (Computer Science)
 * @version	1.0
 */
public final class While extends Command{
	/**
	 * Initializes a new while-statement with a given condition and a given command.
	 * 
	 * @param 	condition
	 * 			The given condition.
	 * @param 	command
	 * 			The given command.
	 * @post	This while's condition is equal to the given condition.
	 * @post	This while's command is equal to the given command.
	 */
	public While(Condition condition, Command command){
		this.condition = condition;
		this.command = command;
	}

	/**
	 * Get's this while-statement's condition.
	 * @return	this while-statement's condition.
	 */
	@Basic @Immutable
	public Condition getCondition() {
		return condition;
	}

	/**
	 * Variable holding this while-statement's condition.
	 */
	private final Condition condition;

	/**
	 * Get's this while-statement's command.
	 * @return	this while-statement's command.
	 */
	@Basic @Immutable
	public Command getCommand() {
		return command;
	}

	/**
	 * Variable holding this while-statement's command.
	 */
	private final Command command;

	/**
	 * Executes this while-statement.
	 * @effect	As long as the next command should be executed and
	 * 			as long as this while's previous evaluation (if any, else making a new evaluation)
	 * 			is "TRUE" then this while-statement command gets executed.
	 * @effect	If this while-statement's command is fully executed then its condition and command gets reset.
	 */
	@Override
	public void execute(Robot robot, ExecutionState executionState) {
		if(!isExecuted()){
			while(executionState.executeNext() && 
					(
							(getCondition().isEvaluated() && getCondition().getPreviousEvaluation())
							|| (!getCondition().isEvaluated() && getCondition().evaluate(robot)))
					){
				getCommand().execute(robot, executionState);

				if(getCommand().isExecuted()){
					getCommand().reset();
					getCondition().reset();
				}
			}

			if(!getCondition().evaluate(robot) && executionState.executeNext())
				this.setExecuted(true);
		}
	}
	
	/**
	 * Returns a string representation of this while-statement.
	 * @return	a string representation of this while-statement.
	 */
	@Override
	public String toString(){
		return "While " + getCondition().toString() + "\n"
				+ "{" +	"\n"
				+ "\t" + getCommand().toString().replaceAll("\n", "\n\t") + "\n"

				+ "}";
	}

	/**
	 * Resets this while-statement.
	 * @effect	This while is no longer considered executed.
	 * @effect	This while's condition is reset.
	 */
	@Override
	public void reset() {
		setExecuted(false);
		getCondition().reset();
	}
}
