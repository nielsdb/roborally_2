package roborally.program.command;

import roborally.entity.Robot;
import roborally.entity.item.Battery;
import roborally.entity.item.Item;
import roborally.entity.item.RepairKit;
import roborally.entity.item.SurpriseBox;

/**
 * A class representing a basic command: Pick up and use.
 * 
 * @author 	Niels De Bock, Michael vincken (Computer Science)
 * @version	1.0
 */
public class PickupAndUse extends BasicCommand {

	/**
	 * Executes this pickup-and-use command with the given robot.
	 * @param	robot
	 * 			The robot to execute this pickup-and-use command with.
	 * @effect	The given robot picks up a random item on the same
	 * 			location (if any) as the robot and uses it.
	 */
	@Override
	public void execute(Robot robot) {
		if(robot.getBoard().getNbItemsOnPosition(robot.getPosition()) == 0)
			return;

		Item item = robot.getBoard().getItemsOnPosition(robot.getPosition()).get(0);
		robot.pickUp(item);
		if(item instanceof Battery)
			robot.use((Battery) item);
		if(item instanceof RepairKit)
			robot.use((Battery) item);
		if(item instanceof SurpriseBox)
			robot.use((SurpriseBox) item);
	}

	/**
	 * Returns the string representation of this pickup-and-use command.
	 * @return the string representation of this pickup-and-use command.
	 */
	@Override
	public String toString(){
		return "PICKUP AND USE";				
	}
}
