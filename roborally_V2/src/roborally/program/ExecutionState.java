package roborally.program;

/**
 * A class for representing the execution state of a program.
 * 
 * @author 	Niels De Bock, Michael Vincken (Bachelor of Computer Science)
 * @version	1.0
 */
public class ExecutionState {
	
	/**
	 * Initializes this execution state with an amount of steps to do before finishing the program.
	 * 
	 * @param 	stepsToDo
	 * 			The amount of steps to do before finishing the program.
	 * 		 	At the beginning, the amount of steps left is equal to the amount of steps to do.
	 */
	public ExecutionState(int stepsToDo){
		stepsLeft = stepsToDo;
	}
	
	/**
	 * A variable registering how many steps there are left to do in the program.
	 */
	private int stepsLeft;
	
	/**
	 * Returns a boolean reflecting if the next command should be executed.
	 * 
	 * @return 	True and only true if steps left is larger than zero.
	 */
	public boolean executeNext(){
		return (stepsLeft > 0);
	}
	
	/**
	 * Decrease the steps left to do with one.
	 */
	public void decrementStepsLeft(){
		stepsLeft--;
	}
	
}
