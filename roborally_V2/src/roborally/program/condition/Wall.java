package roborally.program.condition;

import roborally.entity.Robot;
import roborally.exception.OutOfDimensionException;

/**
 * A class for representing a program condition: Wall.
 * @author 	Niels De Bock, Michael vincken (Computer Science)
 * @version	1.0
 */
public final class Wall extends Condition {
	
	/**
	 * Returns true if there is a wall located to the right of the given robot.
	 * @param	robot
	 * 			The given robot.
	 * @return true if there is a wall located to the right of the given robot.
	 */
	@Override
	public boolean evaluate(Robot robot) {
		try {
			return(robot.getBoard().getNbWallsOnPosition(Robot.getPositionAfterMove(robot.getPosition(), robot.getOrientation().getOrientationAfterXTurns(1))) >= 1);
		} catch (OutOfDimensionException e) {
			return false;
		}
	}

	/**
	 * Returns a string representation of this condition.
	 */
	@Override
	public String toString(){
		return "wall";				
	}
}
