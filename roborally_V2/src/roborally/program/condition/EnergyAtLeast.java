package roborally.program.condition;

import roborally.entity.Robot;
import roborally.value.EnergyAmount;
import roborally.value.EnergyUnit;
import be.kuleuven.cs.som.annotate.Basic;
import be.kuleuven.cs.som.annotate.Immutable;

/**
 * A class for representing a program condition: Energy at least.
 * 
 * @author 	Niels De Bock, Michael vincken (Computer Science)
 * @version	1.0
 */
public final class EnergyAtLeast extends Condition {
	/**
	 * Initializes this condition with a given minimal energy.
	 * 
	 * @param 	minimalEnergy
	 * 			The given minimal energy.	
	 * @post	This EnergyAtLeasy-condition's minimal energy is equal to the given minimal energy.	
	 */
	public EnergyAtLeast(Double minimalEnergy){
		this.minimalEnergy = minimalEnergy;
	}
	
	/**
	 * Gets the minimal energy a robot would need for this condition to evaluate to true.
	 * 
	 * @return	the minimal energy a robot would need for this condition to evaluate to true.
	 */
	@Basic @Immutable
	public double getMinimalEnergy() {
		return minimalEnergy;
	}
	
	/**
	 * A variable referencing the minimal energy a robot would need for this condition to evaluate to true.
	 */
	private final double minimalEnergy;
	
	/**
	 * Evaluates this condition.
	 * @param	robot
	 * 			The robot to evaluate this condition with.
	 * @return	True if and only if the given robot's energy is higher than
	 * 			this energy-at-least-condition's minimal energy.
	 */
	@Override
	public boolean evaluate(Robot robot) {
		return (robot.getEnergy().toUnit(EnergyUnit.Ws).compareTo(new EnergyAmount(minimalEnergy)) >= 0);
	}

	/**
	 * Returns a string representation of this condition.
	 */
	@Override
	public String toString(){
		return "energy at least " + getMinimalEnergy();				
	}
}
