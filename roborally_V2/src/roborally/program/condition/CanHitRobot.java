package roborally.program.condition;

import roborally.entity.Robot;
import roborally.exception.OutOfDimensionException;
import roborally.position.Position;

/**
 * A class for representing a program condition: Can hit robot.
 * 
 * @author 	Niels De Bock, Michael vincken (Computer Science)
 * @version	1.0
 */
public final class CanHitRobot extends Condition {
	/**
	 * Evaluates this condition.
	 * @param	robot
	 * 			The robot to evaluate this condition with.
	 * @return	True if and only if this robot would hit another robot if he would shoot with his laser.
	 */
	@Override
	public boolean evaluate(Robot robot) {
		
		Position nextPosition;
		try {
			nextPosition = robot.getPositionAfterMove();
		} catch (OutOfDimensionException e) {
			return false;
		}

		while(robot.getBoard().getNbRobotsOnPosition(nextPosition) == 0){
			try {
				if(robot.getBoard().isInsideDimensions(Robot.getPositionAfterMove(nextPosition, robot.getOrientation())))
					nextPosition = Robot.getPositionAfterMove(nextPosition, robot.getOrientation());
				else
					return false;
			} catch (OutOfDimensionException e) {
				return false;
			}
		}
		return true;
	}
	
	/**
	 * Returns a string representation of this condition.
	 */
	@Override
	public String toString(){
		return "can hit robot";				
	}

}
