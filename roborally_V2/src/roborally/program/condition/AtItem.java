package roborally.program.condition;

import roborally.entity.Robot;

/**
 * A class for representing a program condition: At item.
 * 
 * @author 	Niels De Bock, Michael vincken (Computer Science)
 * @version	1.0
 */
public final class AtItem extends Condition {

	/**
	 * Evaluates this condition.
	 * @param	robot
	 * 			The robot to evaluate this condition with.
	 * @return	True if and only if the number of items located on this robot's board
	 * 			at this robot's position is greater than 0.
	 */
	@Override
	public boolean evaluate(Robot robot) {
		return (robot.getBoard().getNbItemsOnPosition(robot.getPosition()) > 0);
	}

	/**
	 * Returns a string representation of this condition.
	 */
	@Override
	public String toString(){
		return "at item";				
	}
	
}
