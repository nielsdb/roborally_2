package roborally.program.condition;

import roborally.entity.Robot;

/**
 * A class for representing a program condition.
 * 
 * @author 	Niels De Bock, Michael vincken (Computer Science)
 * @version	1.0
 */
public abstract class Condition {
	/**
	 * Evaluates this condition.
	 * @param 	robot
	 * 			The robot to evaluate this condition.
	 * @return	True and only true if this robot can evaluate this condition.
	 */
	public abstract boolean evaluate(Robot robot);

	/**
	 * Gets the previous evaluation of this condition.
	 * 
	 * @pre 	This condition has already been evaluated.
	 * @return	This condition's previous evaluation.
	 */
	public boolean getPreviousEvaluation(){
		return previousEvaluation;
	}

	/**
	 * Sets the previous evaluation to the given evaluation.
	 * @param 	evaluation
	 * 			The given evaluation.
	 */
	protected void setPreviousEvaluation(boolean evaluation){
		previousEvaluation = evaluation;
	}

	/**
	 * A boolean reflecting this condition's previous evaluation.
	 */
	private boolean previousEvaluation;

	/**
	 * Sets whether this condition has already been evaluated to the given boolean.
	 * @param 	evaluated
	 * 			The given boolean, reflecting whether this condition should be considered
	 * 			as "already evaluated".
	 */
	protected void setEvaluated(boolean evaluated){
		this.evaluated = evaluated;
	}
	
	/**
	 * A boolean reflecting if this condition should be considered as previously been evaluated.
	 * @return	True and only true if this condition is considered as previously evaluated.
	 */
	public boolean isEvaluated(){
		return evaluated;
	}
	
	/**
	 * A boolean reflecting whether this condition is considered as previously evaluated.
	 */
	private boolean evaluated = false;
	
	/**
	 * Resets this condition, in other words, this condition
	 * is considered not previously evaluated.
	 */
	public void reset(){
		evaluated = false;
	}

}