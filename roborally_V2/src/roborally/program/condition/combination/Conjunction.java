package roborally.program.condition.combination;

import roborally.entity.Robot;
import roborally.program.condition.Condition;
import be.kuleuven.cs.som.annotate.Basic;
import be.kuleuven.cs.som.annotate.Immutable;

/**
 * A class for representing a conjunction of two conditions.
 *
 * @author 	Niels De Bock, Michael vincken (Computer Science)
 * @version	1.0
 */
public final class Conjunction extends Condition{
	/**
	 * Initializes a new conjunction with 2 given conditions.
	 * 
	 * @param 	firstCondition
	 * 			The given first condition.
	 * @param 	secondCondition
	 * 			The given second condition.	
	 * @post	This conjunctions first condition is equal to the given first condition.
	 * @post	This conjunctions second condition is equal to the given second condition.
	 * @throws	IllegalArgumentException
	 * 			Non-effective condition(s)
	 */
	public Conjunction(Condition firstCondition, Condition secondCondition){
		if(firstCondition == null)
			throw new IllegalArgumentException("Non-effective first condition!");
		if(secondCondition == null)
			throw new IllegalArgumentException("Non-effective second condition!");
		this.firstCondition = firstCondition;
		this.secondCondition = secondCondition;
	}
	
	/**
	 * Gets the first condition of this conjunction.
	 * 
	 * @return	The first condition.
	 */
	@Basic @Immutable
	public Condition getFirstCondition() {
		return firstCondition;
	}
	
	/**
	 * Gets the second condition of this conjunction.
	 * 
	 * @return	The second condition.
	 */
	@Basic @Immutable
	public Condition getSecondPosition() {
		return secondCondition;
	}
	
	/**
	 * A variable referencing the first condition.
	 */
	private final Condition firstCondition;
	
	/**
	 * A variable referencing the second condition.
	 */
	private final Condition secondCondition;
	
	/**
	 * Evaluates to true if and only if both of this conjunction's conditions are evaluated to true.
	 * 
	 * @param	robot
	 * 			The given robot to evaluate both conditions with.
	 * @return	True if and only if both of this conjunction's conditions are evaluated to true.
	 */
	@Override
	public boolean evaluate(Robot robot) {
		return firstCondition.evaluate(robot) && secondCondition.evaluate(robot);
	}
	
	/**
	 * Returns a string representation of this conjunction.
	 */
	@Override
	public String toString(){
		return "( " + firstCondition.toString() + " AND " + secondCondition.toString() + " )";
	}

}
