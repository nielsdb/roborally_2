package roborally.program.condition.combination;

import roborally.entity.Robot;
import roborally.program.condition.Condition;
import be.kuleuven.cs.som.annotate.Basic;
import be.kuleuven.cs.som.annotate.Immutable;

/**
 * A class for representing a negation of a conditions.
 * 
 * @author 	Niels De Bock, Michael vincken (Computer Science)
 * @version	1.0
 */
public final class Negation extends Condition{
	/**
	 * Initializes a negation with a given condition.
	 * @param 	condition
	 * 			The given program condition.
	 * @post	This negation's condition is equal to the given condition.
	 * @throws	IllegalArgumentException
	 * 			Non-effective condition.
	 */
	public Negation(Condition condition){
		if(condition == null)
			throw new IllegalArgumentException("Non-effective first condition!");
		this.condition = condition;
	}

	/**
	 * Gets this negation's condition.
	 * 
	 * @return	this negation's condition.
	 */
	@Basic @Immutable
	public Condition getCondition() {
		return condition;
	}
	
	/**
	 * A variable holding this negation's condition.
	 */
	private final Condition condition;
	
	/**
	 * Evaluates this negation.
	 * 
	 * @param	robot
	 * 			The given robot to evaluate this negation.	
	 * @return	evaluates to true if the condition of this evaluation evaluates to false.
	 */
	@Override
	public boolean evaluate(Robot robot) {
		return !condition.evaluate(robot);
	}
	
	/**
	 * Returns a string representation of this negation.
	 */
	@Override
	public String toString(){
		return "( NOT " +  condition.toString() + " )";
	}

}
