package roborally.program.condition;

import roborally.entity.Robot;

/**
 * A class for representing a program condition: True
 * @author 	Niels De Bock, Michael vincken (Computer Science)
 * @version	1.0
 */
public final class True extends Condition{

	/**
	 * Evaluates this condition.
	 * @param	robot
	 * 			The robot to evaluate this condition with.
	 * @return 	true.
	 */
	@Override
	public boolean evaluate(Robot robot) {
		return true;
	}

	/**
	 * A string representation of this condition.
	 */
	@Override
	public String toString(){
		return "true";				
	}
}
