package roborally.program;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import roborally.entity.Robot;
import roborally.position.Direction;
import roborally.program.command.Command;
import roborally.program.command.Move;
import roborally.program.command.PickupAndUse;
import roborally.program.command.Shoot;
import roborally.program.command.Turn;
import roborally.program.command.advanced.If;
import roborally.program.command.advanced.Sequence;
import roborally.program.command.advanced.While;
import roborally.program.condition.AtItem;
import roborally.program.condition.CanHitRobot;
import roborally.program.condition.Condition;
import roborally.program.condition.EnergyAtLeast;
import roborally.program.condition.True;
import roborally.program.condition.Wall;
import roborally.program.condition.combination.Conjunction;
import roborally.program.condition.combination.Disjunction;
import roborally.program.condition.combination.Negation;
import roborally.program.exception.UnknownCommandException;
import roborally.program.exception.UnknownConditionException;
import be.kuleuven.cs.som.annotate.Basic;
import be.kuleuven.cs.som.annotate.Immutable;
import be.kuleuven.cs.som.annotate.Model;

/**
 * @author 	Niels De Bock, Michael vincken (Computer Science)
 * @version	1.0
 *
 */
public class Program {
	/**
	 * Initializes a new program with a given robot and a given path.
	 * @param 	path
	 * 			The path to the file containing the program
	 * @post	The command this program will execute is equal to the
	 * 			command parsed from the file on the given path.
	 * @throws 	IOException
	 * 			There was a problem reading the file.
	 * @throws 	UnknownCommandException
	 * 			There was a problem parsing a certain command.
	 * @throws 	UnknownConditionException
	 * 			There was a problem parsing a certain condition.
	 */
	public Program(Robot robot, String path) throws IOException, UnknownCommandException, UnknownConditionException{
		this.originalSource = getContents(new File(path));
		this.command = parse(originalSource);
		}

	/**
	 * A method which returns the original source code of this program.
	 * @return A list of strings, each string represents one line of the original source.
	 */
	private List<String> getOriginalSource() {
		return originalSource;
	}
	
	/**
	 * A variable referencing the original source (code) of this program.
	 */
	private final List<String> originalSource;
	
	/**
	 * Returns the command.
	 * 
	 * @return the command.
	 */
	@Basic @Immutable
	public Command getCommand() {
		return command;
	}

	/**
	 * A variable referencing the command of this program.
	 */
	private final Command command;

	/**
	 * Checks whether this program can be executed by the given robot.
	 * 
	 * @param 	robot
	 * 			The robot which possesses this program.
	 * @return	true and only true if the given robot can execute this program, in other words
	 * 			when the robot is placed on a board and the robot is not terminated.
	 */
	public static boolean canBeExecutedBy(Robot robot){
		return robot != null && robot.isPlacedOnABoard() && !robot.isTerminated();
	}

	/**
	 * Executes the first unexecuted basic command.
	 * @pre	The given robot can executed a program.
	 */
	public void step(Robot robot){
		assert(Program.canBeExecutedBy(robot));
		command.execute(robot, new ExecutionState(1));
	}


	/**
	 * Parses a given list of strings (representing the read program) and returns the command of the program.
	 * 
	 * @param 	contents
	 * 			The given list of contents containing the read program.
	 * @return	The command this program should execute.
	 * @throws 	UnknownCommandException 
	 * 			There was a problem parsing a certain command.
	 * @throws 	UnknownConditionException 
	 * 			There was a problem parsing a certain condition.
	 */
	public static Command parse(List<String> contents) throws UnknownCommandException, UnknownConditionException{

		String program = trim(contents);

		return parseCommand(getSubStringsBetweenMatchingBrackets(program).get(0));
	}

	/**
	 * Returns the strings between matching brackets.
	 * 
	 * @param 	string
	 * 			The string to check for matching brackets.
	 * @return	the substrings between matching brackets.
	 */
	private static List<String> getSubStringsBetweenMatchingBrackets(String string){
		List<String> subStrings = new ArrayList<String>();

		boolean outsideBracket = true;
		int beginIndex = 0;
		int openBrackets = 0;

		for(int i = 0; i < string.length(); i++){
			if(string.charAt(i) == '('){
				openBrackets++;
				if(outsideBracket){
					beginIndex = i;
					outsideBracket = false;
				}
			}
			else if(string.charAt(i) == ')'){
				openBrackets--;
			}

			if(openBrackets == 0 && !outsideBracket){
				subStrings.add(string.substring(beginIndex, i+1));				
				outsideBracket = true;
			}
		}
		return subStrings;
	}

	/**
	 * Parses a command from the given string.
	 * @param 	command
	 * 			The command to parse.
	 * @return	The parsed command.
	 * @throws 	UnknownCommandException
	 * 			The given command is not recognized.
	 * @throws 	UnknownConditionException
	 * 			A condition in the given command is not recognized.
	 */
	private static Command parseCommand(String command) throws UnknownCommandException, UnknownConditionException{
		Pattern p = Pattern.compile("^ ?\\( ?([-a-z]+) ?([a-z]+)?");
		Matcher m = p.matcher(command);

		if (m.find()) {
			if(m.group(1).equals("move")){
				return new Move();
			}
			else if(m.group(1).equals("turn")){
				Direction direction = m.group(2)
						.equals("clockwise") ? Direction.CLOCKWISE : Direction.COUNTERCLOCKWISE;
				return new Turn(direction);
			}
			else if(m.group(1).equals("shoot")){
				return new Shoot();
			}
			else if(m.group(1).equals("pickup-and-use")){
				return new PickupAndUse();
			}
			else if(m.group(1).equals("seq")){
				List<String> arguments = getSubStringsBetweenMatchingBrackets(command.substring(1, command.length()-1));
				List<Command> commands = new ArrayList<Command>();

				for(String argument : arguments)
					commands.add(parseCommand(argument));

				return new Sequence(commands);

			}
			else if(m.group(1).equals("while")){

				List<String> arguments = getSubStringsBetweenMatchingBrackets(
						command.substring(1, command.length()-1));

				return new While(parseCondition(arguments.get(0)), parseCommand(arguments.get(1)));
			}
			else if(m.group(1).equals("if")){

				List<String> arguments = getSubStringsBetweenMatchingBrackets(command.substring(1, command.length()-1));
				
				return new If(parseCondition(arguments.get(0)), parseCommand(arguments.get(1)), parseCommand(arguments.get(2)));
			}
			else {
				throw new UnknownCommandException(m.group(1));
			}
		}
		else
		{
			throw new UnknownCommandException(command);
		}
	}

	/**
	 * Parses a condition from the given string.
	 * @param 	command
	 * 			The command to parse.
	 * @return	The parsed condition.
	 * @throws 	UnknownConditionException
	 * 			The given condition is not recognized.
	 */
	private static Condition parseCondition(String command) throws UnknownConditionException{
		Pattern p = Pattern.compile("^ ?\\( ?([-a-z]+) ?(\\d+)?");
		Matcher m = p.matcher(command);

		if (m.find()) {
			if(m.group(1).equals("true")){
				return new True();
			}
			else if(m.group(1).equals("energy-at-least")){
				return new EnergyAtLeast(Double.parseDouble(m.group(2)));
			}
			else if(m.group(1).equals("at-item")){
				return new AtItem();
			}
			else if(m.group(1).equals("can-hit-robot")){
				return new CanHitRobot();
			}
			else if(m.group(1).equals("wall")){
				return new Wall();
			}
			else if(m.group(1).equals("and")){
				List<String> arguments = getSubStringsBetweenMatchingBrackets(command.substring(1, command.length()-1));

				return new Conjunction(parseCondition(arguments.get(0)), parseCondition(arguments.get(1)));
			}
			else if(m.group(1).equals("or")){
				List<String> arguments = getSubStringsBetweenMatchingBrackets(command.substring(1, command.length()-1));

				return new Disjunction(parseCondition(arguments.get(0)), parseCondition(arguments.get(1)));
			}
			else if(m.group(1).equals("not")){
				List<String> arguments = getSubStringsBetweenMatchingBrackets(command.substring(1, command.length()-1));

				return new Negation(parseCondition(arguments.get(0)));
			}
			else
			{
				throw new UnknownConditionException(m.group(1));
			}
		}
		else
		{
			throw new UnknownConditionException(command);
		}
	}


	/**
	 * Remove tabs, newlines and unnecessary spaces. Also concatenates all strings in the given list in one string.
	 * Also makes the string lower case.
	 * @param 	contents
	 * 			The given list containing strings.
	 * @return	A string containing all strings in the given list of strings, stripped from tabs, newlines and unnecessary spaces.
	 */
	private static String trim(List<String> contents){
		String programOneLine = "";
		
		for(String line : contents){
			line = line.replaceAll("( +)", " ");
			programOneLine += line.replaceAll("(\\r|\\n|\\t)", "");
		}


		return programOneLine.toLowerCase();
	}

	/**
	 * Reads the contents from the given file.
	 * @param 	program
	 * 			The program to read.
	 * @return	An ArrayList of strings, each containing one line of the program.
	 * @throws 	IOException 
	 * 			An error occurred while reading the file.
	 */
	private static List<String> getContents(File program) throws IOException {
		List<String> contents = new ArrayList<String>();

		//use buffering, reading one line at a time
		//FileReader always assumes default encoding is OK!
		BufferedReader input =  new BufferedReader(new FileReader(program));
		try {
			String line = null; //not declared within while loop
			/*
			 * readLine is a bit quirky :
			 * it returns the content of a line MINUS the newline.
			 * it returns null only for the END of the stream.
			 * it returns an empty String if two newlines appear in a row.
			 */
			while (( line = input.readLine()) != null){
				contents.add(line);
			}
		}
		finally {
			input.close();
		}

		return contents;
	}
	/**
	 * Saves this program.
	 * 
	 * @param 	path
	 * 			The path to the file containing the program to save.
	 * @throws 	IOException
	 * 			An error occurred while saving the file.
	 * 
	 */
	public void save(String path) throws IOException{
		    BufferedWriter out = new BufferedWriter(new FileWriter(path));
		    for(String line : getOriginalSource())
		    	out.write(line + "\n");
		    out.close();
	}
	
	@Override
	public String toString(){
		return command.toString();	
	}
}
