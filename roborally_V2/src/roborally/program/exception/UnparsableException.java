package roborally.program.exception;

/**
 * 
 * @author 	Niels De Bock, Michael vincken (Computer Science)
 * @version	1.0
 */
public class UnparsableException extends Exception {
	public UnparsableException(String command) {
		this.command = command;
	}
	
	public String getCommand() {
		return command;
	}
	
	private final String command;
	
	@Override
	public String toString(){
		return "Unparsable: " + getCommand();
	}
	
	private static final long serialVersionUID = 2003001L;

}
