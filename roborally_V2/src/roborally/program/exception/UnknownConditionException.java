package roborally.program.exception;

/**
 * 
 * @author 	Niels De Bock, Michael vincken (Computer Science)
 * @version	1.0
 */
public class UnknownConditionException extends UnparsableException {
	public UnknownConditionException(String condition) {
		super(condition);
	}

	private static final long serialVersionUID = 1L;

	@Override
	public String toString(){
		return "Unknown condition: " + getCommand();
	}	
}
