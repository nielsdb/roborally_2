package roborally.program.exception;

/**
 * @author 	Niels De Bock, Michael vincken (Computer Science)
 * @version	1.0
 *
 */
public class UnknownCommandException extends UnparsableException {

	private static final long serialVersionUID = 1L;

	public UnknownCommandException(String command) {
		super(command);
	}

	@Override
	public String toString(){
		return "Unknown command: " + getCommand();
	}

}
