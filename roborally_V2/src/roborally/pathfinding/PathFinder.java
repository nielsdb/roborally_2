package roborally.pathfinding;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import roborally.entity.Robot;
import roborally.exception.OutOfDimensionException;
import roborally.position.Board;
import roborally.position.Orientation;
import roborally.position.Position;
import roborally.value.EnergyAmount;
import be.kuleuven.cs.som.annotate.Basic;
import be.kuleuven.cs.som.annotate.Model;

/**
 * A class representing a PathFinder with the ability to compute the minimal energy required to reach a position and the
 * most efficient path to reach this position.
 * 
 * @invar		| canHaveAsRobot(getRobot())
 * @version     1.0
 * @author      Niels De Bock, Michael Vincken (Bachelor of Computer Science)
 * @note		The basic structure of this class is taken from http://www.java-gaming.org/index.php?topic=25158.0,
 * 				written by "Riven".
 */
public class PathFinder {
	/**
	 * Initialize this PathFinder with a given robot.
	 * 
	 * @param       robot
	 *              The robot to use as reference when searching a path.
	 * @pre         The given robot must be placed on a board.
	 *              | canHaveAsRobot(robot)
	 * @post        | new.getBoard() == robot.getBoard()
	 * @post        | new.getRobot() == robot

	 */
	public PathFinder(Robot robot)
	{
		assert(canHaveAsRobot(robot)) : "Precondition: The given robot must be placed on a board.";
		this.robot = robot;
		this.FRONTLINE = new TreeSet<Node>(new NodeComparator(this));
	}

	/**
	 * 
	 * @return | frontline.size()
	 */
	@Model
	private int getNbFrontline(){
		return FRONTLINE.size();
	}

	/**
	 * The frontline of the current search.
	 */
	private final TreeSet<Node> FRONTLINE;

	/**
	 * 
	 * @return | result == robot.getBoard()
	 */
	@Model
	private Board getBoard(){
		return robot.getBoard();
	}

	/**
	 * 
	 * @return | result == robot
	 */
	@Model
	private Robot getRobot(){
		return robot;
	}

	/**
	 * 
	 * @param 	robot
	 * 			The robot to check
	 * @return	| if(isTerminated()) then
	 *			|	result == (robot == null)
	 * @return	| result == (robot != null && robot.isPlacedOnABoard())
	 */
	private boolean canHaveAsRobot(Robot robot){
		if(isTerminated())
			return (robot == null);
		return(robot != null && robot.isPlacedOnABoard());
	}
	
	/**
	 * The robot to use as reference when searching a path.
	 */
	private Robot robot;

	/**
	 * Retrieves the minimal energy required to reach the given position.
	 * 
	 * @param       position
	 *              	The given position.
	 * @return      | let leastEnergy = new EnergyAmount(Double.MAX_VALUE)
	 *              | for each orientation in Orientation
	 *              |       if(leastEnergy.compareTo(getAccumulatedWeight(new Node(position, orientation))) > 0) then
	 *              |               leastEnergy = getAccumulatedWeight(new Node(position, orientation))
	 *              | result == leastEnergy
	 */
	public EnergyAmount getEnergyRequiredToReach(Position position)
	{
		EnergyAmount leastEnergy = new EnergyAmount(Double.MAX_VALUE);
		for(int i = 0; i<= 3; i++){
			if(getAccumulatedWeight(new Node(position, Orientation.values()[i])).compareTo(leastEnergy) < 0)
				leastEnergy = getAccumulatedWeight(new Node(position, Orientation.values()[i]));
		}
		return leastEnergy;
	}


	/**
	 * 
	 * @param       endPosition
	 *                      The position to end the search on
	 * @return      The final node found after a search to the given position.
	 */
	public void search(Position endPosition){
		Node startNode = new Node(getRobot().getPosition(), getRobot().getOrientation());
		search(startNode, endPosition);
	}

	/**
	 * 
	 * @param       startNode
	 *              The node to start the search at.
	 * @param       target
	 * 	            The position at which to stop the search.
	 * @return      The final node found after a search to the given position.
	 */
	private void search(Node startNode, Position target){
		reset();
		this.FRONTLINE.add(startNode);
		setAccumulatedWeight(startNode, new EnergyAmount(0));

		while(!this.FRONTLINE.isEmpty())
		{
			this.step(target);

			if(!getVisitedNodesAtPosition(target).isEmpty())
			{
				return;
			}
		}
	}

	public void fullSearch(){
		fullSearch(new EnergyAmount(Double.MAX_VALUE));
	}


	public void fullSearch(EnergyAmount cutOff){
		Node startNode = new Node(robot.getPosition(), robot.getOrientation());
		fullSearch(startNode, cutOff);
	}

	/**
	 * 
	 * @param       startNode
	 * 				The node to start the full search on.
	 */
	private void fullSearch(Node startNode, EnergyAmount cutOff){
		reset();
		this.FRONTLINE.add(startNode);
		setAccumulatedWeight(startNode, new EnergyAmount(0));

		while(!this.FRONTLINE.isEmpty() && !(getAccumulatedWeight(FRONTLINE.first()).compareTo(cutOff) > 0))
		{
			this.step();
		}
	}


	private void step()
	{
		Node head = this.FRONTLINE.pollFirst(); //Retrieves and removes the first (lowest) element

		this.visit(head);

		//create edges from head
		generateEdges(head);

		for (Edge edge : head.getEdges())
		{
			if (this.isVisited(edge.getDestination()))
				continue;

			// compare known (if any) weight and newly discovered weight
			EnergyAmount last = this.getAccumulatedWeight(edge.getDestination());
			EnergyAmount curr = this.getAccumulatedWeight(head).add(edge.getWeight());

			if (curr.compareTo(last) < 0){
				// found (faster?) path to node
				this.FRONTLINE.remove(edge.getDestination());
				this.setAccumulatedWeight(edge.getDestination(), curr);
				this.FRONTLINE.add(edge.getDestination());
			}
		}
	}

	/**
	 * 
	 * @param       target
	 * 				To position to step to.
	 */
	private void step(Position target)
	{
		Node head = this.FRONTLINE.pollFirst(); //Retrieves and removes the first (lowest) element

		this.visit(head);
		if (head.getPosition().equals(target))
			return;


		//create edges from head
		generateEdges(head, target);

		for (Edge edge : head.getEdges())
		{
			if (this.isVisited(edge.getDestination()))
				continue;

			// compare known (if any) weight and newly discovered weight
			EnergyAmount last = this.getAccumulatedWeight(edge.getDestination());
			EnergyAmount curr = this.getAccumulatedWeight(head).add(edge.getWeight());

			if (curr.compareTo(last) < 0){
				// found (faster?) path to node
				this.FRONTLINE.remove(edge.getDestination());
				this.setAccumulatedWeight(edge.getDestination(), curr);
				this.FRONTLINE.add(edge.getDestination());
			}
		}
	}

	/**
	 * Generates edges from the given (head)node, with the option of ignoring bounds.
	 * @param       node
	 *              The node to generate edges from.
	 * @param       ignoreBounds
	 *              boolean defining whether this method should ignore the bounds of the positions in use
	 *              on the board.
	 * @pre         | node != null
	 * @post        The given node has edges going to nodes of the same position, but different orientation.
	 *                      | for each orientation in Orientation
	 *                      |       if (orientation != node.getOrientation())
	 *                      |               new.node.hasAsEdge(new Edge(node.getPosition(), orientation))
	 * @post        The given node has an edge to the node in front of this robot if deemed useful (see formal).
	 *                      | let positionInFront = Robot.getPositionAfterMove(node.getPosition(), node.getOrientation())
	 *                      |       if(robot.canBePutOn(positionInFront) && 
	 *                      |               (ignoreBounds || (!ignoreBounds && isInsideBounds(positionInFront))))
	 *                      |               new.node.hasAsEdge(new Edge(node.getPosition(), orientation))
	 */
	private void generateEdges(Node node, boolean ignoreBounds) {
		assert(node != null);

		try {
			Position positionInFront = Robot.getPositionAfterMove(node.getPosition(), node.getOrientation());
			if(getRobot().canBePutOn(positionInFront)){
				Node nodeInFront = new Node(positionInFront, node.getOrientation());

				if(ignoreBounds){
					new Edge(node, nodeInFront, getRobot().getEnergyRequiredToMove());
				}
				else
				{
					if(getBoard().isInsideBounds(positionInFront)){
						new Edge(node, nodeInFront, getRobot().getEnergyRequiredToMove());
					}
				}
			}
		} catch (OutOfDimensionException e) {
			//This is no problem, we simply do not need to make an edge to this position.
		}

		//generate nodes to all nodes on the same position, but with another orientation
		for(int i = 1; i <= 3; i++){
			Node nodeInNextOrientation = new Node(node.getPosition(), node.getOrientation().getOrientationAfterXTurns(i));
			int amountOfTurnsNeeded = (i == 1 || i == 3) ? 1 : 2;
			new Edge(node, nodeInNextOrientation, new EnergyAmount(Robot.getEnergyRequiredToTurn().getNumeral() * amountOfTurnsNeeded, Robot.getEnergyRequiredToTurn().getUnit()));
		}

	}
	/**
	 * Generates edges from the given (head)node, keeping in mind the position of the target.
	 *
	 * @param       node
	 *              The node to generate edges from.
	 * @pre         | (node != null)
	 * @effect      | generateEdges(head, false)
	 */
	@Model
	private void generateEdges(Node node) {
		assert(node != null);
		generateEdges(node, false);
	}

	/**
	 * Generates edges from the given (head)node, keeping in mind the position of the target. 
	 * 
	 * @param       node
	 *              The node to generate edges from.
	 * @param       target
	 *              The position to eventually reach.
	 * @pre         | (node != null)
	 * @effect      | if(target == null) then generateEdges(node)
	 * @effect      | if(board.isInsideBounds(target)) then 
	 *                      |       generateEdges(head, false)
	 *                      | else 
	 *                      |       generateEdges(head, true)
	 */
	private void generateEdges(Node node, Position target) {
		assert(node != null);
		if(target == null)
			generateEdges(node);

		if(getBoard().isInsideBounds(target))
			generateEdges(node, false);
		generateEdges(node, true);
	}

	public Set<Node> getVisitedNodesAtPosition(Position pos){
		Set<Node> nodesAtPos = new HashSet<Node>();
		for(Node visitedNode : VISITED){
			if(visitedNode.getPosition().equals(pos)){
				nodesAtPos.add(visitedNode);
			}
		}
		return nodesAtPos;
	}
	
	public Node getMostEfficientNodeAt(Position position){
		EnergyAmount leastEnergy = new EnergyAmount(Double.MAX_VALUE);
		Node mostEfficient = null;
		for(Node node : getVisitedNodesAtPosition(position)){
			if(getAccumulatedWeight(node).compareTo(leastEnergy) < 0){
				leastEnergy = getAccumulatedWeight(node);
				mostEfficient = node;
			}
		}
		return mostEfficient;
	}


	/**
	 * Returns the best path to reach the given node.
	 * 
	 * @param       node
	 *              The node to get the path to.
	 * @return      A list containing the nodes to follow in order to reach the given node.
	 */
	public List<Node> getNodesPathTo(Node node)
	{
		LinkedList<Node> path = new LinkedList<Node>();
		path.addFirst(node);

		do
		{
			path.addFirst(node = this.getPreviousNode(node));
		}
		while (!this.getAccumulatedWeight(node).equals(new EnergyAmount(0)));

		return path;
	}

	/**
	 * Get the best node to reach the given node.
	 * 
	 * @param       node
	 * 	            The given node.                         
	 * @return      | let best = null
	 *              | 	for each edge in node.getbacktrack()
	 *              | 		if(isVisited(edge.getSource()) && (best == null || this.getAccumulatedWeight(edge.getSource()).compareTo(this.getAccumulatedWeight(best.getSource())) < 0)) then
	 *              |   	best == edge
	 *              | 	result == best
	 * @throws      IllegalStateException
	 *              | for each edge in node.getBacktrack()
	 *              |      !isVisited(edge.getSource()) ||  ....
	 */
	private Node getPreviousNode(Node node)
	{
		Edge best = null;
		for (Edge edge : node.getBacktrack())
		{
			if (this.isVisited(edge.getSource()))
				if (best == null || this.getAccumulatedWeight(edge.getSource()).compareTo(this.getAccumulatedWeight(best.getSource())) < 0)
					best = edge;
		}
		if (best == null)
			throw new IllegalStateException();
		return best.getSource();
	}

	/**
	 * 
	 * @return | visited.size
	 */
	@Model
	private int getNbVisited(){
		return VISITED.size();
	}

	/**
	 * Set containing the visited nodes.
	 */
	private final Set<Node> VISITED = new HashSet<Node>();

	/**
	 * Visit a node.
	 * 
	 * @param       node
	 *              The given node to visit.
	 * @post        | new.isVisited(node)
	 */
	private void visit(Node node)
	{
		this.VISITED.add(node);
	}

	/**
	 * Constant reflecting if the given node is already been visited.
	 * 
	 * @param       node
	 *              The given node to check
	 * @return      true and only true if the given node is already been visited.
	 *                      | result == this.visited.contains(node)
	 */
	public boolean isVisited(Node node)
	{
		return this.VISITED.contains(node);
	}


	/**
	 * Map containing a node as key and it's accumulated weight as value.
	 */
	private final Map<Node, EnergyAmount> ACCUMULATED_WEIGHT = new HashMap<Node, EnergyAmount>();

	/**
	 * 
	 * @param       node
	 *              The node to get the accumulated weight of.
	 * @return      
	 *        	    | if(!accumulatedWeight.keySet().contains(node)) then result == new EnergyAmount(Double.MAX_VALUE)
	 *              |       else result == this.accumulatedWeight.get(node)
	 */
	public EnergyAmount getAccumulatedWeight(Node node)
	{
		if(!ACCUMULATED_WEIGHT.keySet().contains(node))
			return new EnergyAmount(Double.MAX_VALUE);
		else
			return this.ACCUMULATED_WEIGHT.get(node);
	}

	/**
	 * Sets the accumulated weight for the given node to the given weight.
	 * 
	 * @param       node
	 *              The node whose accumulated weight will be set to the given weight.
	 * @param       weight
	 *              The given weight.
	 * @post        | new.getAccumulatedWeight(node) == weight
	 */
	public void setAccumulatedWeight(Node node, EnergyAmount weight)
	{
		this.ACCUMULATED_WEIGHT.put(node, weight);
	}

	/**
	 * A new search requires a reset.
	 * 
	 * @post        | new.getNbVisited() == 0
	 *              | new.getNbFrontline() == 0
	 *              | new.accumulatedWeight.isEmpty() == true
	 * @effect		| for each node in frontline
	 * 				|	node.terminate()
	 */
	private void reset(){
		for(Node node : FRONTLINE){
			node.terminate();
		}

		FRONTLINE.clear();
		VISITED.clear();
		ACCUMULATED_WEIGHT.clear();
	}

	/**
	 * A Boolean reflecting whether this pathfinder is terminated.
	 * 
	 * @return | isTerminated()
	 */
	@Basic
	public boolean isTerminated()
	{
		return isTerminated;
	}

	/**
	 * Terminate this pathfinder.
	 * 
	 * @post	| new.isTerminated()
	 * @effect	| reset()
	 * @post	| this.getRobot() == null
	 */
	public void terminate(){
		reset();
		robot = null;
		this.isTerminated = true;
	}

	/**
	 * Variable registering whether this pathfinder is terminated.
	 */
	boolean isTerminated;
}