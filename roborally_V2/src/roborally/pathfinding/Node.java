package roborally.pathfinding;

import java.util.ArrayList;
import java.util.List;

import roborally.position.Orientation;
import roborally.position.Position;
import be.kuleuven.cs.som.annotate.Basic;
import be.kuleuven.cs.som.annotate.Immutable;
/**
 * A class representing nodes which have a position and an orientation.
 * 
 * @invar	Each edge this node has registered is valid.
 * 			| for each edge in getEdges()
 * 			| 	canHaveAsEdge(Edge edge)
 * @invar	Each backtrack edge this node has registered is valid.
 * 			| for each edge in getBacktrack()
 * 			| 	canHaveAsBacktrack(Edge edge)
 * 
 * @author 	Niels De Bock, Michael Vincken (Bachelor of Computer Science)
 * @version	1.0
 */
public class Node
{

	/**
	 * 
	 * @param 	position
	 * 			The position for this new node.
	 * @param 	orientation
	 * 			The orientation for this new node.
	 *
	 * @post	| this.getPosition() == position;
	 * @post	| this.getOrienation() == orientation;
	 * @post	| this.getPosition() == position;
	 * @post	| this.getPosition() == position;
	 */
	public Node(Position position, Orientation orientation)
	{
		this.position = position;
		this.orientation = orientation;
		this.EDGES = new ArrayList<Edge>();
	    this.backtrack = new ArrayList<Edge>();
	}
	
	/**
	 * Returns a clone of the list of backtracking edges for this node.
	 * @return	clone of the list of backtracking edges for this node.
	 * 			| for each edge in backtrack
	 * 			|	(result.contains(edge) ==
	 * 			|		this.hasAsBacktrack(edge))
	 */
	public List<Edge> getBacktrack() {
		return new ArrayList<Edge>(backtrack);
	}
	
	/**
	 * 
	 * @return | result == backtrack.size()
	 */
	public int getNbBacktrack(){
		return backtrack.size();
	}
	
	/**
	 * 
	 * @param 	edge
	 * 			The edge to add to the backtrack of this node.
	 * @pre		| canHaveAsBacktrack(Edge edge)
	 * @effect	| backtrack.add(edge)
	 */
	public void addBacktrack(Edge edge){
		assert(canHaveAsBacktrack(edge));
		backtrack.add(edge);
	}
	
	/**
	 * Checks if the given edge is a valid backtrack edge for this node.
	 * @param 	edge
	 * 			The edge to check
	 * @return	| (edge != null && edge.getSource() == this)
	 */
	public boolean canHaveAsBacktrack(Edge edge){
		return (edge != null && edge.getDestination() == this);
	}

	/**
	 * 
	 * @param 	edge
	 * 			The edge to check
	 * @return	| result == backtrack.contains(edge)
	 */
	public boolean hasAsBacktrack(Edge edge){
		return backtrack.contains(edge);
	}
	
	/**
	 * A list containing the edges arriving in this node.
	 */
	private final List<Edge> backtrack;
	
	/**
	 * @return     | result == position
	 */
	@Basic @Immutable
	public Position getPosition(){
		return position;
	}
	
	/**
	 * The position this node is on
	 */
	private final Position position;
	
	/**
	 * @return     | result == orientation
	 */
	@Basic @Immutable
	public Orientation getOrientation(){
		return orientation;
	}
	
	/**
	 * The "orientation" of this node.
	 */
	private final Orientation orientation;
	
	/**
	 * 
	 * @param 	edge
	 * 			The outgoing edge to add.
	 * @pre		| canHaveAsEdge(Edge edge)
	 * @effect	| edges.add(edge)
	 * 
	 */
	public void addEdge(Edge edge){
		assert(canHaveAsEdge(edge));
		EDGES.add(edge);
	}
	
	/**
	 *
	 * @param 	edge
	 * 			The edge to check
	 * @return	| (edge != null && edge.getSource() == this)
	 */
	public boolean canHaveAsEdge(Edge edge){
		return (edge != null && edge.getSource() == this);
	}
	
	/**
	 * Returns a clone of the list of edges originating from this node.
	 * @return	clone of the list of edges originating from this node.
	 * 			| for each edge in edges
	 * 			|	(result.contains(edge) ==
	 * 			|		this.hasAsEdge(edge))
	 */
	@Basic
	public List<Edge> getEdges(){
		return new ArrayList<Edge>(EDGES);
	}
	
	/**
	 * 
	 * @param 	edge
	 * 			The edge to check
	 * @return	| result == edges.contains(edge)
	 */
	public boolean hasAsEdge(Edge edge){
		return EDGES.contains(edge);
	}
	
	/**
	 * 
	 * @return | result == edges.size()
	 */
	public int getNbEdges(){
		return EDGES.size();
	}
	
	/**
	 * A list containing the edges originating from this node.
	 */
	private final List<Edge> EDGES;

	
	 /**
     * Terminate this node.
     * @post	| new.getNbBacktrack() == 0
     * @post	| new.getNbEdges() == 0
     */
    public void terminate(){
    	this.backtrack.clear();
    	this.EDGES.clear();
    }
    
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((orientation == null) ? 0 : orientation.hashCode());
		result = prime * result
				+ ((position == null) ? 0 : position.hashCode());
		return result;
	}
	
	/**
	 * @param	obj
	 * 			the object to compare this object with.
	 * 
	 * @return 	result == (this == obj || 
	 * 				obj != null && getClass() == obj.getClass()
	 * 				&& getOrientation() == other.getOrientation()
	 * 				&& getPosition() != null && other.getPosition() != null &&
	 * 				getPosition().equals(other.getPosition()))
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Node other = (Node) obj;
		if (getOrientation() != other.getOrientation())
			return false;
		if (getPosition() == null) {
			if (other.getPosition() != null)
				return false;
		} else if (!getPosition().equals(other.getPosition()))
			return false;
		return true;
	}
	
	@Override
	public String toString(){
		return "(" + getPosition().getX() + ", " + getPosition().getY() + "), orientation: " + getOrientation();
	}
}