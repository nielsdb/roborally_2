package roborally.pathfinding;

import roborally.value.EnergyAmount;
import be.kuleuven.cs.som.annotate.Basic;

/**
 * A class representing edges having a source node, a destination node, and a weight.
 * 
 * @author 	Niels De Bock, Michael Vincken (Bachelor of Computer Science)
 * @version 1.0
 */
public final class Edge
{


	/**
	 * The source node of this edge.
	 */
	private final Node source;

	/**
	 * The destination node of this edge.
	 */
	private final Node destination;

	/**
	 * The weight required to use this edge.
	 */
	private final EnergyAmount weight;

	/**
	 * Initializes this edge with a given source, destination and weight.
	 * @param 	source
	 * 			The source node for this edge.
	 * @param 	destination
	 * 			The destination node for this edge.
	 * @param 	weight
	 * 			The weight for this edge
	 * @throws	IllegalArgumentException
	 * 			| (source == null || destination == null || weight == null)
	 */
	public Edge(Node source, Node destination, EnergyAmount weight) throws IllegalArgumentException
	{
		if(source == null)
			throw new IllegalArgumentException("Non-effective source");
		if(destination == null)
			throw new IllegalArgumentException("Non-effective destination");
		if(weight == null)
			throw new IllegalArgumentException("Non-effective weight");


		this.source = source;
		this.destination = destination;
		this.weight = weight;

		source.addEdge(this);
		destination.addBacktrack(this);
	}

	/**
	 * @return     | result == weight
	 */
	@Basic
	public EnergyAmount getWeight() {
		return weight;
	}

	/**
	 * 
	 * @return | result == SOURCE
	 */
	@Basic
	public Node getSource() {
		return source;
	}

	/**
	 * 
	 * @return | result == DESTINATION
	 */
	@Basic
	public Node getDestination() {
		return destination;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((destination == null) ? 0 : destination.hashCode());
		result = prime * result + ((source == null) ? 0 : source.hashCode());
		result = prime * result + ((weight == null) ? 0 : weight.hashCode());
		return result;
	}

	/**
	 * Checks if this edge is equal to the given object.
	 * @param	obj
	 * 			The object to compare this object with.
	 * @return	| if(this == obj)
	 * 			|		result == true
	 * @return	| if(obj == null || getClass() != obj.getClass())
	 * 			|		result == false
	 * @return	| if(getDestination().equals(obj.getDestination()) && getSource().equals(other.getSource)
	 * 			|	&& getWeight().equals(other.getWeight())
	 * 			|		result == true
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Edge other = (Edge) obj;
		if (destination == null) {
			if (other.destination != null)
				return false;
		} else if (!destination.equals(other.destination))
			return false;
		if (source == null) {
			if (other.source != null)
				return false;
		} else if (!source.equals(other.source))
			return false;
		if (weight == null) {
			if (other.weight != null)
				return false;
		} else if (!weight.equals(other.weight))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Edge [source=" + source + ", destination=" + destination + ", weight=" + weight + "]";
	}


}