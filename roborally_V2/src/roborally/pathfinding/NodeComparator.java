package roborally.pathfinding;

import java.util.Comparator;

import roborally.value.EnergyAmount;
import be.kuleuven.cs.som.annotate.Model;
/**
 * A class able to compare 2 nodes based on their accumulated weight in the associated
 * PathFinder.
 * 
 * @version	1.0
 * @author 	Niels De Bock, Michael Vincken (Bachelor of Computer Science)
 *
 */
class NodeComparator implements Comparator<Node>
{
	/**
	 * Initializes this NodeComparator with a given PathFinder.
	 * 
	 * @param 	finder
	 * 			The PathFinder to initialize this NodeComparator with.
	 * @post	| new.getPathFinder() == finder
	 */
	public NodeComparator(PathFinder finder)
	{
		this.PATHFINDER = finder;
	}

	/**
	 * 
	 * @return | finder
	 */
	@Model
	private PathFinder getPathFinder() {
		return PATHFINDER;
	}
	

	/**
	 * The finder that will be using this NodeComparator;
	 */
	private final  PathFinder PATHFINDER;

	/**
	 * Compares 2 given nodes based on their accumulated weight.
	 * 
	 * @param 	node1
	 * 			The first node.
	 * @param 	node2
	 * 			The node to compare the first node with.
	 * @return	| if (o1 == o2) result == 0
	 * @return	| if(getPathFinder().getAccumulatedWeight(o1).compareTo(getPathFinder().getAccumulatedWeight(o2)) < 0)
	 * 			|	result == -1
	 * 			| else
	 * 			|	result == +1
	 */
	@Override
	public int compare(Node node1, Node node2)
	{
		if (node1 == node2)
			return 0; // only replace the Node in the TreeSet, if it's the same object, not if the weights are equal
		EnergyAmount w1 = getPathFinder().getAccumulatedWeight(node1);
		EnergyAmount w2 = getPathFinder().getAccumulatedWeight(node2);
		return (w1.compareTo(w2) < 0) ? -1 : +1; // put the easiest to reach node in front
	}
}