package roborally.value;

import be.kuleuven.cs.som.annotate.Basic;
import be.kuleuven.cs.som.annotate.Immutable;
import be.kuleuven.cs.som.annotate.Value;

/**
 * An enumeration introducing different units used to express amounts of energy.
 * In its current form, this class supports the watt-second, Joule and KiloJoule.
 * 
 * @version 1.0
 * @author 	Niels De Bock, Michael Vincken
 *
 */
@Value
public enum EnergyUnit {	
	Ws("watt-second"), J("Joule"), kJ("KiloJoule");
	/**
	 * Initializes this unit with a given full name.
	 * @param	fullName
	 * 			The full name of the new unit.
	 * @post	| new.getFullName().equals(fullName)
	 */
	EnergyUnit(String fullName){
		this.fullName = fullName;
	}

	@Basic @Immutable
	public String getFullName() {
		return fullName;
	}

	
	private final String fullName;

	/**
	 * Return the value of 1 unit of this currency in the other currency.
	 * @param 	other
	 * 			The unit to convert to.
	 * @return	The resulting conversion rate is positive
	 * 			| result >= 0
	 * @return	If this unit is the same as the other unit, the result is equal to 1
	 * 			| if(this == other)
	 * 			|	then result == 1
	 * @return	The resulting conversion rate is the inverse of the conversion rate
	 * 			from the other unit to this unit.
	 * 			| result.equals(1 / other.toUnit(this))
	 */
	public double toUnit(EnergyUnit other)
		throws IllegalArgumentException {
		
		if(other == null)
			throw new IllegalArgumentException("Non effective unit");
		
		if(conversionRates[this.ordinal()][other.ordinal()] == null)
			conversionRates[this.ordinal()][other.ordinal()] = 1/conversionRates[other.ordinal()][this.ordinal()];
		
		return conversionRates[this.ordinal()][other.ordinal()];
	}
	
	/**
	 * Variable referencing a two-dimensional array registering
	 * conversion rates between units expressing energy. the first level is 
	 * indexed by the ordinal number of the unit to convert
	 * from; the ordinal number to convert to is used to index
	 * the second level.
	 */
	private static Double[][] conversionRates = new Double[3][3];

	static{
		conversionRates[Ws.ordinal()][Ws.ordinal()] = (double) 1;
		conversionRates[Ws.ordinal()][J.ordinal()] = (double) 1;
		conversionRates[Ws.ordinal()][kJ.ordinal()] = (double) 0.001;
		conversionRates[J.ordinal()][kJ.ordinal()] = (double) 0.001;
	}

}
