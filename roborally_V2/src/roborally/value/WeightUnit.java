package roborally.value;

import be.kuleuven.cs.som.annotate.Basic;
import be.kuleuven.cs.som.annotate.Immutable;
import be.kuleuven.cs.som.annotate.Value;

/**
 * An enumeration introducing different units used to express amounts of weight.
 * In its current form, this class only supports the g.
 * 
 * @version 1.0
 * @author 	Niels De Bock, Michael Vincken
 *
 */
@Value
public enum WeightUnit {
	g(0), kg(3);


	/**
	 * Initialize this weight unit with a given factor.
	 * 
	 * @param 	exponent
	 * 			This unit is equal to 10^exponent gram.
	 * @post	| new.getFactor() == factor
	 */
	WeightUnit(int exponent){
		this.exponent = exponent;
	}
	
	/**
	 * 
	 * @return | exponent
	 */
	@Basic @Immutable
	private int getExponent(){
		return exponent;
	}
	
	/**
	 * This unit is equal to 10^exponent gram.
	 */
	private final int exponent;
	
	/**
	 * Return the value of 1 unit in this unit expressed in the given unit.
	 * 
	 * @param 	other
	 * 			The unit to convert to.
	 * @return	| result >= 0
	 * @return	| if(this == other)
	 * 			|	then(result == 1)
	 * @return	| if(this != other)
	 * 			|	then(result ==  Math.pow(10, this.getExponent() - other.getExponent()))
	 * @throws	IllegalArgumentException
	 * 			| other == null
	 */
	public double toUnit(WeightUnit other) throws IllegalArgumentException {
		if(other == null) throw new IllegalArgumentException("Non-effective unit");
		
		return Math.pow(10, this.getExponent() - other.getExponent());
	}
}
