package roborally.value;

import be.kuleuven.cs.som.annotate.Basic;
import be.kuleuven.cs.som.annotate.Immutable;
import be.kuleuven.cs.som.annotate.Raw;
import be.kuleuven.cs.som.annotate.Value;

/**
 * A class of weight amounts involving a numeral and a unit.
 * 
 * @invar	The numeral of each weight amount must be a valid numeral.
 * 			| isValidNumeral(getNumeral())
 * @invar	The unit of each weight amount must be a valid unit.
 * 			| isValidUnit(getUnit())
 * 
 * @version	1.0
 * @author 	Niels De Bock, Michael Vincken
 *
 */

@Value
public class WeightAmount implements Comparable<WeightAmount> {
	/**
	 * Initialize this new weight amount with given numeral and given unit.
	 * @param 	numeral
	 * 			The numeral for this new weight amount.
	 * @param 	unit
	 * 			The unit for this new weight amount.
	 * @post	The numeral of this weight amount is equal to the given numeral.
	 * 			| new.getNumeral() == numeral
	 * @post	The unit for this new weight amount is the same as the given unit.
	 * 			| new.getUnit() == unit
	 * @throws	IllegalArgumentException
	 * 			| !isValidNumeral(numeral) || !isValidUnit(unit)
	 */
	@Raw
	public WeightAmount(int numeral, WeightUnit unit) throws IllegalArgumentException{
		if(!isValidNumeral(numeral))
			throw new IllegalArgumentException("Invalid numeral");
		if(!isValidUnit(unit))
			throw new IllegalArgumentException("Invalid unit");
		
		this.numeral = numeral;
		this.unit = unit;
	}
	
	/**
	 * Initialize this new weight amount with given numeral and unit Ws.
	 * @param 	numeral
	 * 			the numeral for this new weight amount.
	 * @effect	This new weight amount is initialized with the given numeral and the unit Ws.
	 * 			| this(numeral, Unit.Ws)
	 * @throws	IllegalArgumentException
	 * 			| !isValidNumeral(numeral)
	 */
	@Raw
	public WeightAmount(int numeral) throws IllegalArgumentException {
		this(numeral, WeightUnit.g);
	}
	
	/**
	 * Return the numeral of this weight amount.
	 */
	@Basic @Raw @Immutable
	public int getNumeral(){
		return this.numeral;
	}

	/**
	 * Check whether the given numeral is a valid numeral for any weight amount.
	 * 
	 * @param	numeral
	 * 			The numeral to check
	 * @return	True if and only if the given numeral is between zero and int.MAX_VALUE
	 * 			| result == ( (numeral >= 0)
	 * 			|	&& (numeral <= int.MAX_VALUE))
	 */
	public static boolean isValidNumeral(int numeral){
		return numeral >= 0 && numeral <= Integer.MAX_VALUE;
	}
	
	/**
	 * Variable referencing the numeral of this weight amount.
	 */
	private final int numeral;
	
	/**
	 * Return the unit of this weight amount.
	 */
	@Basic @Raw @Immutable
	public WeightUnit getUnit(){
		return this.unit;
	}
	
	/**
	 * Check whether the given unit is a valid unit for any weight amount
	 * 
	 * @param 	unit
	 *			The unit to check
	 * @return	True if and only if the given unit is effective
	 * 			| result == (unit != null)
	 */
	public static boolean isValidUnit(WeightUnit unit){
		return unit != null;
	}
	
	/**
	 * Variable referencing the unit of this weight amount.
	 */
	private final WeightUnit unit;
	
	/**
	 * Return a weight amount that has the same value as this weight amount expressed in the given unit.
	 * 
	 * @param	unit
	 * 			The unit to convert to.
	 * @return	The resulting weight amount has the given unit as its unit.
	 * 			| result.getUnit() == unit
	 * @return	the numeral of the resulting weight amount is equal the numeral of this weight
	 * 			amount multiplied with the conversion rate from the unit
	 * 			of this weight amount to the given unit rounded down to 0 decimals.
	 * 			| let
	 * 			|	conversionRate =
	 * 			|		this.getUnit().toUnit(unit).
	 * 			|	numeralInUnit =
	 * 			|		this.getNumeral().multiply(conversionRate)
	 * 			| in
	 * 			|	result.getNumeral().equals(numeralInUnit)
	 * @throws	IllegalArgumentException
	 * 			| unit == null
	 */
	public WeightAmount toUnit(WeightUnit unit) throws IllegalArgumentException
	{
		if(unit == null)
			throw new IllegalArgumentException("Non-effective unit");
		if(this.getUnit() == unit)
			return this;
		double conversionRate = this.getUnit().toUnit(unit);
		int numeralInUnit = (int) (getNumeral()*conversionRate);
		return new WeightAmount(numeralInUnit, unit);
		
	}
	/**
	 * Compute the sum of this weight amount and the other weight amount.
	 * 
	 * @param 	other
	 * 			The other weight amount to add.
	 * 
	 * @pre		| getUnit() == other.getUnit()
	 * 
	 * @return	| result.getNumeral() == this.getNumeral() + other.getNumeral()
	 * 			| result.getUnit() == this.getUnit()
	 * 
	 * @throws 	IllegalArgumentException
	 * 			| other == null
	 */
	public WeightAmount add(WeightAmount other) throws IllegalArgumentException {
		assert(getUnit() == other.getUnit());
		assert(isValidNumeral(numeral) && isValidNumeral(other.getNumeral()));
		
		if(other == null)
			throw new IllegalArgumentException("Non-effective weight amount");
		if(getUnit() == other.getUnit())
			return new WeightAmount(getNumeral() + other.getNumeral(), getUnit());
		else
			throw new IllegalArgumentException("Precondition violated");
	}
	
	/**
	 * Compute the difference between this weight amount and the other weight amount.
	 * 
	 * @param 	other
	 * 			The other weight amount to distract.
	 * 
	 * @pre		| getUnit() == other.getUnit()
	 * @pre		| isValidNumeral(this.getNumeral() - other.getNumeral())
	 * 
	 * @return	| result.getNumeral() == this.getNumeral() - other.getNumeral()
	 * 			| result.getUnit() == this.getUnit()
	 * 
	 * @throws 	IllegalArgumentException
	 * 			| other == null
	 */
	public WeightAmount subtract(WeightAmount other) throws IllegalArgumentException {
		assert(getUnit() == other.getUnit());
		assert(isValidNumeral(numeral) && isValidNumeral(other.getNumeral()));
		assert(isValidNumeral(this.getNumeral() - other.getNumeral()));
		
		if(other == null)
			throw new IllegalArgumentException("Non-effective weight amount");
		if(getUnit() == other.getUnit())
			return new WeightAmount(getNumeral() - other.getNumeral(), getUnit());
		else
			throw new IllegalArgumentException("Precondition violated");
	}
	
	
	/**
	 * Compare this weight amount with the other weight amount.
	 * 
	 * @param	other
	 * 			The other weight amount to compare with.
	 * @return	The result is equal to the comparison of the numeral of this weight amount with
	 * 			the numeral of the other weight amount.
	 * 			| result == this.toUnit(WeightUnit.g).getNumeral().compareTo
	 * 			|		(other.toUnit(WeightUnit.g).getNumeral())
	 * @throws	ClassCastException
	 * 			The other weight amount is not effective
	 * 			| ( (other == null)
	 */
	@Override
	public int compareTo(WeightAmount other) {
		if(other == null)
			throw new ClassCastException("Non-effective weight amount");
		
		Integer thisValue = this.toUnit(WeightUnit.g).getNumeral();
		Integer otherValue = other.toUnit(WeightUnit.g).getNumeral();

		return thisValue.compareTo(otherValue);
	}
	
	/**
	 * Check whether this weight amount is equal to the given object.
	 * @return 	
	 * 			| result == (other != null)
	 * 			|	&& (this.getClass() == other.getClass())
	 * 			|	&& (this.getNumeral().equals(otherAmount.getNumeral())
	 *			|	&& (this.getUnit() == otherAmount.getUnit()))
	 */
	@Override
	public boolean equals(Object other){
		if(other == null)
			return false;
		if(this.getClass() != other.getClass())
			return false;
		WeightAmount otherAmount = (WeightAmount) other;
		return (new Integer(this.getNumeral()).equals(new Integer(otherAmount.getNumeral()))
				&& (this.getUnit() == otherAmount.getUnit()));
	}
	/**
	 * return the hash code for this money amount.
	 */
	@Override
	public int hashCode(){
		return new Integer(getNumeral()).hashCode() + getUnit().hashCode();
	}
	
	/**
	 * @return	| result == this.getNumeral()+this.getUnit().toString()
	 */
	@Override
	public String toString(){
		return this.getNumeral()+this.getUnit().toString();
	}
}
