package roborally.value;

import be.kuleuven.cs.som.annotate.Basic;
import be.kuleuven.cs.som.annotate.Immutable;
import be.kuleuven.cs.som.annotate.Raw;
import be.kuleuven.cs.som.annotate.Value;

/**
 * A class of energy amounts involving a numeral and a unit.
 * 
 * @invar	The numeral of each energy amount must be a valid numeral.
 * 			| isValidNumeral(getNumeral())
 * @invar	The unit of each energy amount must be a valid unit.
 * 			| isValidUnit(getUnit())
 * 
 * @version	1.0
 * @author 	Niels De Bock, Michael Vincken
 *
 */

@Value
public class EnergyAmount implements Comparable<EnergyAmount> {
	/**
	 * Initialize this new energy amount with given numeral and given unit.
	 * @param 	numeral
	 * 			The numeral for this new energy amount.
	 * @param 	unit
	 * 			The unit for this new energy amount.
	 * @pre		| isValidNumeral(numeral)

	 * @pre		| isValidUnit(unit)
	 * 
	 * @post	The numeral of this energy amount is equal to the given numeral.
	 * 			| new.getNumeral() == numeral
	 * @post	The unit for this new energy amount is the same as the given unit.
	 * 			| new.getUnit() == unit
	 */
	@Raw
	public EnergyAmount(double numeral, EnergyUnit unit){	
		assert(isValidNumeral(numeral)) : "Invalid numeral for this new energy amount";
		assert(isValidUnit(unit)) : "Invalid unit for this new energy amount";
		this.numeral = numeral;
		this.unit = unit;
	}
	
	/**
	 * Initialize this new energy amount with given numeral and unit Ws.
	 * @param 	numeral
	 * 			the numeral for this new energy amount.
	 * @effect	This new energy amount is initialized with the given numeral and the unit Ws.
	 * 			| this(numeral, Unit.Ws)
	 */
	@Raw
	public EnergyAmount(double numeral){
		this(numeral, EnergyUnit.Ws);
	}
	
	/**
	 * Return the numeral of this energy amount.
	 */
	@Basic @Raw @Immutable
	public double getNumeral(){
		return this.numeral;
	}

	/**
	 * Check whether the given numeral is a valid numeral for any energy amount.
	 * 
	 * @param	numeral
	 * 			The numeral to check
	 * @return	True if and only if the given numeral is between zero and double.MAX_VALUE
	 * 			| result == ( (numeral >= 0)
	 * 			|	&& (numeral <= double.MAX_VALUE))
	 */
	public static boolean isValidNumeral(double numeral){
		return numeral >= 0 && numeral <= Double.MAX_VALUE;
	}
	
	/**
	 * Variable referencing the numeral of this energy amount.
	 */
	private final double numeral;
	
	/**
	 * Return the unit of this energy amount.
	 */
	@Basic @Raw @Immutable
	public EnergyUnit getUnit(){
		return this.unit;
	}
	
	/**
	 * Check whether the given unit is a valid unit for any energy amount
	 * 
	 * @param 	unit
	 *			The unit to check
	 * @return	True if and only if the given unit is effective
	 * 			| result == (unit != null)
	 */
	public static boolean isValidUnit(EnergyUnit unit){
		return unit != null;
	}
	
	/**
	 * Variable referencing the unit of this energy amount.
	 */
	private final EnergyUnit unit;
	
	/**
	 * Compute the sum of this energy amount and the other energy amount.
	 * 
	 * @param 	other
	 * 			The other energy amount to add.
	 * 
	 * @pre		| getUnit() == other.getUnit()
	 * 
	 * @return	| result.getNumeral() == this.getNumeral() + other.getNumeral()
	 * 			| result.getUnit() == this.getUnit()
	 * 
	 * @throws 	IllegalArgumentException
	 * 			| other == null
	 */
	public EnergyAmount add(EnergyAmount other) throws IllegalArgumentException {
		assert(getUnit() == other.getUnit());
		assert(isValidNumeral(numeral) && isValidNumeral(other.getNumeral()));
		
		if(other == null)
			throw new IllegalArgumentException("Non-effective energy amount");
		if(getUnit() == other.getUnit())
			return new EnergyAmount(getNumeral() + other.getNumeral(), getUnit());
		else
			throw new IllegalArgumentException("Precondition violated");
	}
	
	/**
	 * Compute the difference between this energy amount and the other energy amount.
	 * 
	 * @param 	other
	 * 			The other energy amount to distract.
	 * 
	 * @pre		| getUnit() == other.getUnit()
	 * @pre		| isValidNumeral(this.getNumeral() - other.getNumeral())
	 * 
	 * @return	| result.getNumeral() == this.getNumeral() - other.getNumeral()
	 * 			| result.getUnit() == this.getUnit()
	 * 
	 * @throws 	IllegalArgumentException
	 * 			| other == null
	 */
	public EnergyAmount subtract(EnergyAmount other) throws IllegalArgumentException {
		assert(getUnit() == other.getUnit());
		assert(isValidNumeral(this.getNumeral() - other.getNumeral()));
		
		if(other == null)
			throw new IllegalArgumentException("Non-effective energy amount");
		if(getUnit() == other.getUnit())
			return new EnergyAmount(getNumeral() - other.getNumeral(), getUnit());
		else
			throw new IllegalArgumentException("Precondition violated");
	}
	
	/**
	 * Divides this energy amount with a given divider.
	 * @param 	divider
	 * 			The divider to do the division with.
	 * @pre		| divider.getNumeral() > 0
	 * @pre		| divider != null
	 * @pre		| getUnit() == other.getUnit()
	 * @return	| result == this.getNumeral()/divider.getNumeral()
	 */
	public double divide(EnergyAmount divider){
		assert(divider.getNumeral() > 0);
		assert(divider != null);
		assert(getUnit() == divider.getUnit());
		return this.getNumeral()/divider.getNumeral();
	}	

	/**
	 * Divides this energy amount with a given divider.
	 * @param 	divider
	 * 			The divider to do the division with.
	 * @pre		| divider > 0
	 * @return	| result.equals(new EnergyAmount(this.getNumeral()/divider, this.getUnit()))
	 */
	public EnergyAmount divide(double divider) throws IllegalArgumentException {
		assert(divider > 0);
		return new EnergyAmount(this.getNumeral()/divider, this.getUnit());
	}	

	/**
	 * Multiplies this energy amount with a given multiplier.
	 * @param 	multiplier
	 * 			The multiplier to do the multiplication with.
	 * @pre		| multiplier >= 0
	 * @pre		| multiplier*getNumeral() <= Double.MAX_VALUE
	 * @return	| result.equals(new EnergyAmount(this.getNumeral()*multiplier, this.getUnit()))
	 */
	public EnergyAmount multiply(double multiplier) throws IllegalArgumentException {
		assert(multiplier >= 0);
		assert(multiplier*getNumeral() <= Double.MAX_VALUE);
		return new EnergyAmount(this.getNumeral()*multiplier, this.getUnit());
	}	
	
	/**
	 * Return an energy amount that has the same value as this energy amount expressed in the given unit.
	 * 
	 * @param	unit
	 * 			The unit to convert to.
	 * @return	The resulting energy amount has the given unit as its unit.
	 * 			| result.getUnit() == unit
	 * @return	the numeral of the resulting energy amount is equal the numeral of this energy
	 * 			amount multiplied with the conversion rate from the unit
	 * 			of this energy amount to the given unit.
	 * 			| let
	 * 			|	conversionRate =
	 * 			|		this.getUnit().toUnit(unit).
	 * 			|	numeralInUnit =
	 * 			|		this.getNumeral().multiply(conversionRate)
	 * 			| in
	 * 			|	result.getNumeral().equals(numeralInUnit)
	 * @throws	IllegalArgumentException
	 * 			| unit == null
	 */
	public EnergyAmount toUnit(EnergyUnit unit) throws IllegalArgumentException
	{
		if(unit == null)
			throw new IllegalArgumentException("Non-effective unit");
		if(this.getUnit() == unit)
			return this;
		double conversionRate = this.getUnit().toUnit(unit);
		double numeralInUnit = (getNumeral()*conversionRate);
		return new EnergyAmount(numeralInUnit, unit);
		
	}
	
	/**
	 * Compare this energy amount with the other energy amount.
	 * 
	 * @param	other
	 * 			The other energy amount to compare with.
	 * @return	The result is equal to the comparison of the numeral of this energy amount with
	 * 			the numeral of the other energy amount.
	 * 			| result == this.toUnit(EnergyUnit.Ws).getNumeral().compareTo
	 * 			|		(other.toUnit(EnergyUnit.Ws).getNumeral())
	 * @throws	ClassCastException
	 * 			The other energy amount is not effective.
	 * 			| ( (other == null)
	 */
	@Override
	public int compareTo(EnergyAmount other) {
		if(other == null)
			throw new ClassCastException("Non-effective energy amount");
		
		Double thisValue = this.toUnit(EnergyUnit.Ws).getNumeral();
		Double otherValue = other.toUnit(EnergyUnit.Ws).getNumeral();

		return thisValue.compareTo(otherValue);
		
	}
	
	/**
	 * Check whether this energy amount is equal to the given object.
	 * @return 	
	 * 			| result == (other != null)
	 * 			|	&& (this.getClass() == other.getClass())
	 * 			|	&& (this.getNumeral() == otherAmount.getNumeral()
	 *			|	&& (this.getUnit() == otherAmount.getUnit()))
	 */
	@Override
	public boolean equals(Object other){
		if(other == null)
			return false;
		if(this.getClass() != other.getClass())
			return false;
		EnergyAmount otherAmount = (EnergyAmount) other;
		return (this.getNumeral() == otherAmount.getNumeral()
				&& (this.getUnit() == otherAmount.getUnit()));
	}
	/**
	 * return the hash code for this money amount.
	 */
	@Override
	public int hashCode(){
		return new Double(getNumeral()).hashCode() + getUnit().hashCode();
	}
	
	/**
	 * @return	| result == this.getNumeral()+this.getUnit().toString()
	 */
	@Override
	public String toString(){
		return this.getNumeral()+this.getUnit().toString();
	}

}
