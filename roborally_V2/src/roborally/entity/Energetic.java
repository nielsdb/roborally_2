package roborally.entity;

import roborally.value.EnergyAmount;

public interface Energetic {
	public EnergyAmount getEnergy();
}
