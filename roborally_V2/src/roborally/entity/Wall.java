package roborally.entity;

import roborally.position.Board;
import roborally.position.Position;

/**
* A class for representing walls which can be put on any board.
* 
* @version 	1.0
* @author	Niels De Bock, Michael Vincken (Bachelor of Computer Science)
*
*/
public class Wall extends Entity {

	
	
	/**
	 * Checks whether this wall can be put on the given board and given position.
	 * 
	 * @param 	board
	 * 			The board to put the entity on.
	 * @param 	position
	 * 			The position to put the entity on.
	 * @return	true if and only if this entity can be put on the given board and given position.
	 *			| result == (board.canHaveAsEntity(this) && 
	 *			|	(board.isInsideDimensions(position) && canHaveAsBoard(board))
	 *			|	 	&& (board.getNbEntitiesOnPosition(position) == 0)
	 */			
	@Override
	public boolean canBePutOn(Board board, Position position){
		if(canHaveAsBoard(board) && canHaveAsPosition(position) && 
				board.canHaveAsEntity(this) && (board.isInsideDimensions(position))){
			return (board.getNbEntitiesOnPosition(position) == 0);
		}
		return false;
	}

	/**
	 * returns the string representation of this wall.
	 * @return	|	if(isPlacedOnABoard()) then
	 *			|		result == "Wall: (position: " + getPosition().toString() + ")"
	 *			|	else
	 *			|		result == "Wall"
	 */
	@Override
	public String toString(){
		if(isPlacedOnABoard())
			return "Wall: (position: " + getPosition().toString() + ")";
		return "Wall";
	}
	
}
