package roborally.entity;

import roborally.exception.IllegalPositionException;
import roborally.position.Board;
import roborally.position.Position;
import be.kuleuven.cs.som.annotate.Basic;
import be.kuleuven.cs.som.annotate.Raw;

/**
 * A class representing entities that can be placed on a board.
 * 
 * @version	1.1
 * @author 	Niels De Bock, Michael Vincken (Bachelor of Computer Science)
 * 
 * @invar	If this entity is placed on a board, it must have both a board and position.
 * 			| if(getBoard() == null) then (getPosition() == null)
 * 			| if(getBoard() != null) then (getPosition() ! null)
 * @invar	If this entity is placed on a board, its position and board must be valid for this entity.
 * 			| if(isPlacedOnABoard()) then canHaveAsBoard(getBoard()) && canHaveAsPosition(getPosition())
 */
public abstract class Entity {

	/**
	 * @return	the board this entity is on.
	 * 			| result == board
	 */
	@Basic
	public Board getBoard(){
		return board;
	}

	/**
	 * Sets the board for this entity to the given board.
	 * 
	 * @param 	board
	 * 			The new board for this entity.
	 * @pre		This robot can have the given board as board.
	 * 			| canHaveAsBoard(board)
	 * @post	The new board this entity is on is equal to the given robot.  
	 * 			| new.getBoard = board
	 */
	@Raw
	private void setBoard(Board board){
		assert(canHaveAsBoard(board)) : "precondition: | canHaveAsBoard(board)";
		this.board = board;
	}

	/**
	 * Checks whether this entity can be placed on the given board.
	 * 
	 * @return	| if(isTerminated())
	 * 			|	result == (board == null)
	 * @return 	true if and only if this board exist and is not terminated.
	 * 			| result == (board != null) && (!board.isTerminated())
	 */
	protected boolean canHaveAsBoard(Board board){
		if(isTerminated())
			return (board == null);
		return (board != null) && (!board.isTerminated());
	}

	/**
	 * The board this entity is on.
	 */
	protected Board board = null;

	/**
	 * Sets the position for this entity to the given position.
	 * 
	 * @param 		position     
	 * 				The new position for this entity.
	 * @pre			| canHaveAsPosition(position)
	 * @post     	The new position this entity is on is equal to the given position.  
	 * 				| new.getPosition = position
	 */
	@Raw
	private void setPosition(Position position){
		assert(canHaveAsPosition(position));
		this.position = position;
	}

	/**
	 * @return	the position of this entity.
	 * 			| position
	 */
	public Position getPosition(){
		return position;
	}

	/**
	 * Checks whether this entity can have the given position as position.
	 * 
	 * @return	| if(isTerminated())
	 * 			|	result == (position == null)
	 * @return 	true if and only if this position exists.
	 * 			| result == (position != null) && (!position.isTerminated())
	 */
	protected boolean canHaveAsPosition(Position position) {
		if(isTerminated())
			return (position == null);
		return (position != null);
	}

	/**
	 * A variable holding the position of this entity on its board.
	 */
	protected Position position = null;

	/**
	 * Checks whether this entity can be put on the given position.
	 * 
	 * @param	position
	 * 			The position to put this entity on.
	 * @return	false if this robot is not placed on a board.
	 * 			| !isPlacedOnABoard()
	 * @return 	true if and only if this entity can be put on the given position.
	 * 			| canBePutOn(getBoard(), position)
	 */
	public boolean canBePutOn(Position position){
		if(!isPlacedOnABoard())
			return false;
		return this.canBePutOn(getBoard(), position);
	}

	/**
	 * Checks whether this entity can be put on the given board and given position.
	 * 
	 * @param 	board
	 * 			The board to put the entity on.
	 * @param 	position
	 * 			The position to put the entity on.
	 * @return	true if and only if this entity can be put on the given board and given position.
	 *			| result == (board.canHaveAsEntity(this) && 
	 *			|	(board.isInsideDimensions(position) && canHaveAsBoard(board) && canHaveAsPosition(position))
	 */
	public boolean canBePutOn(Board board, Position position){
		return canHaveAsBoard(board) && canHaveAsPosition(position) && board.canHaveAsEntity(this) && (board.isInsideDimensions(position));
	}

	/**
	 * Put this entity on the given position on the given board.
	 * 
	 * @param 	position
	 * 			The position to put this entity on.
	 * @param 	board
	 * 			The board to put this entity on.
	 * @pre		This entity is not placed on a board.
	 * 			| !isPlacedOnABoard()
	 * @effect	| board.addEntityToPosition(position, this)
	 * 			| setBoard(board)
	 * 			| setPosition(position)
	 * @throws 	IllegalPositionException
	 * 			The entity is put on an invalid position.
	 * 			| (!this.canBePutOn(position))
	 * @throws	IllegalArgumentException
	 * 			| !canHaveAsBoard(board) || !canHaveAsPosition(position)
	 */
	public void put(Position position, Board board) throws IllegalPositionException{
		if(!canHaveAsBoard(board) || !canHaveAsPosition(position))
			throw new IllegalArgumentException("This entity cannot have the given board or position.");
		assert(!isPlacedOnABoard()) : "Precondition: This entity is not placed on a board.";
		board.addEntityToPosition(position, this);
		setBoard(board);
		setPosition(position);
	}

	/**
	 * Remove this entity from its position and board.
	 * 
	 * @pre		This entity is placed on a board.
	 * 			| isPlacedOnABoard()
	 * @post	This entity is removed from the board
	 * 			| new.isPlacedOnABoard() == false;
	 */
	public void remove(){
		assert(isPlacedOnABoard()) : "precondition: This entity is placed on a board.";
		
		board.removeEntityFromPosition(position, this);
		this.position = null;
		this.board = null;
	}

	/**
	 * Replace an entity to a given destination.
	 * 
	 * @param 	destination
	 * 			The given destination position to move this entity to.
	 * @pre		This entity is placed on a board.
	 * 			| isPlacedOnABoard()
	 * @post	| new.this.getPosition() == destination
	 * @throws 	IllegalPositionException
	 * 			| (!canBePutOn(destination)
	 * @throws	IllegalArgumentException
	 * 			| !canHaveAsPosition(position))
	 */
	protected void replace(Position destination) throws IllegalPositionException{
		if(!canHaveAsPosition(position))
			throw new IllegalArgumentException("This entity cannot have the given position as position");
		assert(isPlacedOnABoard()) : "Precondition: This entity is placed on a board.";
		if(!canBePutOn(destination)) throw new IllegalPositionException(position);

		if(isPlacedOnABoard()){
			board.replaceEntity(this, destination);
			setPosition(destination);
		}
	}

	/**
	 * This method defines how an entity reacts to being hit.
	 * 
	 * @effect	| return
	 */
	public void getsHit(){
		//Default reply: do nothing.
	}
	
	/**
	 * Return a boolean indicating whether or not this entity is placed on a board.
	 * 
	 * @return	True if and only if this entity is placed on a board.
	 * 			| result == (board != null && position != null)
	 */
	public boolean isPlacedOnABoard(){
		return (board != null && position != null);
	}

	/**
	 * Return a boolean indicating whether or not this entity is terminated.
	 */
	public boolean isTerminated() {
		return this.isTerminated;
	}

	/**
	 * Terminate this entity, breaking the association with board and position in which that entity might be involved. 
	 *
	 * @post	| new.getBoard() == null
	 * 			| new.getPosition() == null
	 * @post	This entity is terminated
	 * 			| new.isTerminated()	
	 */
	public void terminate(){
		if(isPlacedOnABoard())
			remove();
		this.isTerminated = true;
	}

	/**
	 * Variable registering whether this entity is terminated.
	 */
	protected boolean isTerminated = false;
}
