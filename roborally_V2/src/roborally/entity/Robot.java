package roborally.entity;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;

import roborally.entity.item.Battery;
import roborally.entity.item.Item;
import roborally.entity.item.RepairKit;
import roborally.entity.item.SurpriseBox;
import roborally.exception.IllegalPositionException;
import roborally.exception.OutOfDimensionException;
import roborally.pathfinding.Node;
import roborally.pathfinding.PathFinder;
import roborally.position.Board;
import roborally.position.Direction;
import roborally.position.Orientation;
import roborally.position.Position;
import roborally.program.Program;
import roborally.program.exception.UnparsableException;
import roborally.value.EnergyAmount;
import roborally.value.WeightAmount;
import roborally.value.WeightUnit;
import be.kuleuven.cs.som.annotate.Basic;
import be.kuleuven.cs.som.annotate.Immutable;
import be.kuleuven.cs.som.annotate.Model;
import be.kuleuven.cs.som.annotate.Raw;

/**
 * A class for representing robots which can move, turn and have a certain amount of energy. 
 * 
 * @invar	The energy of a robot must be valid.
 * 			| isValidEnergy(getEnergy())
 * @invar	each robot can have its orientation as orientation.
 * 			| canHaveAsOrientation(getOrientation)
 * @invar	each robot can have its energy as energy.
 * 			| canHaveAsEnergy(getEnergy())
 * @invar	each robot can have its maximum energy as maximum energy.
 * 			| canHaveAsMaximumEnergy(getMaximumEnergy())
 * @invar	this robot can have each possession it has.
 * 			| for each possession in getPossessions()
 * 			|	canHaveAsPossession(possession)
 * @invar	each possession this robot has is not placed on a board.
 * 			| for each possession in getPossessions()
 * 			|	!possession.isPlacedOnABoard()
 * @invar	This robot can have its program as program
 * 			| canHaveAsProgram(getProgram())
 * 
 * @version 1.3
 * @author 	Niels De Bock, Michael Vincken (Bachelor of Computer Science)
 */
public class Robot extends Entity implements Energetic {
	/**
	 * Initializes this new robot with a given position(<code>xPosition</code>, <code>yPosition</code>), orientation and energy.
	 * 
	 * @param 	orientation
	 * 			The initial orientation for this new robot.
	 * @param 	energy
	 * 			The initial energy for this new robot.
	 * @pre		The given energy must be a valid energy for any robot.
	 * 			| canHaveAsEnergy(energy)
	 * @post	The energy of this new Robot is equal to the given energy.
	 * 			| new.getEnergy() == energy
	 * @effect	The orientation of this robot is set to the given orientation.
	 * 			| setOrientation(orientation)
	 * @note 	Any new robot initialized with this constructor will satisfy all its class invariants.
	 */
	@Raw
	public Robot(Orientation orientation, EnergyAmount energy){		
		assert(canHaveAsEnergy(energy)) : "Precondition: The given energy must be a valid energy for any robot.";

		setEnergy(energy);
		setOrientation(orientation);
	}
	
	/**
	 * @return 	the highest possible value for the energy of any robot.
	 * 			| result.equals(maximumEnergy)
	 */
	@Basic
	public EnergyAmount getMaximumEnergy() {
		return maximumEnergy;
	}

	/**
	 * Sets the maximum energy of this robot to the given energy.
	 * 
	 * @param 	maximumEnergy     
	 * 			The new maximum energy for this robot.
	 * @pre     The given maximal energy is valid for this robot.  
	 * 			| canHaveAsMaximumEnergy(energy)
	 * @post    The new maximum energy for this robot is equal to the given energy.  
	 * 			| new.getMaximumEnergy() == maximumEnergy
	 * @effect	If the new maximum energy is lower then the current energy
	 * 			the current energy of the robot will be decreased to the new maximum energy.
	 * 			| if(getEnergy().compareTo(getMaximumEnergy()) > 0)
	 *			| 	then setEnergy(getMaximumEnergy())
	 */
	public void setMaximumEnergy(EnergyAmount maximumEnergy){
		assert(canHaveAsMaximumEnergy(maximumEnergy));
		this.maximumEnergy = maximumEnergy;
		if(getEnergy().compareTo(getMaximumEnergy()) > 0)
			setEnergy(getMaximumEnergy());
	}

	/**
	 * Checks if this robot can have the given energy as maximum energy.
	 * 
	 * @param	maximumEnergy
	 * 			The energy to check.
	 * @return	True if the given maximum energy is effective and smaller than or equal to the maximum maximum energy for this robot.
	 *			| result == (maximumEnergy != null 
	 *			|	&& energyAmount.compareTo(Robot.getMaximumMaximumEnergy()) <= 0)
	 */
	@Raw
	private static boolean canHaveAsMaximumEnergy(EnergyAmount maximumEnergy){
		return(maximumEnergy != null && maximumEnergy.compareTo(Robot.getMaximumMaximumEnergy()) <= 0);
	}

	/**
	 * Constant reflecting for the highest possible value for the energy of any robot.
	 * 
	 * @return	the highest possible value for the energy of a robot.
	 */
	private EnergyAmount maximumEnergy = new EnergyAmount(20000);

	/**
	 * Gets the highest possible value for the maximum energy of any robot.
	 * 
	 * @return 	the highest possible value for the maximum energy of any robot.
	 * 			| result.equals(maximumMaximumEnergy)
	 */
	@Basic
	public static EnergyAmount getMaximumMaximumEnergy() {
		return MAXIMUM_MAXIMUM_ENERGY;
	}

	/**
	 * Constant reflecting for the highest possible value for the maximum energy of any robot.
	 * 
	 * @return	the highest possible value for the maximum energy of a robot.
	 * 			| result.equals(new EnergyAmount(20000))
	 */
	private final static EnergyAmount MAXIMUM_MAXIMUM_ENERGY = new EnergyAmount(20000);


	/**
	 * Gets the energy required to move one step.
	 * 
	 * @return 	the energy required to move one step.
	 * 			| result == baseEnergyRequiredToMove.add(new EnergyAmount(extraEnergyRequiredToMovePerKg.getNumeral() * getCarriedWeight().toUnit(WeightUnit.kg).getNumeral()))
	 */
	public EnergyAmount getEnergyRequiredToMove() {
		return BASE_ENERGY_REQUIRED_TO_MOVE.add(new EnergyAmount(EXTRA_ENERGY_REQUIRED_TO_MOVE_PER_KG.getNumeral() * getCarriedWeight().toUnit(WeightUnit.kg).getNumeral()));
	}

	/**
	 * Constant reflecting the energy required to move one step forward.
	 * 
	 * @return	the energy required to move one step forward.
	 * 			| result == 500ws
	 */
	private final static EnergyAmount BASE_ENERGY_REQUIRED_TO_MOVE = new EnergyAmount(500);

	/**
	 * Constant reflecting the extra energy required to move one step forward per kg this robot is carrying.
	 * 
	 * @return	the extra energy required to move one step forward per kg this robot is carrying.
	 * 			| result == 50ws
	 */
	private final static EnergyAmount EXTRA_ENERGY_REQUIRED_TO_MOVE_PER_KG = new EnergyAmount(50);


	/**
	 * Gets the energy required to turn 90 degrees.
	 * 
	 * @return 	the energy required to turn 90 degrees.
	 * 			| result == 100
	 */
	@Immutable @Basic
	public static EnergyAmount getEnergyRequiredToTurn() {
		return ENERGY_REQUIRED_TO_TURN;
	}

	/**
	 * Constant reflecting the energy required to turn 90 degrees.
	 * 
	 * @return	the energy required to turn 90 degrees.
	 * 			| result == 100
	 */
	private final static EnergyAmount ENERGY_REQUIRED_TO_TURN = new EnergyAmount(100);

	/**
	 * Gets the energy required to shoot.
	 * 
	 * @return 	the energy required to shoot.
	 * 			| result == 1000
	 */
	@Immutable @Basic
	public static EnergyAmount getEnergyRequiredToShoot() {
		return ENERGY_REQUIRED_TO_SHOOT;
	}

	/**
	 * Constant reflecting the energy required to shoot.
	 * 
	 * @return	the energy required to shoot.
	 * 			| result == 1000
	 */
	private final static EnergyAmount ENERGY_REQUIRED_TO_SHOOT = new EnergyAmount(1000);

	/**
	 * Gets the energy of this robot.
	 * 
	 * @return	the energy of this robot.
	 * 			| result == energy
	 */
	@Basic
	public EnergyAmount getEnergy() {
		return energy;
	}


	/**
	 * Sets the energy of this robot to the given energy.
	 * 
	 * @param 	energy     
	 * 			The new energy for this robot.
	 * @pre     The given energy is valid for this robot.  
	 * 			| canHaveAsEnergy(energy)
	 * @post    The new energy for this robot is equal to the given energy.  
	 * 			| new.getEnergy() == energy
	 */
	private void setEnergy(EnergyAmount energy) {
		assert(canHaveAsEnergy(energy)) : "Precondition: The given energy is valid for this robot.";

		this.energy = energy;
	}

	/**
	 * Checks if this robot can have the given energy.
	 * 
	 * @param	energyAmount
	 * 			The energy to check.
	 * @return	True if the given energy-amount is effective and smaller than or equal to the maximum energy for this robot.
	 *			| result == (energyAmount != null 
	 *			|	&& energyAmount.compareTo(getMaximumEnergy()) <= 0)
	 */
	@Raw
	private boolean canHaveAsEnergy(EnergyAmount energyAmount){
		return(energyAmount != null 
				&& energyAmount.compareTo(getMaximumEnergy()) <= 0);
	}

	/**
	 * a variable registering the energy in watt-second (Ws) of this robot.
	 */
	private EnergyAmount energy;

	/**
	 * Gets the orientation of this robot.
	 * 
	 * @return	the orientation of this robot (Enum).
	 * 			| result == orientation
	 */
	@Basic
	public Orientation getOrientation() {
		return orientation;
	}

	/**
	 * Returns a boolean reflecting whether this robot can have the given orientation as orientation.
	 * 
	 * @param 	orientation
	 * 			The orientation to check.
	 * @return  True if and only if the given orientation is effective.
	 * 			| result == (orientation != null)
	 */
	private boolean canHaveAsOrientation(Orientation orientation){
		return (orientation != null);
	}

	/**
	 * Sets the orientation of this robot to the given orientation.
	 * 
	 * @param 	orientation     
	 * 			The new orientation for this robot.
	 * @post    If this robot can't have the given orientation as orientation, it will receive an upwards orientation.  
	 * 			| if(!canHaveAsOrientation(orientation)) then new.getOrientation() == Orientation.UP
	 * @post    The new orientation for this robot is equal to the given orientation.  
	 * 			| new.getOrientation() == orientation
	 */
	private void setOrientation(Orientation orientation) {
		if(!canHaveAsOrientation(orientation))
			this.orientation = Orientation.UP;
		else
			this.orientation = orientation;
	}

	/**
	 * a variable referencing the orientation of this robot.
	 */
	private Orientation orientation;

	/**
	 * Query the amount of energy in watt-second (Ws) as a percentage of the maximum amount.
	 * 
	 * @return 	The energy this robot has left divided by its maximum amount of energy, multiplied by 100.
	 * 			| result == Math.round((getEnergy().divide(getMaximumEnergy()))*100);
	 */
	public double getFractionOfEnergy(){
		return Math.round((getEnergy().divide(getMaximumEnergy()))*100);
	}

	/**
	 * Determines if this robot has sufficient energy to move one step forward.
	 * 
	 * @return  True if and only if the Robot has enough energy to move one step forward.
	 * 			| result == (getEnergy().compareTo(getEnergyRequiredToMove()) >= 0)
	 */
	public boolean hasEnoughEnergyToMove(){
		return (getEnergy().compareTo(getEnergyRequiredToMove()) >= 0);
	}

	/**
	 * Determines if this robot has sufficient energy to turn 90 degrees.
	 * 
	 * @return  True if and only if the Robot has enough energy to turn 90 degrees.
	 * 			| result == (getEnergy().compareTo(getEnergyRequiredToTurn()) >= 0)
	 */
	public boolean hasEnoughEnergyToTurn(){
		return (getEnergy().compareTo(getEnergyRequiredToTurn()) >= 0);
	}

	/**
	 * Determines if this robot has sufficient energy to shoot.
	 * 
	 * @return  True if and only if the Robot has enough energy to shoot.
	 * 			| result == (getEnergy().compareTo(getEnergyRequiredToShoot()) >= 0)
	 */
	public boolean hasEnoughEnergyToShoot(){
		return (getEnergy().compareTo(getEnergyRequiredToShoot()) >= 0);
	}

	/**
	 * Moves this robot one step in its current direction if this robot has sufficient energy. You can check if this robot has enough
	 * energy to move (one step forward) using the hasEnoughEnergyToMove() method.
	 * 
	 * @pre		The robot has enough energy to move one step forward.
	 * 			| hasEnoughEnergyToMove() == true
	 * @pre		This robot must be placed on a board.
	 * 			| isPlacedOnABoard()
	 * @post	The new position of the robot is the position reached after moving one step forward.
	 * 			| new.getPosition() == getPositionAfterMove()
	 * @effect	Amount of energy required to move is removed from the robot.
	 * 			| discharge(getEnergyRequiredToMove())
	 * @throws 	IllegalPositionException
	 * 			The robot cannot move.
	 * 			| !canMove()	
	 * @throws 	OutOfDimensionException
	 * 			The robot tries to move out of bounds.
	 * 			| !Position.isValidPosition(getXPositionAfterMove(), getYPositionAfterMove())
	 */
	public void move() throws OutOfDimensionException, IllegalPositionException {
		assert(isPlacedOnABoard()) : "Precondition: The robot must be placed on a board.";
		assert(hasEnoughEnergyToMove()) : "Precondition: The robot has enough energy to move one step forward.";
		if(!Position.isValidPosition(getXPositionAfterMove(), getYPositionAfterMove())) 
			throw new OutOfDimensionException(getXPositionAfterMove(), getYPositionAfterMove());
		if(!canMove())
			throw new IllegalPositionException(getPositionAfterMove());
		replace(getPositionAfterMove());
		
		discharge(getEnergyRequiredToMove());
	}

	/**
	 * Determines if this robot can move one step forward.
	 * 
	 * @pre		This robot must be placed on a board.
	 * 			| isPlacedOnABoard()
	 * @return	True if and only if this robot can move one step forward, in other words, if the position after moving is a valid one.
	 * 			| result == this.canBePutOn(getPositionAfterMove())
	 * @return	False if the position after moving is out of bounds.
	 * 			| if(!Position.isValidPosition(getXPositionAfterMove(), getYPositionAfterMove())) then
	 * 			| 	result == false
	 */
	@Model
	private boolean canMove(){
		assert(isPlacedOnABoard()) : "Precondition: The robot must be placed on a board.";
		try {
			return this.canBePutOn(getPositionAfterMove());
		} catch (OutOfDimensionException e) {
			return false;
		}
	}

	/**
	 * Get the position after a move from the given position in the given orientation.
	 * 
	 * @param 	position
	 * 			The given position to move from.
	 * @param 	orientation
	 * 			The given orientation to move in. 
	 * @return	the position after a move from the given position in the given orientation.
	 * 			| Position(getXPositionAfterMove(position, orientation), getYPositionAfterMove(position, orientation))
	 * @throws 	OutOfDimensionException
	 * 			The robot tries to move out of bounds.
	 * 			| !Position.isValidPosition(getXPositionAfterMove(position, orientation), getYPositionAfterMove(position, orientation)))		
	 */
	public static Position getPositionAfterMove(Position position, Orientation orientation) throws OutOfDimensionException {
		return new Position(getXPositionAfterMove(position, orientation), getYPositionAfterMove(position, orientation));
	}

	/**
	 * Return the position of this robot after he moved one step forward.
	 * 
	 * @return 	getPositionAfterMove(getPosition(), getOrientation())
	 * @pre		This robot must be placed on a board.
	 * 			| isPlacedOnABoard()
	 * @return	the position of this robot after he moved from its current position in his current orientation.
	 * 			| getPositionAfterMove(getPosition(), getOrientation())
	 * @throws 	OutOfDimensionException 
	 *			The robot tries to move out of bounds.
	 * 			| !Position.isValidPosition(getXPositionAfterMove(position, orientation), getYPositionAfterMove(position, orientation)))		
	 */
	public Position getPositionAfterMove() throws OutOfDimensionException{
		assert(isPlacedOnABoard()) : "Precondition: The robot must be placed on a board.";

		return getPositionAfterMove(getPosition(), getOrientation());
	}

	/**
	 * Return the Y-position of this robot after he moved one step from the given position in the
	 * given orientation.
	 * 
	 * @param 	position
	 * 			The current position of this robot.
	 * @param 	orientation
	 * 			The current orientation of this robot.
	 * @return	the Y-position of this robot after he moved one step from the given position in the
	 * 			given orientation.
	 * 			| if(orientation.ordinal() % 2 == 0) then
	 * 			| 	result == position.getY() + (orientation.ordinal() % 4 - 1);
	 *			| else
	 *			|	result == position.getY();
	 */
	private static long getYPositionAfterMove(Position position, Orientation orientation){
		if(orientation.ordinal() % 2 == 0)
			return position.getY() + (orientation.ordinal() % 4 - 1);
		else
			return position.getY();
	}

	/**
	 * Return the Y-coordinate of the position reached after moving in the current orientation.
	 * 
	 * @pre		This robot must be placed on a board.
	 * 			| isPlacedOnABoard()
	 * @return	the Y-coordinate of the position reached after moving in the current orientation.
	 * 			| this.getYPositionAfterMove(getOrientation())
	 */
	private long getYPositionAfterMove(){
		assert(isPlacedOnABoard()) : "Precondition: The robot must be placed on a board.";

		return Robot.getYPositionAfterMove(getPosition(), getOrientation());
	}

	/**
	 * Return the X-coordinate of the position reached after moving from the given position
	 * in the given orientation.
	 * 
	 * @param 	position
	 * 			The current position of this robot.
	 * @param 	orientation
	 * 			The current orientation of this robot.
	 * @return	the X-coordinate of the position reached after moving from the given position
	 *			in the given orientation.
	 * 			| if(orientation.ordinal() % 2 == 0) then
	 * 			| 	result == position.getX();
	 *			| else 
	 *			|	result == position.getX() - (orientation.ordinal() % 4 - 2)
	 */
	private static long getXPositionAfterMove(Position position, Orientation orientation){
		if(orientation.ordinal() % 2 == 0)
			return position.getX();
		else
			return position.getX() - (orientation.ordinal() % 4 - 2);
	}

	/**
	 * Return the X-coordinate of the position reached after moving in the current orientation.
	 * 
	 * @pre		This robot must be placed on a board.
	 * 			| isPlacedOnABoard()
	 * @return	the X-coordinate of the position reached after moving in the current orientation.
	 * 			| this.getXPositionAfterMove(getOrientation())
	 */
	private long getXPositionAfterMove(){
		assert(isPlacedOnABoard()) : "Precondition: The robot must be placed on a board.";

		return Robot.getXPositionAfterMove(getPosition(), getOrientation());
	}

	/**
	 * Let this robot turn clockwise.
	 * 
	 * @pre		The robot has enough energy to turn 90 degrees.
	 * 			| hasEnoughEnergyToTurn() == true
	 * @effect	this robot turns clockwise
	 * 			| turn(Direction.CLOCKWISE)
	 */
	public void turn(){
		assert(hasEnoughEnergyToTurn()) : "Precondition: The robot has enough energy to turn 90 degrees.";
		turn(Direction.CLOCKWISE);
	}

	/**
	 * Turn this robot (counter)clockwise if the robot has enough energy to do so. You can check if the robot has enough
	 * energy using the hasEnoughEnergyToTurn() method.
	 * 
	 * @pre		The robot has enough energy to turn 90 degrees.
	 * 			| hasEnoughEnergyToTurn() == true
	 * @param 	direction
	 * 			The direction in which to turn the robot (enum). Can be Direction.CLOCKWISE or Direction.COUNTERCLOCKWISE.
	 * @post	The robot is turned in the given direction.
	 * 			| if(direction == Direction.CLOCKWISE) then
	 * 			|	new.getOrientation() == getOrientation().getOrientationAfterXTurns(1);
	 * 			| if(direction == Direction.COUNTERCLOCKWISE) then
	 *			|	new.getOrientation() ==  getOrientation().getOrientationAfterXTurns(3)
	 * @effect	Amount of energy required to turn is removed from the robot.
	 * 			| discharge(getEnergyRequiredToTurn());
	 */
	public void turn(Direction direction){
		assert(hasEnoughEnergyToTurn()) : "Precondition: The robot has enough energy to turn 90 degrees.";
		if (direction == Direction.CLOCKWISE){
			setOrientation(getOrientation().getOrientationAfterXTurns(1));
		}
		else if (direction == Direction.COUNTERCLOCKWISE){
			setOrientation(getOrientation().getOrientationAfterXTurns(3));
		}

		discharge(getEnergyRequiredToTurn());
	}

	/**
	 * Returns the energy required to reach the given target.
	 * 
	 * @param 	target
	 * 			the given target.
	 * @return	If this robot is not placed on a board, or the given position is unreachable due to obstacles, this method will return -1
	 * 			| if (!isPlacedOnABoard() || pathFinder.getEnergyRequiredToReach(target).equals(Double.MAX_VALUE)) then
	 * 			|	result == -1
	 * @return	Else if the given target is unreachable due to a lack of energy, this method will return -2.
	 * 			| else if(pathFinder.getEnergyRequiredToReach(target).compareTo(this.getEnergy()) >= 0) then
	 * 			|	result == -2
	 * @return	Else, this method will return a double representing the energy in Ws.
	 * 			|	result == pathFinder.getEnergyRequiredToReach(target).getNumeral()
	 */
	public double getEnergyRequiredToReach(Position target){
		if(!isPlacedOnABoard())
			return -1;

		PathFinder pathFinder = new PathFinder(this);
		pathFinder.search(target);
		EnergyAmount reqEnergy = pathFinder.getEnergyRequiredToReach(target);
		if(reqEnergy.equals(Double.MAX_VALUE)) //unreachable
			return -1;
		else if(reqEnergy.compareTo(this.getEnergy()) >= 0)
			return -2;
		else
			return reqEnergy.getNumeral();
	}

	/**
	 * Returns a map containing the positions that are inside the bounds of the board and that
	 * this robot can reach as keys, and the energy needed to do so as values.
	 *
	 * @pre		This robot must be placed on a board.
	 * 			| isPlacedOnABoard()
	 * @return	Returns a map containing the positions this robot can reach as keys, and the energy needed to do so
	 * 			as values.
	 * 			| for each x from 0 to board.getWidth()
	 * 			|	for each y from 0 to board.getHeight()
	 * 			|		let position = new Position(x,y)
	 * 			| 		if(getEnergyRequiredToReach(position) < 0)
	 * 			|			result.contains(position)
	 */
	@Model
	private Map<Position, EnergyAmount> getReachablePositions(){
		assert(this.isPlacedOnABoard());
		Map<Position, EnergyAmount> reachablePos = new HashMap<Position, EnergyAmount>();

		long leftX,rightX,bottomY,topY;

		leftX = board.getBoundsOfPositionsInUse()[0].getX();
		rightX = board.getBoundsOfPositionsInUse()[1].getX();
		topY = board.getBoundsOfPositionsInUse()[0].getY();
		bottomY = board.getBoundsOfPositionsInUse()[1].getY();

		PathFinder pathFinder = new PathFinder(this);
		pathFinder.fullSearch(getEnergy());
		for(long i = leftX; i <= rightX; i++){
			for(long j = topY; j <= bottomY; j++){
				//create a new position
				Position pos = null;
				try {
					pos = new Position(i,j);
				} catch (OutOfDimensionException e1) {
					e1.printStackTrace();
				}

				if(pathFinder.getEnergyRequiredToReach(pos).compareTo(this.getEnergy()) <= 0){	
					reachablePos.put(pos, pathFinder.getEnergyRequiredToReach(pos));
				}
			}
		}
		return reachablePos;

	}

	/**
	 * Moves this robot as close as possible (expressed in Manhattan distance) to the given robot.
	 * 
	 * @param	other
	 * 			The robot to move next to.
	 * @pre		Both robots are located on the same board.
	 * 			| this.isPlacedOnABoard() && other.isPlacedOnABoard()
	 * 			| this.getBoard() == other.getBoard()
	 * @post	This robot's position is as close as possible to the other's position, using the least amount of energy
	 * 			possible.
	 * 			| let optimalEnergy = new EnergyAmount(Double.MAX_VALUE)
	 *			| let optimalDistance = Board.getDistanceBetween(this, other)
	 * 			| for each positionThis in this.getReachablePositions()
	 * 			|	for each positionOther in other.getReachablePositions()
	 * 			|		let distance = Board.getDistanceBetween(positionThis, positionOther)
	 * 			|		let energy = getEnergyRequiredToReach(positionThis).add(other.getEnergyRequiredToReach(positionOther))
	 * 			|		if((distance < optimalDistance && distance != 0)
	 * 			|			|| distance == optimalDistance && energy < optimalEnergy) then
	 * 			|				optimalPositionForThisRobot = positionThis
	 * 			|				optimalPositionForOtherRobot = positionOther
	 * 			|
	 * 			|	 			optimalDistance = distance;
	 * 			|	 			optimalEnergy = energy;
	 * 			|	  
	 * 			| new.getPosition().equals(optimalPositionForThisRobot)
	 * 			| new.other.getPosition().equals(optimalPositionForOtherRobot)
	 * 			| new.getOrientation().equals(new PathFinder(this).search(optimalPositionForThisRobot).getOrientation())
	 * 			| new.other.getOrientation().equals(new PathFinder(other).search(optimalPositionForOtherRobot).getOrientation())
	 **/
	public void moveNextTo(Robot other){
		assert(this.isPlacedOnABoard() && other.isPlacedOnABoard());
		assert(this.getBoard() == other.getBoard());

		if(Board.getDistanceBetween(this.getPosition(), other.getPosition()) == 1)
			return;

		Map<Position, EnergyAmount> positionsThisRobotCanReach = getReachablePositions();
		Map<Position, EnergyAmount> positionsOtherRobotCanReach = other.getReachablePositions();

		Position optimalPositionForThisRobot = this.getPosition();
		Position optimalPositionForOtherRobot = other.getPosition();

		long optimalDistance = Board.getDistanceBetween(this, other);
		EnergyAmount optimalEnergy = new EnergyAmount(Double.MAX_VALUE);

		for(Position positionThis : positionsThisRobotCanReach.keySet()){
			for(Position positionOther : positionsOtherRobotCanReach.keySet()){

				long distance = Board.getDistanceBetween(positionThis, positionOther);
				if(distance > optimalDistance || distance == 0)
					continue;

				EnergyAmount energy = positionsThisRobotCanReach.get(positionThis).add(positionsOtherRobotCanReach.get(positionOther));
				if(distance == optimalDistance && energy.compareTo(optimalEnergy) >= 0)
					continue;


				optimalPositionForThisRobot = positionThis;
				optimalPositionForOtherRobot = positionOther;

				optimalDistance = distance;
				optimalEnergy = energy;
			}
		}
		this.moveTo(optimalPositionForThisRobot);
		other.moveTo(optimalPositionForOtherRobot);
	}

	/**
	 * Moves this robot via the most efficient path to the given position.
	 * 
	 * @param	position
	 * 			The position to move to.
	 * @pre		This robot must be placed on a board.
	 * 			| isPlacedOnABoard()
	 * @pre		The given position is reachable by this robot.
	 * 			| getEnergyRequiredToReach(position) >= 0
	 * @post	| new.this.getPosition().equals(position)
	 * 			| new.getOrientation().equals(new PathFinder(this).search(position).getOrientation())
	 */
	private void moveTo(Position position){
		assert(isPlacedOnABoard()) : "Precondition: The robot must be placed on a board.";
		assert(getEnergyRequiredToReach(position) >= 0) : "Precondition: The given position is reachable by this robot.";
				
		PathFinder pathFinder = new PathFinder(this);
		pathFinder.search(position);
		
		if(!position.equals(getPosition())){
			Node optimalNode = pathFinder.getMostEfficientNodeAt(position);
			for(Node node : pathFinder.getNodesPathTo(optimalNode)){
				moveTo(node);
			}
		}
	}
	
	/**
	 * Moves this robot to a neighboring node.
	 * 
	 * @param 	destination
	 * 			The node to move to.
	 * @pre		The given node's position is inside the dimensions of this robot's board.
	 * 			| getBoard().isInsideDimensions(destination.getPosition())
	 * @pre		The given node has a position equal to this robot's position or the given node's position is equal to this
	 * 			robot's position after moving, and has the same orientation as this robot.
	 * 			| destination.getPosition().equals(this.getPosition())
	 * 			|	|| (destination.getPosition().equals(this.getPositionAfterMove()) 
	 * 			|		&& destination.getOrientation() == this.getOrientation())
	 * @pre		If the given node is in front of this robot, this robot must be able to move.
	 * 			| if(destination.getPosition().equals(this.getPositionAfterMove()))
	 * 			|	then canMove()
	 * @pre		This robot has enough energy to move/turn as much as needed.
	 * 			| if(destination.getPosition().equals(this.getPositionAfterMove())
	 * 			| 	then this.getEnergy().compareTo(getEnergyRequiredToMove()) >= 0
	 * 			| else
	 * 			| 	for i from 0 to 3
	 * 			|		if(destination.getOrientation() == getOrientation().getOrientationAfterXTurns(i))
	 * 			|			then this.getEnergy().compareTo(getEnergyRequiredToTurn().multiply(i == 2 ? 2 : 1)) >= 0)
	 * @pre		This robot must be placed on a board.
	 * 			| isPlacedOnABoard()
	 * @post	This robot moves to the given node and ends up in the given node's orientation.
	 * 			| new.getOrientation() == destination.getOrientation()
	 * 			| new.getPosition() == destination.getPosition()
	 */
	private void moveTo(Node destination){	
		assert(isPlacedOnABoard()) : "Precondition: The robot must be placed on a board.";
		assert(getBoard().isInsideDimensions(destination.getPosition()));
		try {
			assert(destination.getPosition().equals(this.getPosition()) || (destination.getPosition().equals(this.getPositionAfterMove()) && destination.getOrientation() == this.getOrientation()));
		} catch (OutOfDimensionException e1) {
			e1.printStackTrace();
		}
		if(destination.equals(new Node(getPosition(), getOrientation())))
			return;

		try {
			if(destination.getPosition().equals(getPositionAfterMove()))
				try {
					move();
				} catch (OutOfDimensionException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IllegalPositionException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		} catch (OutOfDimensionException e) {
			//This robot does nothing if it cannot move.
			//(in this case because it would walk out off the board)
		}

		if(destination.getOrientation() == this.getOrientation().getOrientationAfterXTurns(3)){
			turn(Direction.COUNTERCLOCKWISE);
		}
		else if(destination.getOrientation() == this.getOrientation().getOrientationAfterXTurns(2)){
			turn();turn();
		}
		else if(destination.getOrientation() == this.getOrientation().getOrientationAfterXTurns(1)){
			turn();
		}
	}

	
	/**
	 * Transfers all the possessions of this robot to an adjacent robot.
	 * 
	 * @param	other
	 * 			The robot to transfer possessions to.
	 * @pre		This robot is standing adjacent to the given robot.
	 * 			(This implies that both robots are on the same board.)
	 * 			| board.getDistanceBetween(this, other) == 1
	 * @post	All possessions of this robot are transfered to the given robot.
	 * 			| for each item in getPossessions()
	 * 			| 	new.other.hasAsPossession(item)
	 */
	public void transferPossessionsTo(Robot other){
		assert(Board.getDistanceBetween(this, other) == 1) : "This robot is adjacent to the given robot.";
		for(Item item : getPossessions()){
			removePossession(item);
			other.addPossession(item);
		}
	}
	
	
	/**
	 * Return the number of items this robot has in possession.
	 * 
	 * @return	the number of items this robot has in possession.
	 * 			| possessions.size()
	 */
	@Basic
	@Raw
	public int getNbPossessions() {
		return possessions.size();
	}

	/**
	 * Returns a clone of the list of items this robot has in possession.
	 * 
	 * @return	clone of the list of items this robot has in possession.
	 * 			| for each possession in Robot
	 * 			|	(result.contains(possession) ==
	 * 			|		this.hasAsPossession(possession))
	 */
	@Basic
	public List<Item> getPossessions() {
		return new ArrayList<Item>(possessions);
	}

	/**
	 * Check whether this content has the given entity as one of its entities.
	 * 
	 * @param  item
	 * 		   The item to check.
	 * @return The given item is registered at some position as
	 *         a possession of this robot.
	 *       	| for some I in 1..getNbPossessions():
	 *       	| 	possessions.get(I) == entity
	 */
	public boolean hasAsPossession(@Raw Item item) {
		return possessions.contains(item);
	}
	
	/**
	 * Looks for the I'th heaviest item in possession.
	 * 
	 * @param	i
	 * 			The given index. If i equals 1, the the heaviest item will be returned.
	 * 			If i equals the amount of possessions carried by this robot, the lightest item
	 * 			will be returned.
	 * @pre		The given index is greater than zero.
	 * 			| i > 0
	 * @return 	the i-th heaviest element this robot is carrying.
	 * 			| result == possessions.get(getNbPossessions() - i)
	 * @throws	NoSuchElementException
	 * 			| (i > getNbPossessions())
	 */
	public Item getIthHeaviestPossession(int i){
		assert(i > 0);
		if(i > getNbPossessions())
			throw new NoSuchElementException();
		return possessions.get(getNbPossessions() - i);
	}

	/**
	 * Returns the amount of weight the robot is carrying.
	 * 
	 * @return	the amount of weight the robot is carrying.
	 * 			| let
	 * 			| weight == new WeightAmount(0)
	 * 			|	for each possession in getPossessions()
	 * 			|		weight == weight.add(Possession.getWeight())
	 * 			|	result == weight
	 */
	public WeightAmount getCarriedWeight(){
		WeightAmount weight = new WeightAmount(0);
		for(Item Possession : getPossessions()){
			weight = weight.add(Possession.getWeight());
		}
		return weight;
	}

	/**
	 * Let the robot pick up an item.
	 * 
	 * @param 	item 
	 * 			The item to pick up.
	 * @pre		This robot must be placed on a board.
	 * 			| isPlacedOnABoard()
	 * @pre		This robot can have the given item as item.
	 * 			| canHaveAsPossession(item)
	 * @post	If this item is on the same position and board as the robot, this item will be removed from the board,
	 * 			and added to the possessions of this robot.
	 * 			| if(item.getPosition().equals(getPosition()) || item.getBoard() != this.getBoard()) then
	 * 			|	new.getPossessions().contains(item)
	 * 			|	new.item.isPlacedOnABoard() == false
	 * @post	If this item is not on the same position and board as the robot, nothing will happen.
	 * 			| if(!item.getPosition().equals(getPosition()) || item.getBoard() != this.getBoard())
	 * 			|	new.getPossessions() == getPossessions()
	 */
	public void pickUp(Item item){
		assert(isPlacedOnABoard()) : "Precondition: The robot must be placed on a board.";
		assert(canHaveAsPossession(item)) : "Precondition: This robot can have the given item";
		if(!item.getPosition().equals(getPosition()) || item.getBoard() != this.getBoard())
			return;

		item.remove();
		addPossession(item);
	}

	/**
	 * Recharges the robot with a battery he picked up.
	 * 
	 * @param 	battery
	 * 			One of the batteries the robot picked up.
	 * @pre		The given battery is possessed by this robot.
	 * 			| hasAsPossession(battery)
	 * @post	If the given battery is fully discharged after charging this robot,
	 * 			the given battery will be terminated.
	 * 			| let maxReplenishable = 
	 * 			|		getMaximumEnergy().subtract(this.getEnergy())
	 * 			| if(maxReplenishable >= battery.getEnergy()) then
	 * 			|		new.getEnergy().equals(getEnergy().add(battery.getEnergy()))
	 * 			|		new.hasAsPossession(battery) == false
	 * 			|		new.battery.isTerminated() == true
	 * @post	If the battery is not fully discharged after charging this robot,
	 * 			the given battery's energy will be discharged with the maximum replenishable
	 * 			energy of this robot.
	 * 			| if(maxReplenishable < battery.getEnergy()) then
	 * 			|	new.getEnergy().equals(Robot.getMaximumEnergy())
	 * 			|	new.battery.getEnergy().equals(battery.getEnergy().subtract(maxReplenishable))
	 */
	public void use(Battery battery){
		assert(hasAsPossession(battery)) : "Precondition: The given battery is possessed by this robot.";

		EnergyAmount maxReplenishable = getMaximumEnergy().subtract(this.getEnergy());
		if(maxReplenishable.compareTo(battery.getEnergy()) >= 0){
			this.recharge(battery.getEnergy());
			battery.discharge(battery.getEnergy());
			this.removePossession(battery);
			battery.terminate();
		}
		else
		{
			this.recharge(maxReplenishable);
			battery.discharge(maxReplenishable);
		}
	}

	/**
	 * Repairs the robot with a repair kit he picked up.
	 * 
	 * @param 	repairKit
	 * 			One of the repair kits the robot picked up.
	 * @pre		The given repair kit is possessed by this robot.
	 * 			| hasAsPossession(repairKit)
	 * @post	If the given repairKit is fully discharged after charging this robot,
	 * 			the given battery will be terminated.
	 * 			| let maxReplenishable = 
	 * 			|		getMaximumEnergy().subtract(this.getMaximumEnergy())
	 * 			| if(maxReplenishable.compareTo(repairKit.getEnergy().divide(2)) >= 0) then
	 * 			|		new.getMaximimumEnergy().equals(getEnergy().add(repairKit.getEnergy().divide(2)))
	 * 			|		new.hasAsPossession(repairKit) == false
	 * 			|		new.repairKit.isTerminated() == true
	 * @post	If the repairKit is not fully discharged after charging this robot,
	 * 			the given repairKit's energy will be discharged with twice the maximum replenishable
	 * 			energy of this robot.
	 * 			| if(maxReplenishable.compareTo(repairKit.getEnergy().divide(2)) < 0) then
	 * 			|	new.getMaximumEnergy().equals(Robot.getMaximumMaximumEnergy())
	 * 			|	new.repairKit.getEnergy().equals(repairKit.getEnergy().subtract(maxReplenishable.multiply(2)))
	 */
	public void use(RepairKit repairKit){
		assert(hasAsPossession(repairKit)) : "Precondition: The given battery is possessed by this robot.";

		EnergyAmount maxReplenishable = getMaximumMaximumEnergy().subtract(this.getMaximumEnergy());
		if(maxReplenishable.compareTo(repairKit.getEnergy().divide(2)) >= 0){
			this.repair(repairKit.getEnergy().divide(2));
			repairKit.discharge(repairKit.getEnergy());
			this.removePossession(repairKit);
			repairKit.terminate();
		}
		else
		{
			this.repair(maxReplenishable);
			repairKit.discharge(maxReplenishable.multiply(2));
		}
	}
	
	/**
	 * Use the given surprise box.
	 * 
	 * @param 	surpriseBox
	 * 			One of the surprise boxes the robot picked up.
	 * @pre		The given surprise box is possessed by this robot.
	 * 			| hasAsPossession(surpriseBox)
	 * @effect	If the given suprisebox contains an explosion
	 * 			then this robot gets hit.
	 * 			| If supriseBox.getSurprise() == Surprise.EXPLODE
	 * 			|	then getsHit()
	 * @effect	If the given suprisebox contains a teleportation	 
	 * 			then this robot is teleported to a random position.
	 * 			| If supriseBox.getSurprise == Surprise.TELEPORTATION
	 * 			|	then teleport()
	 * @effect	If the given surprisebox contains another surprisebox
	 * 			then this robot receives a new suprisebox with the same weight as the used suprisebox.
	 * 			| If surprisebox.getSurprise() == Surprise.SUPRISEBOX
	 * 			|	then this.addPossession(new SurpriseBox(surpriseBox.getWeight()))
	 * @effect	If the given surprisebox contains a battery
	 * 			then this robot receives a new battery with the same weight as the used suprisebox
	 * 			and an energy equal to 5000Ws.
	 * 			| If surprisebox.getSurprise() == Surprise.BATTERY
	 * 			|	then this.addPossession(new Battery(new EnergyAmount(5000), surpriseBox.getWeight()))
	 * @effect	If the given surprisebox contains a repairkit
	 * 			then this robot receives a new repairkit with the same weight as the used suprisebox
	 * 			and an energy equal to the maximum maximum energy of any robot.
	 * 			| If surprisebox.getSurprise() == Surprise.REPAIRKIT
	 * 			|	then this.addPossession(new RepairKit(Robot.getMaximumMaximumEnergy(), surpriseBox.getWeight()))
	 * @effect	The used surprisebox is terminated.
	 * 			| surpriseBox.terminate()
	 */
	public void use(SurpriseBox surpriseBox){
		assert(hasAsPossession(surpriseBox)) : "Precondition: The given suprise box is possessed by this robot.";
		switch(surpriseBox.getSurprise()){
		case 	EXPLODE: this.getsHit();	
		break;
		case 	TELEPORTATION: this.teleport();
		break;
		case 	SURPRISEBOX: this.addPossession(new SurpriseBox(surpriseBox.getWeight()));
		break;
		case 	REPAIRKIT: this.addPossession(new RepairKit(Robot.getMaximumMaximumEnergy(), surpriseBox.getWeight()));
		break;
		case 	BATTERY: this.addPossession(new Battery(new EnergyAmount(5000), surpriseBox.getWeight()));
		break;
		}
		
		this.removePossession(surpriseBox);
		surpriseBox.terminate();
	}
	
	/**
	 * Let this robot drop an item.
	 * 
	 * @pre		This item is possessed by this robot.
	 * 			| hasAsPossession(item)
	 * @pre		This item can be put on the position of this robot.
	 * 			| item.canBePutOn(getBoard(), getPosition())
	 * @post	The given item is placed on the same board and position as this robot.
	 * 			| item.getPosition().equals(this.getPosition()) && item.getBoard() == this.getBoard()
	 * @param 	item
	 * 			One of the items the robot picked up.
	 */
	public void drop(Item item){
		assert(hasAsPossession(item)) : "Precondition: This item is possessed by this robot.";
		assert(item.canBePutOn(getBoard(), getPosition()));
		try {
			possessions.remove(item);
			item.put(getPosition(), getBoard());
		} catch (IllegalPositionException e) {
			//This robot would have to be on an illegal position for this exception to happen.
			System.err.println(e.toString());
		}
	}

	/**
	 * Checks if this robot can have the given item as possession.
	 * 
	 * @return If this robot is terminated, true if and only if the
	 *         given item is not effective.
	 *       | if (this.isTerminated())
	 *       |   then result == (item == null)
	 * @return If this robot is not terminated, true if and only if the given
	 *         item is effective and not yet terminated.
	 *       | if (! this.isTerminated())
	 *       |   then result ==
	 *       |     (item  != null) &&
	 *       |     (! item.isTerminated())
	 */
	public boolean canHaveAsPossession(Item item){
		if (this.isTerminated())
			return item == null;
		return (item != null)
				&& (!item.isTerminated());
	}

	/**
	 * Adds the given item to this robot's possessions.
	 * 
	 * @param 	item
	 * 			The item to be added to the list of possessions
	 * @pre		The given item is possible to be possessed by this robot.
	 * 			| canHaveAsPossession(item)
	 * @pre		The given item isn't already possessed.
	 * 			| !this.hasAsPossession(item)
	 * @pre		The given item is not placed on a board.
	 * 			| !isPlacedOnABoard(item)
	 * @post	The given item is added to the possessions of this robot.
	 * 			| new.getPossessions().contains(item)
	 * @effect	This list gets sorted.
	 * 			| Collections.sort(possessions)
	 */
	@Raw
	private void addPossession(Item item) {
		assert (canHaveAsPossession(item) && !this.hasAsPossession(item) && !item.isPlacedOnABoard());
		possessions.add(item);
		Collections.sort(possessions);

	}

	/**
	 * Remove the given item from the list of possessions of this robot.
	 * 
	 * @param  item
	 *         The item to be removed.
	 * @pre    The given item is effective and this robot has the
	 *         given item as one of its possessions.
	 *       	| (canHaveAsPossession(item)s) &&
	 *       	| this.hasAsPossession(item)
	 * @post   The number of possessions of this robot is
	 *         decremented by 1.
	 *       	| new.getNbPossessions() == getNbPossessions() - 1
	 * @post   this robot no longer has the given item as
	 *         one of its items.
	 *       	| ! new.hasAsPossesssion(item)
	 * @post   All possessions registered at an index beyond the index at
	 *         which the given item was registered, are shifted
	 *         one position to the left.
	 *       	| for each I,J in 1..getNbPossessions():
	 *       	|   if ( (getItemAt(I) == item) and (I < J) )
	 *      	|     then new.possessions.get(J-1) == possessions.get(J)
	 */
	@Raw
	private void removePossession(Item item) {
		assert (canHaveAsPossession(item)) && this.hasAsPossession(item);
		possessions.remove(item);
	}

	/**
	 * Variable referencing a list collecting all the possessions of this robot.
	 * 
	 * @invar	Each item registered in the referenced list is  effective and not yet terminated.  
	 * 			| for each entity in possessions:  
	 * 			|   ( (item != null) &&  
	 * 			|     (! item.isTerminated()) )
	 * @invar	No item is registered at several positions  in the referenced list.  
	 * 			| for each I,J in 0..possesions.size()-1:  
	 * 			|	  ( (I == J) ||  
	 * 			|     (possessions.get(I) != possessions.get(J))
	 */
	private List<Item> possessions = new ArrayList<Item>();

	/**
	 * Discharges this robot with the given energy.
	 * @param 	energy
	 * 			The amount of energy to discharge.
	 * @pre		The given energy is smaller than this robot's energy.
	 * 			| energy.compareto(getEnergy()) <= 0
	 * @post	The new energy of this robot is equal to it's old energy subtracted
	 * 			by the given energy.
	 * 			| new.getEnergy().equals(getEnergy().subtract(energy))
	 */
	@Model
	private void discharge(EnergyAmount energy){
		assert(energy.compareTo(getEnergy()) <= 0);

		setEnergy(getEnergy().subtract(energy));
	}
	
	/**
	 * Recharges this robot with the given amount of energy.
	 * 
	 * @pre		this robot can have the given energy as energy.
	 * 			| canHaveAsEnergy(amount)
	 * @pre		The amount of energy the robot should be recharged with added to the current amount of energy of the robot must be a valid amount. 
	 * 			| canHaveAsEnergy(getEnergy().add(amount))
	 * @param 	amount
	 * 			The amount of energy this robot should be recharged with.
	 * @effect	The amount of energy of this robot is set to the given amount of energy added to the initial amount of energy of the robot.
	 * 			| setEnergy(getEnergy().add(amount))
	 */
	@Model
	private void recharge(EnergyAmount amount){
		assert(canHaveAsEnergy(amount)) : "Precondition: this robot can have the given energy as energy.";
		assert(canHaveAsEnergy(amount.add(this.getEnergy()))) : "Class invariant: The energy of a robot must be valid.";
		setEnergy(getEnergy().add(amount));
	}

	/**
	 * Repairs this robot with the given amount of energy.
	 * 
	 * @pre		this robot can have the given energy as energy.
	 * 			| canHaveAsMaximumEnergy(amount)
	 * @pre		The amount of energy the robot should be recharged with added to the current amount of energy of the robot must be a valid amount. 
	 * 			| canHaveAsMaximumEnergy(getMaximumEnergy().add(amount))
	 * @param 	amount
	 * 			The amount of energy the robot should be repaired with.
	 * @effect	The amount of energy of the robot is set to the given amount of energy added to the initial amount of energy of the robot.
	 * 			| setMaximumEnergy(getMaximumEnergy().add(amount))
	 */
	public void repair(EnergyAmount amount){
		assert(canHaveAsMaximumEnergy(amount)) : "Precondition: this robot can have the given energy as energy.";
		assert(canHaveAsMaximumEnergy(amount.add(this.getMaximumEnergy()))) : "Class invariant: The energy of a robot must be valid.";
		setMaximumEnergy(getMaximumEnergy().add(amount));
	}
	
	/**
	 * This method defines if it is possible for this robot to teleport.
	 * 
	 * @return	True if and only if this robot has an (other) place to teleport to.
	 * 			| for i from 0 to getBoard().getWidth()
	 * 			|	for j from 0 to getBoard().getHeight()
	 * 			|		if(this.canBePutOn(new Position(i,j))) then return true
	 * 			| return false
	 */
	@Model
	private boolean canTeleport(){
		for(long i = 0; i <= getBoard().getWidth(); i++){
			for(long j = 0; j <= getBoard().getHeight(); j++){
				try {
					if(this.canBePutOn(new Position(i,j))) return true;
				} catch (OutOfDimensionException e) {
				}
				if(i == getBoard().getWidth() && j == getBoard().getWidth()){
					return false;
				}
			}
		}
		return false;
	}
	/**
	 * Teleports this robot to a random position.
	 * 
	 * @pre		This robot is placed on a board.
	 * 			| isPlacedOnABoard()
	 * @post	This robot is teleported if possible
	 * 			| if(canTeleport())
	 * 			|	new.getPosition() != getPosition()
	 * @effect	This robot is not teleported if not possible.
	 * 			| if(!canTeleport())
	 * 			|	new.getPosition() == getPosition()
	 */
	public void teleport(){
		assert(isPlacedOnABoard()) : "This robot is placed on a board.";
		if(!canTeleport()) return;
		
		long randX = (Math.round(getBoard().getWidth()*Math.random()));
		long randY = (Math.round(getBoard().getHeight()*Math.random()));
		try {					
			Position destination;

			destination = new Position(randX,randY);

			while(!canBePutOn(destination)){
				randX = (Math.round(getBoard().getWidth()*Math.random()));
				randY = (Math.round(getBoard().getHeight()*Math.random()));

				destination = new Position(randX,randY);
			}
		}
		catch(OutOfDimensionException e){
			e.printStackTrace();
		}

		try {
			replace(new Position(randX,randY));
		} catch (IllegalPositionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (OutOfDimensionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Let this robot shoot another entity.
	 * 
	 * @pre		The amount of energy of this robot should be greater than or equal to the energy needed to shoot.
	 * 			| hasEnoughEnergyToShoot()
	 * @effect	This robot loses the the amount of energy required to shoot.
	 * 			| discharge(getEnergyRequiredToShoot())
	 * @post	The entity in front of this robot gets hit.
	 * 			| let nextPosition = getPositionAfterMove()
	 * 			| while (getBoard().getNbEntitiesOnPosition(nextPosition) == 0 && getBoard().isInsideDimensions(nextPosition))
	 * 			|	nextPosition = getPositionAfterMove(nextPosition, getOrientation())
	 * 			| if(getBoard().getNbEntitiesOnPosition(nextPosition) > 0)
	 * 			|	new.board.getEntitiesOnPosition(nextPosition).get(0).getsHit()
	 */
	public void shoot(){
		assert(isPlacedOnABoard()) : "Precondition: The robot must be placed on a board.";
		assert(hasEnoughEnergyToShoot()) : "Precondition: This robot has enough energy to shoot.";
		discharge(getEnergyRequiredToShoot());

		Position nextPosition;
		try {
			nextPosition = getPositionAfterMove();
		} catch (OutOfDimensionException e) {
			return;
		}

		while(board.getNbEntitiesOnPosition(nextPosition) == 0){
			try {
				if(getBoard().isInsideDimensions(getPositionAfterMove(nextPosition, getOrientation())))
					nextPosition = getPositionAfterMove(nextPosition, getOrientation());
				else
					return;
			} catch (OutOfDimensionException e) {
				return;
			}
		}
		board.getEntitiesOnPosition(nextPosition).get(0).getsHit();

	}
	
	/**
	 * Executes the next n steps of this robot's program. That is, this method executes the program until it
	 * encounters n basic commands or until it reaches a final state.
	 * 
	 * @param	n
	 * 			The amount of basic commands to execute.
	 * @pre		This robot has a program stored.
	 * 			| hasProgram()
	 * @pre		n is a positive number
	 * 			| n >= 0
	 * @pre		Any program can be executed by this robot
	 * 			| Program.canBeExecutedBy(this)
	 */
	public void stepN(int n){
		assert(hasProgram());
		assert(Program.canBeExecutedBy(this));
		assert(n>0);
		for(int i = 0; i < n; i++)
			if(!isTerminated())
				getProgram().step(this);
			else
				break;
	}
	
	/**
	 * Loads the given program.
	 * @param	path
	 * 			The path to the file containing the program to load.
	 * @post	A new program, loaded from the given path, is stored in this robot.
	 * 			| new.getProgram().equals(new Program(this, path))
	 * @throws 	IOException
	 * 			An error occurred while trying to read the given file.
	 * @throws	UnparsableException
	 * 			There was an error in the given code.
	 */
	public void loadProgram(String path) throws IOException, UnparsableException {
		this.program = new Program(this, path);
	}
	
	/**
	 * Unloads the program stored by this robot.
	 * @post	This robot stores no program
	 * 			| ! new.hasProgram()
	 */
	public void unloadProgram(){		
		this.program = null;
	}
	
	/**
	 * Returns the program carried by this robot.
	 * @return 	the program carried by this robot.
	 * 			| result == program
	 */
	@Basic
	public Program getProgram() {
		return program;
	}
	
	/**
	 * Checks if this robot can have the given program as program.
	 * @param 	program
	 * 			The program to check
	 * @return	If this robot is terminated, true if and only if the given program is not effective.
	 * 			| if(isTerminated()) then result == (program == null)
	 * @return	Else, always true.
	 * 			| result == true
	 */
	@Model
	private boolean canHaveAsProgram(Program program){
		if(isTerminated())
			return (program == null);
		return true;
	}
	
	/**
	 * Checks if this robot has a program stored.
	 * @return	True if this robot has a program stored.
	 * 			| result == (program != null)
	 */
	public boolean hasProgram(){
		return program != null;
	}
	
	/**
	 * A variable representing a program that can be stored and executed by this robot.
	 */
	private Program program;
	
	
	/**
	 * Terminate this robot, breaking the association with
	 * all its possessions.
	 *
	 * @effect	This robot is removed from the board.
	 * 			| super.terminate()
	 * @post	This entity is terminated
	 * 			| new.isTerminated()
	 * @post	All possessions are taken away from this robot.
	 * 			| new.getNbPossessions() == 0
	 * @effect	The program stored in this robot is unloaded.
	 * 			| unloadProgram()
	 */
	@Override
	public void terminate(){
		unloadProgram();
		super.terminate();
		possessions.clear();
	}
	
	/**
	 * This method defines how this robot reacts to being hit by a laser.
	 * @post	If this robot's maximum energy exceeds 4000Ws then this robot's maximum energy will be decreased by 4000Ws.
	 * 			| if(getMaximumEnergy().compareTo(new EnergyAmount(4000)) > 0) then
	 *			|	new.getMaximumEnergy().equals(getMaximumEnergy().subtract(new EnergyAmount(4000)))
	 * @post	Else this robot will be TERMINATED!
	 * 			| else new.isTerminated();		
	 */
	@Override
	public void getsHit(){
		if(getMaximumEnergy().compareTo(new EnergyAmount(4000)) > 0)
			setMaximumEnergy(getMaximumEnergy().subtract(new EnergyAmount(4000)));
		else
			terminate();		
	}

	/**
	 * Checks whether this robot can be put on the given board and given position.
	 * 
	 * @param 	board
	 * 			The board to put the entity on.
	 * @param 	position
	 * 			The position to put the entity on.
	 * @return	true if and only if this entity can be put on the given board and given position.
	 *			| result == (board.canHaveAsEntity(this) && 
	 *			|	(board.isInsideDimensions(position) && canHaveAsBoard(board))
	 *			|	 	&& (board.getNbWallsOnPosition(position) == 0 && board.getNbRobotsOnPosition(position) == 0)
	 */			
	@Override
	public boolean canBePutOn(Board board, Position position){
		if(canHaveAsBoard(board) && canHaveAsPosition(position) && board.canHaveAsEntity(this) && (board.isInsideDimensions(position))){
			return (board.getNbWallsOnPosition(position) == 0 && board.getNbRobotsOnPosition(position) == 0);
		}
		return false;
	}
	
	/**
	 * Returns a string representation of this robot.
	 */
	@Override
	public String toString(){
		String stringRepresentation = "Robot: (energy: " + getEnergy() + ", maximum energy: " + getMaximumEnergy() + ", orientation: " + getOrientation()
				+ ", carrying: " + getCarriedWeight();
		if(isPlacedOnABoard())
			stringRepresentation += ", position: " + getPosition().toString();
		stringRepresentation += ")";
		return stringRepresentation;

	}
}	
