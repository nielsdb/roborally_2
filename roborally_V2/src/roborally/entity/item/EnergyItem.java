package roborally.entity.item;

import roborally.entity.Energetic;
import roborally.value.EnergyAmount;
import roborally.value.WeightAmount;
import be.kuleuven.cs.som.annotate.Basic;
import be.kuleuven.cs.som.annotate.Immutable;
import be.kuleuven.cs.som.annotate.Model;
import be.kuleuven.cs.som.annotate.Raw;

/**
 * A class representing items having a certain amount of energy.
 *
 * @invar	each energy item can have its energy as energy.
 * 			| canHaveAsEnergy(getEnergy())
 * 
 * @version	1.0
 * @author 	Niels De Bock, Michael Vincken (Bachelor of Computer Science)
 */

public abstract class EnergyItem extends Item implements Energetic {
	/**
	 * Initializes this new Energy-item with a given amount of energy and given weight.
	 * 
	 * @param 	energy
	 * 			The amount of energy stored in this new battery.
	 * @param 	weight
	 * 			The weight for this new battery.
	 * @param 	maximumEnergy
	 * 			The maximum energy of this new energy-item.
	 * @pre		This item must be able to have the given weight as weight.
	 * 			| isValidWeight(weight)
	 * @effect	| setEnergy(energy)
	 * @post	| new.getMaximumEnergy() == maximumEnergy
	 */
	protected EnergyItem(EnergyAmount energy, WeightAmount weight, EnergyAmount maximumEnergy){
		super(weight);
		
		this.MAXIMUM_ENERGY = maximumEnergy;
		setEnergy(energy);
	}
	
	/**
	 * @return	the energy of this energy-item.
	 * 			| result == energy
	 */
	@Basic
	public EnergyAmount getEnergy(){
		return energy;
	}

	/**
	 * 
	 * @param 	energy
	 * 			The energy to check.
	 * @return	| energy != null && energy.compareTo(getMaximumEnergy()) <= 0
	 */
	public boolean canHaveAsEnergy(EnergyAmount energy){
		return energy != null && energy.compareTo(getMaximumEnergy()) <= 0;
	}
	
	/**
	 * @return the maximum amount of energy of this energy-item.
	 */
	@Model @Immutable @Basic
	private EnergyAmount getMaximumEnergy(){
		return MAXIMUM_ENERGY;
	}

	/**
	 * Constant reflecting for the highest possible value for the energy of this energy-item.
	 */
	private final EnergyAmount MAXIMUM_ENERGY;	
	

	/**
	 * Sets the energy of this energy-item to the given energy.
	 * @param 	energy     
	 * 			The new energy for this energy-item.
	 * @pre     The given energy is valid for this energy-item.  
	 * 			| energy.compareTo(getMaximumEnergy()) <= 0
	 * @post    | new.getEnergy() = energy
	 */
	protected void setEnergy(EnergyAmount energy){
		this.energy = energy;
	}

	/**
	 * a variable registering the energy of this energy-item.
	 */
	public EnergyAmount energy;

	/**
	 * Discharges this energy-item with the given energy.
	 * @param 	energy
	 * 			The amount of energy to discharge.
	 * @pre		| energy.compareto(getEnergy()) <= 0
	 * @post	| new.getEnergy().equals(getEnergy().subtract(energy))
	 */
	public void discharge(EnergyAmount energy){
		assert(energy.compareTo(getEnergy()) <= 0);

		setEnergy(getEnergy().subtract(energy));
	}
	
	/**
	 * Recharges this energy-item with the given amount of energy.
	 * 
	 * @pre		this energy-item can have the given energy as energy.
	 * 			| isValidEnergy(amount)
	 * @pre		The amount of energy the robot should be recharged with added to the current amount of energy of the robot must be a valid amount. 
	 * 			| isValidEnergy(getEnergy().add(amount))
	 * @param 	amount
	 * 			The amount of energy this energy-item should be recharged with.
	 * @effect	The amount of energy of this energy-item is set to the given amount of energy added to the initial amount of energy of the robot.
	 * 			| setEnergy(getEnergy().add(amount))
	 */
	public void recharge(EnergyAmount amount){
		assert(canHaveAsEnergy(amount)) : "Precondition: this energy-item can have the given energy as energy.";
		assert(canHaveAsEnergy((amount.add(this.getEnergy())))) : "Class invariant: The energy of an energy-item must be valid.";
		setEnergy(getEnergy().add(amount));
	}

	
	/**
	 * @post	| If(getEnergy().add(new EnergyAmount(500)).compareTo(getMaximumEnergy()) <= 0)
	 * 			| 	new.getEnergy().equals(getEnergy().add(new EnergyAmount(500)))
	 * 			| else
	 * 			|	new.getEnergy().equals(getMaximumEnergy())
	 */
	@Override
	public void getsHit(){
		if(getEnergy().add(new EnergyAmount(500)).compareTo(getMaximumEnergy()) <= 0)
			recharge(new EnergyAmount(500));
		else
			recharge(getMaximumEnergy().subtract(getEnergy()));
	}
	
	/**
	 * @effect	| super.terminate()
	 * @post	This entity is terminated
	 * 			| new.isTerminated()
	 */
	@Override
	public void terminate(){
		super.terminate();
	}
}
