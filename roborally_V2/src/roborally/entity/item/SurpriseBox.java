package roborally.entity.item;

import java.util.HashSet;
import java.util.Set;

import roborally.entity.Entity;
import roborally.exception.OutOfDimensionException;
import roborally.position.Position;
import roborally.value.WeightAmount;
import be.kuleuven.cs.som.annotate.Basic;
import be.kuleuven.cs.som.annotate.Immutable;
import be.kuleuven.cs.som.annotate.Model;

/**
 * A class for representing surprise boxes which have a random surprise.
 * 
 * @invar	each surprise box has a random surprise.
 * 			| canHaveAsSurprise(getSurprise())
 * 
 * @version	1.0
 * @author 	Niels De Bock, Michael Vincken (Bachelor of Computer Science)
 */

public class SurpriseBox extends Item {
	/**
	 * Initializes this surprise box with a given surprise and weight.
	 * @param	weight
	 * 			The weight for this new surpriseBox
	 * @param	surprise
	 * 			The surprise inside this new surpriseBox.
	 * @effect	| super(new WeightAmount(0))
	 * @post	| new.getSurprise() == surprise
	 * @note	Any new Surprise Box initialized with this constructor will satisfy all its class invariants.
	 */
	public SurpriseBox(WeightAmount weight, Surprise surprise) {
		super(weight);
		this.surprise = surprise;
	}
	
	/**
	 * Initializes this surprise box with a random surprise and given weight.
	 * @effect	| this(weight, Surprise.getRandomSurprise())
	 * @post	| new.canHaveAsSurprise(getSurprise())
	 * @note	Any new Surprise Box initialized with this constructor will satisfy all its class invariants.
	 */
	public SurpriseBox(WeightAmount weight) {
		this(weight, Surprise.getRandomSurprise());
	}


	/**
	 * @return | result == surprise
	 */
	@Basic @Immutable
	public Surprise getSurprise(){
		return surprise;
	}
	
	/**
	 * Returns a boolean reflecting whether this surprise box can have the given surprise as surprise.
	 * @param 	surprise
	 * 			The surprise to check.
	 * @return	true if and only if the given surprise is not null.
	 * 			| result == (surprise != null)
	 */
	@Model
	private boolean canHaveAsSurprise(Surprise surprise){
		return (surprise != null);
	}
	
	
	/**
	 * A variable containing the surprise in this surprise box.
	 */
	private final Surprise surprise;


	/**
	 * When a surprise is hit by a robot, it explodes and hits all items located on adjacent squares.
	 * @pre	| isPlacedOnABoard()
	 */
	@Override
	public void getsHit(){
		assert(isPlacedOnABoard());
		
		Set<Entity> toBeHit = new HashSet<Entity>();
		
		try {
			Position right = new Position(position.getX()+1, position.getY());
			if(board.isPositionInUse(right))
				toBeHit.addAll(board.getEntitiesOnPosition(right));
		} catch (OutOfDimensionException e) {
		}
		
		try {
			Position left = new Position(position.getX()-1, position.getY());
			if(board.isPositionInUse(left))
				toBeHit.addAll(board.getEntitiesOnPosition(left));
		} catch (OutOfDimensionException e) {
		}		
		
		try {
			Position up = new Position(position.getX(), position.getY()-1);
			if(board.isPositionInUse(up))
				toBeHit.addAll(board.getEntitiesOnPosition(up));
		} catch (OutOfDimensionException e) {
		}					
		
		try {
			Position down = new Position(position.getX(), position.getY()+1);
			if(board.isPositionInUse(down))
				toBeHit.addAll(board.getEntitiesOnPosition(down));
		} catch (OutOfDimensionException e) {
		}

		this.terminate(); 
		for(Entity hitThis : toBeHit){
			if(!hitThis.isTerminated())
				hitThis.getsHit();
		}
	}
	

	
	@Override
	public String toString(){

		String stringRepresentation = "Surprise box: (surprise: " + getSurprise() + ", weight: " + getWeight();
		if(isPlacedOnABoard())
			stringRepresentation += ", position: " + getPosition().toString();
		stringRepresentation += ")";
		return stringRepresentation;
	}

	
}
