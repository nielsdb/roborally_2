package roborally.entity.item;

import roborally.value.EnergyAmount;
import roborally.value.WeightAmount;
import be.kuleuven.cs.som.annotate.Basic;
import be.kuleuven.cs.som.annotate.Immutable;
import be.kuleuven.cs.som.annotate.Raw;

/**
 * A class for representing repair kits which have a certain amount of energy.
 * 
 * @version	1.0
 * @author 	Niels De Bock, Michael Vincken (Bachelor of Computer Science)
 *
 */
public class RepairKit extends EnergyItem {
	
	/**
	 * Initializes this new repair kit with a given amount of energy.
	 * 
	 * @param 	repairAmount
	 * 			The amount of energy stored in this new repair kit.
	 * @param	weight
	 * 			The weight of this new repair kit.
	 * @pre		This battery must be able to have the given energy as energy.
	 * 			| repairAmount.compareTo(getMaximumEnergy()) <= 0
	 * @pre		The given weight is valid for any repair kit.
	 * 			| isValidWeight(weight)
	 * @post	The energy of this new battery is equal to the given energy.
	 * 			| new.getEnergy() == energy
	 * @post	The energy of this new battery is equal to the given energy.
	 * 			| new.getWeight() == weight
	 * @effect	| super(energy, new WeightAmount(0), getMaximumEnergy())
	 * @note	Any new repair kit initialized with this constructor will satisfy all its class invariants.
	 * 
	 */
	@Raw
	public RepairKit(EnergyAmount repairAmount, WeightAmount weight){
		super(repairAmount, weight, getMaximumEnergy());
	}

	/**
	 * @return the maximum amount of energy of any repair kit.
	 */
	@Immutable @Basic
	public static EnergyAmount getMaximumEnergy(){
		return MAXIMUM_ENERGY;
	}

	/**
	 * Constant reflecting for the highest possible value for the energy of any repair kit.
	 * 
	 * @return  the highest value for the energy of any repair kit is the maximum value for an double.
	 * 			| result.equals(new EnergyAmount(Double.MAX_VALUE))
	 */
	public final static EnergyAmount MAXIMUM_ENERGY = new EnergyAmount(Double.MAX_VALUE);	
	
	@Override
	public String toString(){

		String stringRepresentation = "Repair kit: (energy: " + getEnergy() + ", weight: " + getWeight();
		if(isPlacedOnABoard())
			stringRepresentation += ", position: " + getPosition().toString();
		stringRepresentation += ")";
		return stringRepresentation;
	}

}
