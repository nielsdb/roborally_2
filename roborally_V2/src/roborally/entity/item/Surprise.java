package roborally.entity.item;

/**
 * Enumeration representing all possible Surprises in a Surprise Box.
 * @author 	Niels De Bock, Michael Vincken (Bachelor of Computer Science)
 *
 */
public enum Surprise {
	EXPLODE, TELEPORTATION, BATTERY, SURPRISEBOX, REPAIRKIT;
	
	public static Surprise getRandomSurprise(){
		return values()[(int) (Math.random()*values().length)];
	}
	
		
}
