package roborally.entity.item;

import roborally.value.EnergyAmount;
import roborally.value.WeightAmount;
import be.kuleuven.cs.som.annotate.Basic;
import be.kuleuven.cs.som.annotate.Immutable;
import be.kuleuven.cs.som.annotate.Raw;

/**
 * A class for representing batteries which have a certain amount of energy and a certain weight.
 * 
 * @version	1.0
 * @author 	Niels De Bock, Michael Vincken (Bachelor of Computer Science)
 *
 */
public class Battery extends EnergyItem {

	/**
	 * Initializes this new battery with a given amount of energy and given weight.
	 * 
	 * @param 	energy
	 * 			The amount of energy stored in this new battery.
	 * @param 	weight
	 * 			The weight for this new battery.
	 * @pre		This battery must be able to have the given energy as energy.
	 * 			| energy.compareTo(getMaximumEnergy()) <= 0
	 * @pre		This item must be able to have the given weight as weight.
	 * 			| isValidWeight(weight)
	 * @effect	| super(energy, weight, getMaximumEnergy())
	 * @note	Any new battery initialized with this constructor will satisfy all its class invariants.
	 * 
	 */
	@Raw
	public Battery(EnergyAmount energy, WeightAmount weight){
		super(energy, weight, getMaximumEnergy());
	}

	
	/**
	 * Gets the maximum amount of energy for any battery.
	 * 
	 * @return the maximum amount of energy for any battery.
	 */
	@Immutable @Basic
	public static EnergyAmount getMaximumEnergy(){
		return MAXIMUM_ENERGY;
	}

	/**
	 * Constant reflecting for the highest possible value for the energy of any battery.
	 * 
	 * @return  the highest value for the energy of any battery is 5000.
	 * 			| result.equals(new EnergyAmount(5000))
	 */
	public final static EnergyAmount MAXIMUM_ENERGY = new EnergyAmount(5000);	
	
	@Override
	public String toString(){

		String stringRepresentation = "Battery: (energy: " + getEnergy() + ", weight: " + getWeight();
		if(isPlacedOnABoard())
			stringRepresentation += ", position: " + getPosition().toString();
		stringRepresentation += ")";
		return stringRepresentation;
	}

}