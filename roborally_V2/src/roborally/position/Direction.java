package roborally.position;

/**
 * All possible directions in which a robot can turn.
 * 
 * @version	1.0
 * @author 	Niels De Bock, Michael Vincken (Bachelor of Computer Science)
 * 
 */
public enum Direction {
	CLOCKWISE, COUNTERCLOCKWISE
}
