package roborally.position;

/**
 * All possible orientations for a robot.
 * 
 * @version	1.0
 * @author 	Niels De Bock, Michael Vincken (Bachelor of Computer Science)
 * 
 */
public enum Orientation {
	UP, RIGHT, DOWN, LEFT;
	
	/**
	 * Get the orientation of a robot after X turns.
	 *  
	 * @param 	amount
	 * 			The given amount of turns the robot made.
	 * @pre		The given amount must be greater than or equal to 0 
	 * 			and smaller than or equal to Integer.MAX_VALUE. 
	 * 			| amount >= 0 && amount <= Integer.MAX_VALUE 	
	 * @return	the orientation of the robot after X turns. 
	 * 			| result == Orientation.values()[(this.ordinal()+ amount)%4]
	 * 
	 */
	public Orientation getOrientationAfterXTurns(int amount){
		assert(amount >= 0 && amount <= Integer.MAX_VALUE) : "Precondition: The given amount must be greater than or equal to 0 and smaller than or equal to Integer.MAX_VALUE.";
		return Orientation.values()[(this.ordinal() + amount) % 4];
	}
}
