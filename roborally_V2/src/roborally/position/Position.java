package roborally.position;

import roborally.exception.OutOfDimensionException;
import be.kuleuven.cs.som.annotate.Basic;

/**
 * 
 * @version 1.0
 * @author 	Niels De Bock, Michael Vincken (Bachelor of Computer Science)
 * 
 * @invar 	The X- and Y-coordinate of a position must be valid.
 * 			| isValidPosition(getXPosition(), getYPosition())
 */
public final class Position {
	/**
	 * Initializes this position with a given X- and Y-coordinate.
	 * @param 	x
	 * 			The initial X-coordinate for this position.
	 * @param 	y
	 * 			The initial Y-coordinate for this position.
	 * @throws	OutOfDimensionException
	 * 			The given position is out of bounds.
	 * 			| !isValidPosition(xPosition, yPosition)
	 */
	public Position(long x, long y) throws OutOfDimensionException{
		if(!Position.isValidPosition(x, y))
			throw new OutOfDimensionException(x,y);
		
		this.X = x;
		this.Y = y;
	}
	
	/**
	 * @return     	the X-coordinate of this position.
	 */
	@Basic
	public long getX() {
		return X;
	}

	/**
	 * Checks whether the given X-coordinate is a valid X-coordinate for any position.
	 * 
	 * @param 	x
	 * 			The X-coordinate to check.
	 * @return	true if and only if the given x-coordinate is greater than or equal to 0 and smaller or equal to the maximum value for a Long.
	 * 			| result == (xPosition >= 0 && xPosition <= Long.MAX_VALUE)
	 */
	private static boolean isValidX(long x){
		return x >= 0 && x <= Long.MAX_VALUE;
	}

	/**
	 * a variable registering the X-coordinate for this position.
	 */
	private final long X;


	/**
	 * @return     	the Y-coordinate of this position.
	 */
	@Basic
	public long getY() {
		return Y;
	}

	/**
	 * Checks whether the given Y-coordinate is a valid Y-coordinate for any position.
	 * 
	 * @param 	y
	 * 			The Y-coordinate to check.
	 * @return	true if and only if the given y-coordinate is greater than or equal to 0 and smaller or equal to the maximum value for a Long.
	 * 			| result == (y >= 0 && y <= Long.MAX_VALUE)
	 */
	private static boolean isValidY(long y){
		return y >= 0 && y <= Long.MAX_VALUE;
	}

	/**
	 * a variable registering the Y-coordinate for this position.
	 */
	private final long Y;
	

	/**
	 * Checks whether the given position(<code>x</code>, <code>y</code>) is a valid position.
	 * 
	 * @param 	x
	 * 			The X-coordinate of the position to check.
	 * @param	y
	 * 			The Y-coordinate of the position to check.
	 * @return	true if and only if the given position is valid.
	 * 			| result == (isValidX(x) && isValidY(y))
	 */
	public static boolean isValidPosition(long x, long y){
		return isValidX(x) && isValidY(y);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (X ^ (X >>> 32));
		result = prime * result + (int) (Y ^ (Y >>> 32));
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Position other = (Position) obj;
		if (X != other.X)
			return false;
		if (Y != other.Y)
			return false;
		return true;
	}
	
	@Override
	public String toString(){
		return "(" + getX() + ", " + getY() + ")";
	}
}
