package roborally.position;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Set;

import roborally.entity.Entity;
import roborally.entity.Robot;
import roborally.entity.Wall;
import roborally.entity.item.Battery;
import roborally.entity.item.Item;
import roborally.entity.item.RepairKit;
import roborally.entity.item.SurpriseBox;
import roborally.exception.IllegalDimensionException;
import roborally.exception.IllegalPositionException;
import roborally.exception.OutOfDimensionException;
import roborally.filter.Filter;
import be.kuleuven.cs.som.annotate.Basic;
import be.kuleuven.cs.som.annotate.Immutable;
import be.kuleuven.cs.som.annotate.Model;
import be.kuleuven.cs.som.annotate.Raw;

/**
 * A class for representing two-dimensional boards which have a certain height and a certain width.
 * 
 * @invar	The dimension of a board must be valid.
 * 			| isValidDimension(getHeight(), getWidth())
 * @invar	All entities on this board are placed inside the dimensions of the board.
 * 			| for each position in getPositionsInUse()
 * 			| 	isInsideDimensions(position)
 * @invar	| for each entity in getAllEntities()
 * 			|	canHaveAsEntity(entity)
 * @invar	All entities on this board have a reference back to the board and position they're on.
 * 			| for each position in getPositionsInUse()
 * 			| 	for each entity in getEntitiesOnposition(position)
 * 			|		entity.getPosition() == position && entity.getBoard() == this
 * 			
 * @version	1.0
 * @author 	Niels De Bock, Michael Vincken (Bachelor of Computer Science)
 *
 */
public class Board{

	/**
	 * Initialize this new two-dimensional board with a given height and width.
	 * 
	 * @param 	height
	 * 			The height for this new board.
	 * @param 	width
	 * 			The width for this new board.
	 * @post	The height of this new board is equal to the given height.
	 * 			| new.getHeight() == height
	 * @post	The width of this new board is equal to the given width.
	 * 			| new.getWidth() == width
	 * @throws 	IllegalDimensionException
	 * 			The give dimensions are not valid dimensions for any board.
	 * 			| !isValidBoard(height, width)
	 * @note	Please keep in mind that a board will have height + 1 rows and
	 * 			width + 1 columns;
	 * @note	Any new board initialized with this constructor will satisfy all its class invariants.
	 * 
	 */
	public Board(long height, long width) throws IllegalDimensionException {
		if(!isValidDimension(height, width)) throw new IllegalDimensionException(height, width);
		this.height = height;
		this.width = width;
	}

	/**
	 * @return	the height of this board.
	 */
	@Basic @Immutable
	public long getHeight(){
		return height;
	}

	/**
	 * Check whether the given height is a valid height for any board.
	 * 
	 * @param 	height
	 * 			The height to check.
	 * @return	true if and only if the given height is greater than or equal to zero and
	 * 			smaller than or equal to the maximum value for a Long.
	 * 			| result == height >= 0 && height <= Long.MAX_VALUE
	 */	
	private static boolean isValidHeight(long height){
		return height >= 0 && height <= Long.MAX_VALUE;
	}

	/**
	 * a variable registering the height of this board.
	 */
	private final long height;

	/**
	 * @return     the width of this board.
	 */
	@Basic @Immutable
	public long getWidth(){
		return width;
	}

	/**
	 * Check whether the given width is a valid width for any board.
	 * 
	 * @param 	width
	 * 			The width to check.
	 * @return	true if and only if the given width is greater than or equal to the maximum value for a Long.
	 * 			| result == width >= 0 && width <= Long.MAX_VALUE
	 */	
	private static boolean isValidWidth(long width){
		return width >= 0 && width <= Long.MAX_VALUE;
	}

	/**
	 * a variable registering the width of this board.
	 */
	private final long width;

	/**
	 * Check whether the given dimension(<code>height</code>,<code>width</code>) is a valid dimension for any board.
	 * @param 	height
	 * 			The height of the dimension.
	 * @param 	width
	 * 			The width of the dimension.
	 * @return	true if and only if the given dimension is a valid dimension for any board.
	 * 			| result == (isValidHeight(height) && isValidWidth(width))
	 */
	public static boolean isValidDimension(long height, long width){
		return isValidHeight(height) && isValidWidth(width);
	}

	/**
	 * Calculate the Manhattan distance between two given positions((<code>x1</code>, <code>y1</code>) and (<code>x2</code>, <code>y2</code>)).
	 * @param 	x1
	 * 			The X-coordinate of the first position.
	 * @param 	y1
	 * 			The Y-coordinate of the first position.
	 * @param 	x2
	 * 			The X-coordinate of the second position.
	 * @param 	y2
	 * 			The Y-coordinate of the second position.
	 * @return	The Manhattan distance between the two given positions.
	 * 			| result == Math.abs(x1 - x2) + Math.abs(y1 - y2)
	 */
	public static long getDistanceBetween(long x1, long y1, long x2, long y2){
		return Math.abs(x1 - x2) + Math.abs(y1 - y2);
	}
	/**
	 * Calculate the Manhattan distance between two given positions  (<code>position1</code>) and (<code>position2</code>).
	 * 
	 * @param 	position1
	 * 			The first position.
	 * @param 	position2
	 * 			The second position.
	 * @return	The Manhattan distance between the two given positions.
	 * 			| result == getDistanceBetween(position1.getX(), position1.getY(), position2.getX(), position2.getY())
	 */
	public static long getDistanceBetween(Position position1, Position position2){
		return getDistanceBetween(position1.getX(), position1.getY(), position2.getX(), position2.getY());
	}

	/**
	 * Get the distance between two given entities (<code>entity1</code>) and (<code>entity2</code>). 
	 * 
	 * @param 	entity1
	 * 			The first entity. 			
	 * @param 	entity2
	 * 			The second entity.
	 * @pre		Both entities are placed on a board.
	 * 			| entity1.isPlacedOnABoard() && entity2.isPlacedOnABoard()
	 * @pre		Both entities are placed on the same board
	 * 			| entity1.getBoard() == entity2.getBoard()
	 * @return	The Manhattan distance between the two given entities.
	 * 			| result == getDistanceBetween(entity1.getPosition(), entity2.getPosition())
	 */
	public static long getDistanceBetween(Entity entity1, Entity entity2){
		assert(entity1.isPlacedOnABoard() && entity2.isPlacedOnABoard()) : "Precondition: entity1.isPlacedOnABoard() && entity2.isPlacedOnABoard()";
		assert(entity1.getBoard() == entity2.getBoard());

		return getDistanceBetween(entity1.getPosition(), entity2.getPosition());
	}


	/**
	 * Check whether the given position(<code>position<code>) is inside the dimensions.
	 * 
	 * @param 	position
	 * 			The given position.
	 * @return	True and only true if the position is inside its dimensions.
	 * 			| result == (position.getX() < this.getWidth() && position.getY() < this.getHeight())
	 */
	public boolean isInsideDimensions(Position position){
		return (position.getX() <= this.getWidth() && position.getY() <= this.getHeight());
	}

	/**
	 * Returns 2 positions, defining a rectangle in which all entities on this board are located with a minimal padding of 1 position.
	 * @return	Result[0] contains the top-left rectangle defining this rectangle.
	 * 			| for each position in getPositionsInUse()
	 * 			|	result[0].getX() + 1 <= position.getX()
	 * 			|	result[0].getY() + 1 <= position.getY()
	 * @return	Result[1] contains the bottom-right position defining this rectangle.
	 * 			| for each position in getPositionsInUse()
	 * 			|	result[1].getX() - 1 >= position.getX()
	 * 			|	result[1].getY() - 1 >= position.getY()
	 */
	public Position[] getBoundsOfPositionsInUse(){
		long left = getWidth();
		long right = 0;
		long top = getHeight();
		long bottom = 0;
		try {
			for(Position pos : getPositionsInUse()){
				if(pos.getX() > right){
					right = pos.getX();
				}
				if(pos.getX() < left){
					left = pos.getX();
				}
				if(pos.getY() < top){
					top = pos.getY();
				}
				if(pos.getY() > bottom){
					bottom = pos.getY();
				}
			}

			left = (left == 0) ? 0 : left - 1;
			right = (right == getWidth()) ? getWidth() : right + 1;
			top = (top == 0) ? 0 : top - 1;
			bottom = (bottom == getHeight()) ? getHeight() : bottom + 1;

			Position[] bounds = { new Position(left, top), new Position(right, bottom) };
			return bounds;
		}
		catch (OutOfDimensionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * Check whether the given position is inside its bounds.
	 * 
	 * @param 	pos
	 * 			The given position.
	 * @pre		The given position is effective.
	 * 			| (pos != null)
	 * @return	True and only true if the given position is inside its dimensions.
	 * 			| result == (pos.getX() >= leftX && pos.getX() <= rightX && pos.getY() >= topY && pos.getY() <= bottomY)
	 */
	public boolean isInsideBounds(Position pos){
		assert(pos != null) : "The given position is effective.";
		if(!isInsideDimensions(pos))
			return false;

		Position topLeft;
		Position bottomRight;

		topLeft = getBoundsOfPositionsInUse()[0];
		bottomRight = getBoundsOfPositionsInUse()[1];

		return (pos.getX() >= topLeft.getX() && pos.getX() <= bottomRight.getX()
				&& pos.getY() >= topLeft.getY() && pos.getY() <= bottomRight.getY());
	}

	/**
	 * Checks if this board van have 
	 * @param	entity
	 * 			The entity to check.
	 * @return	| if(isTerminated())
	 * 			|	result == (entity == null)
	 * @return	| result == (entity != null && !entity.isTerminated())
	 */
	public boolean canHaveAsEntity(Entity entity){
		if(isTerminated())
			return (entity == null);
		return (entity != null && !entity.isTerminated());
	}
	
	/**
	 * @return the number of contents in use associated with this board.
	 */
	@Basic @Raw
	public int getNbPositionsInUse(){
		return contents.size();
	}

	/**
	 * @return the positions in use associated with this board.
	 * 			| result == contents.keySet();
	 */
	public Set<Position> getPositionsInUse(){
		return contents.keySet();
	}

	/**
	 * Returns a boolean defining whether the given position is in use on this board.
	 * @param 	position
	 * 			the position to check
	 * @return  | contents.containsKey(position)
	 */
	public boolean isPositionInUse(Position position){
		return contents.containsKey(position);
	}

	/**
	 * Return the number of Items on the given position.
	 * 
	 * @param 	position
	 * 			The given position to check.
	 * @return	| getNbEntitiesOfTypeOnPosition(Item.class, position)
	 */
	public int getNbItemsOnPosition(Position position){
		return getNbEntitiesOfTypeOnPosition(Item.class, position);
	}

	/**
	 * Return the number of Walls on the given position.
	 * 
	 * @param 	position
	 * 			The given position to check.
	 * @return	| getNbEntitiesOfTypeOnPosition(Wall.class, position)
	 */
	public int getNbWallsOnPosition(Position position){
		return getNbEntitiesOfTypeOnPosition(Wall.class, position);
	}

	/**
	 * Return the number of Robots on the given position.
	 * 
	 * @param 	position
	 * 			The given position to check.
	 * @return	| getNbEntitiesOfTypeOnPosition(Robot.class, position)
	 */
	public int getNbRobotsOnPosition(Position position){
		return getNbEntitiesOfTypeOnPosition(Robot.class, position);
	}

	/**
	 * Return the number of Batteries on the given position.
	 * 
	 * @param 	position
	 * 			The given position to check.
	 * @return	| getNbEntitiesOfTypeOnPosition(Battery.class, position)
	 */
	public int getNbBatteriesOnPosition(Position position){
		return getNbEntitiesOfTypeOnPosition(Battery.class, position);
	}

	/**
	 * Return the number of repair kits on the given position.
	 * 
	 * @param 	position
	 * 			The given position to check.
	 * @return	| getNbEntitiesOfTypeOnPosition(RepairKit.class, position)
	 */
	public int getNbRepairKitsOnPosition(Position position){
		return getNbEntitiesOfTypeOnPosition(RepairKit.class, position);
	}

	/**
	 * Return the number of surprise boxes on the given position.
	 * 
	 * @param 	position
	 * 			The given position to check.
	 * @return	| getNbEntitiesOfTypeOnPosition(RepairKit.class, position)
	 */
	public int getNbSurpriseBoxesOnPosition(Position position){
		return getNbEntitiesOfTypeOnPosition(SurpriseBox.class, position);
	}

	/**
	 * Return the number of entities on the given position.
	 * 
	 * @param 	position
	 * 			The position to check.
	 * @return	the number of entities on the given position
	 * 			| if(!contents.containsKey(position)) then result == 0
	 * 			| result == contents.get(Position).size()
	 */
	public int getNbEntitiesOnPosition(Position position){
		if(!contents.containsKey(position))
			return 0;
		return contents.get(position).size();
	}

	/**
	 * Returns the number of entities of the given type on the given position.
	 * @param	type
	 * 			The type of entities to search.
	 * @return	| if(!contents.containsKey(position)) then result == 0
	 * @return	| let nbEntitesSoFar = 0
	 * 			| for each entity in getEntitiesOnPosition(position)
	 * 			| 	if(type.isAssignableFrom(entity.getClass()))
	 * 			|		nbEntitiesSoFar++
	 * 			| result == nbEntitiesSoFar
	 */
	private <T extends Entity> int getNbEntitiesOfTypeOnPosition(Class<T> type, Position position){
		if(!contents.containsKey(position)) return 0;

		int nbEntitiesSoFar = 0;
		for(Entity entity : getEntitiesOnPosition(position)){
			if(type.isAssignableFrom(entity.getClass())){
				nbEntitiesSoFar++;
			}
		}
		return nbEntitiesSoFar;
	}

	/**
	 * Returns a clone of the contents of this board.
	 * @return	| for each entity in getAllEntities()
	 * 			| 	if(hasAsEntityOnPosition(entity, entity.getPosition()) then
	 * 			|		result.hasAsEntityOnPosition(entity, entity.getPosition())
	 */
	@Basic
	public HashMap<Position, ArrayList<Entity>> getContents(){
		return new HashMap<Position, ArrayList<Entity>>(contents);
	}

	/**
	 * 
	 * @param 	position
	 * 			The position for which you want to retrieve the entities.
	 * @return	| if(getContents().containsKey(position))
	 * 			|	result == getContents().get(position)
	 * @return	| if(!getContents().containsKey(position))
	 * 			|	result == null
	 * 			
	 */
	public List<Entity> getEntitiesOnPosition(Position position){
		if(contents.containsKey(position))
			return contents.get(position);
		else
			return null;
	}
	
	/**
	 * Returns a list containing all items on a given position
	 * @param 	position
	 * 			The position to look at.
	 * @return	| getAllEntitiesOfTypeOnPosition(Item.class, position)
	 */
	public List<Item> getItemsOnPosition(Position position){
		return getAllEntitiesOfTypeOnPosition(Item.class, position);
	}
	
	
	/**
	 * Returns a list containing all entities of the given type located on this board.
	 * @param	type
	 * 			The type of entities to search.
	 * @param	position
	 * 			The position to search at.
	 * @return	| for each entity in getEntitiesOnPosition() where type.isAssignableFrom(entity.getClass())
	 * 			|	result.contains(entity)
	 * 
	 */
	@SuppressWarnings("unchecked")
	private <T extends Entity> List<T> getAllEntitiesOfTypeOnPosition(Class<T> type, Position position){
		List<T> list = new ArrayList<T>();

		for(Entity entity : getEntitiesOnPosition(position)){
			if(type.isAssignableFrom(entity.getClass())){
				list.add((T) entity);
			}
		}
		return list;
	}
	
	/**
	 * Returns a set containing all entities of the given type located on this board.
	 * @param	type
	 * 			The type of entities to search.
	 * @return	| for each entity in getAllEntities() where type.isAssignableFrom(entity.getClass())
	 * 			|	result.contains(entity)
	 * 
	 */
	@SuppressWarnings("unchecked")
	public <T extends Entity> Set<T> getAllEntitiesOfType(Class<T> type){
		Set<T> set = new HashSet<T>();

		for(Entity entity : getAllEntities()){
			if(type.isAssignableFrom(entity.getClass())){
				set.add((T) entity);
			}
		}
		return set;
	}

	/**
	 * Returns a boolean reflecting if the given entity is at the given position on this board.
	 * @param 	entity
	 * 			The entity to look for.
	 * @param 	position
	 * 			The position to look on.
	 * @return	| result == (contents.containsKey(position) && contents.get(position).contains(entity))
	 */
	public boolean hasAsEntityOnPosition(Entity entity, Position position){
		if(!contents.containsKey(position))
			return false;
		return contents.get(position).contains(entity);
	}

	/**
	 * Remove the given entity on the given position.
	 * 
	 * @param 	position
	 * 			The given position to remove the entity from.
	 * @param 	entity
	 * 			The given entity to remove.
	 * @pre		The given entity is currently placed at the given position.
	 * 			| hasAsEntityOnPosition(entity, position)
	 * @post	The given entity is removed from this board.
	 * 			| !new.hasAsEntity(entity)
	 */
	public void removeEntityFromPosition(Position position, Entity entity){
		assert(hasAsEntityOnPosition(entity, position)) : "Precondition: The given entity is currently placed at the given position.";

		if(contents.containsKey(position)){
			contents.get(position).remove(entity);
			if(contents.get(position).size() == 0){
				contents.remove(position);
			}
		}
	}

	/**
	 * Replace an entity from one to another position.
	 * 
	 * @param 	entity
	 * 			The given entity to replace.
	 * @param 	destination
	 * 			The given destination position to move the entity to.
	 * @pre		The given entity is placed on a board.
	 * 			| entity.isPlacedOnABoard()
	 * @effect	The given entity is removed from its initial position and added to the destination position.
	 * 			| removeEntityFromPosition(initial, entity)
	 *			| addEntityToPosition(destination, entity)
	 * @throws 	IllegalPositionException
	 * 			The position to move the entity to is not valid.
	 * 			| !entity.canBePutOn(destination)
	 * 			
	 */
	public void replaceEntity(Entity entity, Position destination) throws IllegalPositionException, IllegalArgumentException{
		assert(entity.isPlacedOnABoard()) : "Precondition: The given entity is placed on a board.";

		removeEntityFromPosition(entity.getPosition(), entity);
		addEntityToPosition(destination, entity);
	}

	/**
	 * Add a given entity to the given position.
	 * @param 	position
	 * 			The given position to put the entity on.
	 * @param 	entity
	 * 			The given entity to put on the given position.
	 * @pre		| !hasAsEntity(entity)
	 * @post	| new.hasAsEntityOnPosition(entity, position)
	 * @throws 	The position to put the entity on must be valid.
	 * 			IllegalPositionException
	 * 			| (!entity.canBePutOn(this, position))
	 */
	@Raw
	public void addEntityToPosition(Position position, Entity entity) throws IllegalPositionException{
		assert(!hasAsEntity(entity));
		if(!entity.canBePutOn(this, position))
			throw new IllegalPositionException(position);

		if(contents.containsKey(position))
			contents.get(position).add(entity);
		else
		{
			contents.put(position, new ArrayList<Entity>());
			contents.get(position).add(entity);
		}
	}

	/**
	 * returns a set containing all entities contained on this board.
	 * 
	 * @return 	| for each entityList in contents.values()
	 * 			|	entities.addAll(entityList)
	 * 			| result == entities
	 */
	private Set<Entity> getAllEntities(){
		Set<Entity> entities = new HashSet<Entity>();
		for(ArrayList<Entity> entityList : contents.values()){
			entities.addAll(entityList);
		}

		return entities;
	}

	/**
	 * Checks if this board has the given entity located.
	 * @param 	entity
	 * 			The entity to check
	 * @return	| getAllEntities().contains(entity)
	 */
	@Model
	private boolean hasAsEntity(Entity entity){
		return getAllEntities().contains(entity);
	}

	/**
	 * A map containing positions as keys and the entities on that position in a list as values.
	 */
	private HashMap<Position, ArrayList<Entity>> contents = new HashMap<Position, ArrayList<Entity>>();

	/**
	 * Return an iterator returning all the elements in this 
	 * composed binary tree.
	 * @return  An iterator that iterates over all entities that get accepted by the given filter.
	 *        | for each entity in getAllEntities():
	 *        |   if (filter.accepts(entity))
	 *        |     then result.contains(entity)
	 */
	public Iterator<Entity> iterator(final Filter filter) {
		return new Iterator<Entity>() {
			
			/**
			 * Checks whether this iterator has the given entity as entity.
			 * @param 	entity
			 * 			The entity to check.
			 * @return	|	filteredEntities.contains(entity)
			 */
			@Model
			private boolean hasAsFilteredEntity(Entity entity){
				return filteredEntities.contains(entity);
			}
			
			/**
			 * Gets a clone of the list of all entities that are accepted by the given filter.
			 * @return	| for each entity in iterator
			 * 			|	(result.contains(entity) ==
			 * 			|		this.hasAsFilteredEntity(entity))
			 */
			@Model
			private List<Entity> getFilteredEntities() {
				return new ArrayList(filteredEntities);
			}
			
			private List<Entity> filteredEntities = new ArrayList<Entity>();
			
			private int index = 0;
			
			{
				for(Entity entity : getAllEntities()){
					if(filter.accepts(entity)) filteredEntities.add(entity);
				}
			}
			
			/**
			 * @return	| getFilteredEntities().contains(entity)
			 */
			public boolean contains(Entity entity){
				return filteredEntities.contains(entity);
			}
			
			@Override
			public boolean hasNext() {
				return index < filteredEntities.size();
			}

			@Override
			public Entity next() {
				if(!hasNext())
					throw new NoSuchElementException();
				
				Entity next = filteredEntities.get(index);
				index++;
				return next;
			}

			@Override
			public void remove() {
				throw new UnsupportedOperationException();
			}
			
		};
	}
	
	/**
	 * Merge this board with another board.
	 * @pre		Each entity on the other board can have this board as board.
	 * 			| for each entity in other.getAllEntities
	 * 			|	entity.canHaveAsBoard(this)
	 * @param 	other
	 * 			The board to merge this board with.
	 * @post	| for each entity in other.getAllEntities()
	 * 			| 	if(entity.canBePutOn(this, entity.getPosition()))
	 * 			|		new.this.hasAsEntityOnPosition(entity, entity.getPosition())
	 *			| new.other.isTerminated() == true
	 */
	public void merge(Board other){
		//preventing 'ConcurrentModificationException'
		Set<Position> positionsClone = new HashSet<Position>(other.getPositionsInUse());
		for(Position position :  positionsClone){
			if(isInsideDimensions(position)){
				List<Entity> entitiesClone = new ArrayList<Entity>(other.getEntitiesOnPosition(position));
				//preventing 'ConcurrentModificationException'
				for(Entity entity : entitiesClone){
					if(entity.canBePutOn(this, position)){
						try {
							entity.remove();
							entity.put(position, this);
						} catch (IllegalPositionException e) {
							e.toString();						
						}
					}
				}
			}
		}
		other.terminate();
	}

	/**
	 * returns a boolean reflecting whether this board is terminated.
	 * @return | result == isTerminated
	 */
	@Basic @Raw
	public boolean isTerminated() {
		return this.isTerminated;
	}

	/**
	 * Terminate this board, terminating all robots, walls, and batteries on the board.
	 * 
	 *	@post 	this board is terminated
	 *			| new.isTerminated()
	 * 	@effect	| for each entity in entities
	 * 			| 	entity.terminate()
	 * 			| contents.clear()
	 * @post	| new.this.isTerminated()
	 * 			
	 */
	public void terminate(){
		for (Entity entity : getAllEntities()){
			entity.terminate();
		}

		contents.clear();
		this.isTerminated = true;
	}

	/**
	 * Variable registering whether this board is terminated.
	 */
	private boolean isTerminated = false;

}
