package roborally.filter;

import be.kuleuven.cs.som.annotate.Basic;
import be.kuleuven.cs.som.annotate.Immutable;
import roborally.entity.Energetic;
import roborally.entity.Entity;
import roborally.entity.Robot;
import roborally.entity.item.Battery;
import roborally.entity.item.RepairKit;
import roborally.value.EnergyAmount;

public class EnergyFilter implements Filter{
	public EnergyFilter(EnergyFilterArguments argument, EnergyAmount energy){	
		this.argument = argument;
		this.energy = energy;
	}
	
	@Basic @Immutable
	public EnergyFilterArguments getArgument() {
		return argument;
	}
	
	private final EnergyFilterArguments argument;
	
	@Basic @Immutable
	public EnergyAmount getEnergy() {
		return energy;
	}
	
	private final EnergyAmount energy;
	
	@Override
	public boolean accepts(Entity entity){
		switch(getArgument()){
		case LOWER:
			if(entity instanceof Energetic)
				return (((Energetic) entity).getEnergy().compareTo(this.getEnergy()) < 0);
			break;
		case EQUAL:
			if(entity instanceof Energetic)
				return (((Energetic) entity).getEnergy().compareTo(this.getEnergy()) == 0);
			break;
		case HIGHER:
			if(entity instanceof Energetic)
				return (((Energetic) entity).getEnergy().compareTo(energy) > 0);
			break;
		}
		return false;
	}
}
