package roborally.filter;

import roborally.entity.Entity;

/**
 * 
 * @author 	Niels De Bock, Michael Vincken (Computer Science) 
 * @version	1.0
 */
public interface Filter {
	boolean accepts(Entity entity);
}
