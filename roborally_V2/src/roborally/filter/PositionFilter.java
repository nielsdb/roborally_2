package roborally.filter;

import be.kuleuven.cs.som.annotate.Basic;
import be.kuleuven.cs.som.annotate.Immutable;
import roborally.entity.Entity;
import roborally.position.Board;
import roborally.position.Position;

public class PositionFilter implements Filter {
	/**
	 * 
	 * @param 	board
	 * 			The board this filter will operate on.
	 * @post	| new.getTopLeft().equals(board.getBoundsOfPositionsInUse()[0])
	 * 			| new.getBottomRight().equals(board.getBoundsOfPositionsInUse()[1])
	 */
	public PositionFilter(Board board){
		this.topLeft = board.getBoundsOfPositionsInUse()[0];
		this.bottomRight = board.getBoundsOfPositionsInUse()[1];
	}
	
	public PositionFilter(Position topLeft, Position bottomRight){
		this.topLeft = topLeft;
		this.bottomRight = bottomRight;
	}
	
	@Basic @Immutable
	public Position getTopLeft() {
		return topLeft;
	}
	
	final private Position topLeft;	
	
	@Basic @Immutable
	public Position getBottomRight() {
		return bottomRight;
	}
	
	final private Position bottomRight;
	
	@Override
	public boolean accepts(Entity entity) {
		return entity.getPosition().getX() <= bottomRight.getX() && entity.getPosition().getX() >= topLeft.getX()
				&& entity.getPosition().getY() <= bottomRight.getY() && entity.getPosition().getY() >= topLeft.getY();
	}

}
