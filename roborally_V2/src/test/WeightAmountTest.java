package test;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import roborally.value.WeightAmount;
import roborally.value.WeightUnit;

public class WeightAmountTest {
	@Test
	public void testConversion(){
		assertTrue(new WeightAmount(5).toUnit(WeightUnit.kg).getNumeral() == 0);
		assertTrue(new WeightAmount(5000).toUnit(WeightUnit.kg).getNumeral() == 5);

		assertTrue(new WeightAmount(2542).toUnit(WeightUnit.kg).getNumeral() == 2);
		assertTrue(new WeightAmount(99).toUnit(WeightUnit.kg).getNumeral() == 0);

		assertTrue(new WeightAmount(5,WeightUnit.kg).toUnit(WeightUnit.g).getNumeral() == 5000);
		assertTrue(new WeightAmount(5000,WeightUnit.kg).toUnit(WeightUnit.g).getNumeral() == 5000000);

		assertTrue(new WeightAmount(2542,WeightUnit.kg).toUnit(WeightUnit.g).getNumeral() == 2542000);
		assertTrue(new WeightAmount(99,WeightUnit.kg).toUnit(WeightUnit.g).getNumeral() == 99000);

	}
	
	@Test
	public void testAdd(){
		assertTrue(new WeightAmount(99).add(new WeightAmount(3)).getNumeral() == 102);

	}
}
