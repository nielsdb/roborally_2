package test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

import roborally.entity.Robot;
import roborally.entity.Wall;
import roborally.entity.item.Battery;
import roborally.entity.item.Item;
import roborally.entity.item.RepairKit;
import roborally.entity.item.Surprise;
import roborally.entity.item.SurpriseBox;
import roborally.exception.IllegalDimensionException;
import roborally.exception.IllegalPositionException;
import roborally.exception.OutOfDimensionException;
import roborally.position.Board;
import roborally.position.Direction;
import roborally.position.Orientation;
import roborally.position.Position;
import roborally.program.exception.UnparsableException;
import roborally.value.EnergyAmount;
import roborally.value.WeightAmount;

public class RobotTest {
	private Board smallBoard;
	private Board largeBoard;

	@Before
	public void initializeBoards(){
		try{
			smallBoard = new Board(4,4);
			largeBoard = new Board(50, 50);
		}
		catch(IllegalDimensionException e){
			System.err.println(e.toString());
		}
	}

	@Test
	public void testGetEnergy(){
		try{
			Robot robot = new Robot(Orientation.UP, new EnergyAmount(2500));
			robot.put(new Position(5,5), largeBoard);
			assertTrue(robot.getEnergy().equals(new EnergyAmount(2500)));

		} catch(Exception e){
			e.printStackTrace();
		}
	}

	@Test
	public void testGetOrientation_UP(){
		try{
			Robot robot = new Robot(Orientation.UP, new EnergyAmount(2500));
			robot.put(new Position(5,5), largeBoard);
			assertTrue(robot.getOrientation() == Orientation.UP);

		} catch(Exception e){
			e.printStackTrace();
		}
	}

	@Test
	public void testGetOrientation_RIGHT(){
		try{
			Robot robot = new Robot(Orientation.RIGHT, new EnergyAmount(2500));
			robot.put(new Position(5,5), largeBoard);
			assertTrue(robot.getOrientation() == Orientation.RIGHT);

		} catch(Exception e){
			e.printStackTrace();
		}
	}

	@Test
	public void testGetOrientation_DOWN(){
		try{
			Robot robot = new Robot(Orientation.DOWN, new EnergyAmount(2500));
			robot.put(new Position(5,5), largeBoard);
			assertTrue(robot.getOrientation() == Orientation.DOWN);

		} catch(Exception e){
			e.printStackTrace();
		}
	}

	@Test
	public void testGetOrientation_LEFT(){
		try{
			Robot robot = new Robot(Orientation.LEFT, new EnergyAmount(2500));
			robot.put(new Position(5,5), largeBoard);
			assertTrue(robot.getOrientation() == Orientation.LEFT);

		} catch(Exception e){
			e.printStackTrace();
		}
	}

	@Test
	public void testGetOrientation_null(){
		try{
			Robot robot = new Robot(null, new EnergyAmount(2500));
			assertTrue(robot.getOrientation() == Orientation.UP);

		} catch(Exception e){
			e.printStackTrace();
		}
	}

	@Test
	public void testGetFractionOfEnergy(){
		try{
			Robot robot = new Robot(Orientation.LEFT, new EnergyAmount(2500));
			robot.put(new Position(5,5), largeBoard);
			assertEquals(13, robot.getFractionOfEnergy(), 0.1);
		} catch (OutOfDimensionException e){
			e.printStackTrace();
		} catch (IllegalPositionException e){
			e.printStackTrace();
		}
	}

	@Test
	public void testHasEnoughEnergyToMove(){
		try{
			Robot robot1 = new Robot(Orientation.LEFT, new EnergyAmount(2500));
			robot1.put(new Position(4,4), largeBoard);
			Robot robot2 = new Robot(Orientation.LEFT, new EnergyAmount(400));
			robot2.put(new Position(5,5), largeBoard);
			assertTrue(robot1.hasEnoughEnergyToMove() == true);
			assertTrue(robot2.hasEnoughEnergyToMove() == false);

		} catch (OutOfDimensionException e){
			e.printStackTrace();
		} catch (IllegalPositionException e){
			e.printStackTrace();
		}
	}

	@Test
	public void testHasEnoughEnergyToTurn(){
		try{
			Robot robot1 = new Robot(Orientation.LEFT, new EnergyAmount(2500));
			robot1.put(new Position(4,4), largeBoard);
			Robot robot2 = new Robot(Orientation.LEFT, new EnergyAmount(50));
			robot2.put(new Position(5,5), largeBoard);
			assertTrue(robot1.hasEnoughEnergyToTurn() == true);
			assertTrue(robot2.hasEnoughEnergyToTurn() == false);

		} catch (OutOfDimensionException e){
			e.printStackTrace();
		} catch (IllegalPositionException e){
			e.printStackTrace();
		}
	}

	@Test
	public void testHasEnoughEnergyToShoot(){
		try{
			Robot robot1 = new Robot(Orientation.LEFT, new EnergyAmount(2500));
			robot1.put(new Position(4,4), largeBoard);
			Robot robot2 = new Robot(Orientation.LEFT, new EnergyAmount(500));
			robot2.put(new Position(5,5), largeBoard);
			assertTrue(robot1.hasEnoughEnergyToShoot() == true);
			assertTrue(robot2.hasEnoughEnergyToShoot() == false);

		} catch (OutOfDimensionException e){
			e.printStackTrace();
		} catch (IllegalPositionException e){
			e.printStackTrace();
		}
	}

	@Test
	public void testTransferTo(){
		try {
			Robot robotFrom = new Robot(Orientation.RIGHT, new EnergyAmount(13000));
			robotFrom.put(new Position(4,4), largeBoard);

			Robot robotTo = new Robot(Orientation.LEFT, new EnergyAmount(13000));
			robotTo.put(new Position(5,4), largeBoard);

			SurpriseBox surpriseBox = new SurpriseBox(new WeightAmount(400));
			surpriseBox.put(new Position(4,4), largeBoard);
			robotFrom.pickUp(surpriseBox);

			RepairKit repairKit = new RepairKit(new EnergyAmount(1000),new WeightAmount(700));
			repairKit.put(new Position(4,4), largeBoard);
			robotFrom.pickUp(repairKit);

			Battery battery = new Battery(new EnergyAmount(1000),new WeightAmount(300));
			battery.put(new Position(4,4), largeBoard);
			robotFrom.pickUp(battery);

			assertEquals(3, robotFrom.getNbPossessions());
			robotFrom.transferPossessionsTo(robotTo);
			assertEquals(3, robotTo.getNbPossessions());
			assertEquals(0, robotFrom.getNbPossessions());

			ArrayList<Item> list = new ArrayList<Item>();
			list.add(battery);
			list.add(surpriseBox);
			list.add(repairKit);
			assertEquals(list, robotTo.getPossessions());

		} catch (OutOfDimensionException e){
			e.printStackTrace();
		} catch (IllegalPositionException e){
			e.printStackTrace();
		}

	}
	@Test
	public void testMove_Orientation_UP(){
		try{
			Robot robot = new Robot(Orientation.UP, new EnergyAmount(1000));
			robot.put(new Position(7,7), largeBoard);
			robot.move();
			assertTrue(robot.getPosition().equals(new Position(7,6)));
			assertTrue(robot.getEnergy().equals(new EnergyAmount(500)));		

		}catch (OutOfDimensionException e){
			e.printStackTrace();
		} catch (IllegalPositionException e){
			e.printStackTrace();
		}
	}

	@Test (expected=IllegalPositionException.class)
	public void testMove_ImpossibleToMove() throws IllegalPositionException{
		try{
			Robot robot = new Robot(Orientation.RIGHT, new EnergyAmount(500));
			robot.put(new Position(8,7), largeBoard);
			new Wall().put(new Position(8,7), largeBoard);
			robot.move();
			assertEquals(new Position(8,7), robot.getPosition());
			assertEquals(new EnergyAmount(500), robot.getEnergy());
		}catch (OutOfDimensionException e){
			e.printStackTrace();
		}
	}

	@Test
	public void testMove_Orientation_RIGHT(){
		try{
			Robot robot = new Robot(Orientation.RIGHT, new EnergyAmount(1000));
			robot.put(new Position(7,7), largeBoard);
			robot.move();
			assertTrue(robot.getPosition().equals(new Position(8,7)));
			assertTrue(robot.getEnergy().equals(new EnergyAmount(500)));		
		}catch (OutOfDimensionException e){
			e.printStackTrace();
		} catch (IllegalPositionException e){
			e.printStackTrace();
		}
	}

	@Test
	public void testMove_Orientation_DOWN(){
		try{
			Robot robot = new Robot(Orientation.DOWN, new EnergyAmount(1000));
			robot.put(new Position(7,7), largeBoard);
			robot.move();
			assertTrue(robot.getPosition().equals(new Position(7,8)));
			assertTrue(robot.getEnergy().equals(new EnergyAmount(500)));		

		}catch (OutOfDimensionException e){
			e.printStackTrace();
		} catch (IllegalPositionException e){
			e.printStackTrace();
		}
	}

	@Test
	public void testMove_Orientation_LEFT(){
		try{
			Robot robot = new Robot(Orientation.LEFT, new EnergyAmount(1000));
			robot.put(new Position(7,7), largeBoard);
			robot.move();
			assertTrue(robot.getPosition().equals(new Position(6,7)));
			assertTrue(robot.getEnergy().equals(new EnergyAmount(500)));		

		}catch (OutOfDimensionException e){
			e.printStackTrace();
		} catch (IllegalPositionException e){
			e.printStackTrace();
		}
	}

	@Test
	public void testMove_CarryingAnItemOfWeight1000(){
		try{
			Robot robot = new Robot(Orientation.LEFT, new EnergyAmount(750));
			robot.put(new Position(7,7), largeBoard);
			Battery battery = new Battery(new EnergyAmount(2000), new WeightAmount(1000));
			battery.put(new Position(7,7), largeBoard);
			robot.pickUp(battery);
			robot.move();
			assertTrue(robot.getPosition().equals(new Position(6,7)));
			assertTrue(robot.getEnergy().equals(new EnergyAmount(200)));

		} catch (OutOfDimensionException e){
			e.printStackTrace();
		} catch (IllegalPositionException e){
			e.printStackTrace();
		}
	}

	@Test
	public void testMove_CarryingAnItemOfWeight2999(){
		try{
			Robot robot = new Robot(Orientation.LEFT, new EnergyAmount(750));
			robot.put(new Position(7,7), largeBoard);
			Battery battery = new Battery(new EnergyAmount(2000), new WeightAmount(2999));
			battery.put(new Position(7,7), largeBoard);
			robot.pickUp(battery);
			robot.move();
			assertTrue(robot.getPosition().equals(new Position(6,7)));
			assertTrue(robot.getEnergy().equals(new EnergyAmount(150)));

		} catch (OutOfDimensionException e){
			e.printStackTrace();
		} catch (IllegalPositionException e){
			e.printStackTrace();
		}
	}

	@Test
	public void testGetPositionAfterMove(){
		try{
			assertTrue(Robot.getPositionAfterMove(new Position(2,2), Orientation.UP).equals(new Position(2,1)));
			assertTrue(Robot.getPositionAfterMove(new Position(2,2), Orientation.DOWN).equals(new Position(2,3)));

			assertTrue(Robot.getPositionAfterMove(new Position(2,2), Orientation.LEFT).equals(new Position(1,2)));
			assertTrue(Robot.getPositionAfterMove(new Position(2,2), Orientation.RIGHT).equals(new Position(3,2)));

		}catch (OutOfDimensionException e) {
			e.printStackTrace();
		}
	}

	@Test
	public void testTurn_UP(){
		try{
			Robot robot = new Robot(Orientation.UP, new EnergyAmount(15000));
			robot.put(new Position(2,2), smallBoard);
			robot.turn();
			assertTrue(robot.getOrientation().equals(Orientation.RIGHT));
			assertEquals(new EnergyAmount(14900), robot.getEnergy());
		} catch (OutOfDimensionException e){
			e.printStackTrace();
		} catch (IllegalPositionException e){
			e.printStackTrace();
		}
	}

	@Test
	public void testTurn_LEFT(){
		try{
			Robot robot = new Robot(Orientation.LEFT, new EnergyAmount(15000));
			robot.put(new Position(2,2), smallBoard);
			robot.turn();
			assertTrue(robot.getOrientation().equals(Orientation.UP));

		} catch (OutOfDimensionException e){
			e.printStackTrace();
		} catch (IllegalPositionException e){
			e.printStackTrace();
		}
	}

	@Test
	public void testTurn_DOWN(){
		try{
			Robot robot = new Robot(Orientation.DOWN, new EnergyAmount(15000));
			robot.put(new Position(2,2), smallBoard);
			robot.turn();
			assertTrue(robot.getOrientation().equals(Orientation.LEFT));

		} catch (OutOfDimensionException e){
			e.printStackTrace();
		} catch (IllegalPositionException e){
			e.printStackTrace();
		}
	}

	@Test
	public void testTurn_RIGHT(){
		try{
			Robot robot = new Robot(Orientation.RIGHT, new EnergyAmount(15000));
			robot.put(new Position(2,2), smallBoard);
			robot.turn();
			assertTrue(robot.getOrientation().equals(Orientation.DOWN));

		} catch (OutOfDimensionException e){
			e.printStackTrace();
		} catch (IllegalPositionException e){
			e.printStackTrace();
		}
	}

	@Test
	public void testTurn_CLOCKWISE_UP(){
		try{
			Robot robot = new Robot(Orientation.UP, new EnergyAmount(15000));
			robot.put(new Position(2,2), smallBoard);
			robot.turn(Direction.CLOCKWISE);
			assertTrue(robot.getOrientation().equals(Orientation.RIGHT));

		} catch (OutOfDimensionException e){
			e.printStackTrace();
		} catch (IllegalPositionException e){
			e.printStackTrace();
		}
	}

	@Test
	public void testTurn_CLOCKWISE_LEFT(){
		try{
			Robot robot = new Robot(Orientation.LEFT, new EnergyAmount(15000));
			robot.put(new Position(2,2), smallBoard);
			robot.turn(Direction.CLOCKWISE);
			assertTrue(robot.getOrientation().equals(Orientation.UP));

		} catch (OutOfDimensionException e){
			e.printStackTrace();
		} catch (IllegalPositionException e){
			e.printStackTrace();
		}
	}

	@Test
	public void testTurn_CLOCKWISE_DOWN(){
		try{
			Robot robot = new Robot(Orientation.DOWN, new EnergyAmount(15000));
			robot.put(new Position(2,2), smallBoard);
			robot.turn(Direction.CLOCKWISE);
			assertTrue(robot.getOrientation().equals(Orientation.LEFT));

		} catch (OutOfDimensionException e){
			e.printStackTrace();
		} catch (IllegalPositionException e){
			e.printStackTrace();
		}
	}

	@Test
	public void testTurn_CLOCKWISE_RIGHT(){
		try{
			Robot robot = new Robot(Orientation.RIGHT, new EnergyAmount(15000));
			robot.put(new Position(2,2), largeBoard);
			robot.turn(Direction.CLOCKWISE);
			assertTrue(robot.getOrientation().equals(Orientation.DOWN));

		} catch (OutOfDimensionException e){
			e.printStackTrace();
		} catch (IllegalPositionException e){
			e.printStackTrace();
		} 
	}

	@Test
	public void testTurn_COUNTERCLOCKWISE_UP(){
		try{
			Robot robot = new Robot(Orientation.UP, new EnergyAmount(15000));
			robot.put(new Position(2,2), smallBoard);
			robot.turn(Direction.COUNTERCLOCKWISE);
			assertTrue(robot.getOrientation().equals(Orientation.LEFT));

		} catch (OutOfDimensionException e){
			e.printStackTrace();
		} catch (IllegalPositionException e){
			e.printStackTrace();
		} 
	}

	@Test
	public void testTurn_COUNTERCLOCKWISE_LEFT(){
		try{
			Robot robot = new Robot(Orientation.LEFT, new EnergyAmount(15000));
			robot.put(new Position(2,2), smallBoard);
			robot.turn(Direction.COUNTERCLOCKWISE);
			assertTrue(robot.getOrientation().equals(Orientation.DOWN));

		} catch (OutOfDimensionException e){
			e.printStackTrace();
		} catch (IllegalPositionException e){
			e.printStackTrace();
		}
	}

	@Test
	public void testTurn_COUNTERCLOCKWISE_DOWN(){
		try{
			Robot robot = new Robot(Orientation.DOWN, new EnergyAmount(15000));
			robot.put(new Position(2,2), smallBoard);
			robot.turn(Direction.COUNTERCLOCKWISE);
			assertTrue(robot.getOrientation().equals(Orientation.RIGHT));

		} catch (OutOfDimensionException e){
			e.printStackTrace();
		} catch (IllegalPositionException e){
			e.printStackTrace();
		}
	}

	@Test
	public void testTurn_COUNTERCLOCKWISE_RIGHT(){
		try{
			Robot robot = new Robot(Orientation.RIGHT, new EnergyAmount(15000));
			robot.put(new Position(2,2), smallBoard);
			robot.turn(Direction.COUNTERCLOCKWISE);
			assertTrue(robot.getOrientation().equals(Orientation.UP));

		} catch (OutOfDimensionException e){
			e.printStackTrace();
		} catch (IllegalPositionException e){
			e.printStackTrace();
		}
	}

	@Test
	public void testGetEnergyRequiredToReach_Basic(){
		try{
			Robot robot = new Robot(Orientation.UP, new EnergyAmount(15000));
			robot.put(new Position(5,5), largeBoard);
			assertTrue(robot.getEnergyRequiredToReach(new Position(5,4)) == 500);
			assertTrue(robot.getEnergyRequiredToReach(new Position(5,6)) == 700);
			assertTrue(robot.getEnergyRequiredToReach(new Position(4,5)) == 600);
			assertTrue(robot.getEnergyRequiredToReach(new Position(4,5)) == 600);
		}
		catch(IllegalPositionException e){
			e.printStackTrace();
		} catch (OutOfDimensionException e) {
			e.printStackTrace();
		} 
	}

	@Test
	public void testGetEnergyRequiredToReach_Obstacles(){
		try{
			Robot robot = new Robot(Orientation.UP, new EnergyAmount(15000));
			robot.put(new Position(5,5), largeBoard);
			new Wall().put(new Position(6,5), largeBoard);
			new Wall().put(new Position(6,6), largeBoard);
			assertTrue(robot.getEnergyRequiredToReach(new Position(7,6)) == 2700);

			robot.turn(Direction.CLOCKWISE);
			assertTrue(robot.getEnergyRequiredToReach(new Position(7,6)) == 2800);

			robot.turn(Direction.CLOCKWISE);
			assertTrue(robot.getEnergyRequiredToReach(new Position(7,6)) == 2700);

			robot.turn(Direction.CLOCKWISE);
			assertTrue(robot.getEnergyRequiredToReach(new Position(7,6)) == 2800);

			robot.turn(Direction.CLOCKWISE);

			new Wall().put(new Position(6,7), largeBoard);
			assertTrue(robot.getEnergyRequiredToReach(new Position(7,6)) == 2700);

			robot.turn(Direction.CLOCKWISE);
			assertTrue(robot.getEnergyRequiredToReach(new Position(7,6)) == 2800);

			robot.turn(Direction.CLOCKWISE);
			assertTrue(robot.getEnergyRequiredToReach(new Position(7,6)) == 2900);

			robot.turn(Direction.CLOCKWISE);
			assertTrue(robot.getEnergyRequiredToReach(new Position(7,6)) == 2800);

			robot.turn(Direction.CLOCKWISE);
			new Wall().put(new Position(6,4), largeBoard);
			new Wall().put(new Position(6,3), largeBoard);
			new Wall().put(new Position(5,3), largeBoard);
			new Wall().put(new Position(4,3), largeBoard);
			new Wall().put(new Position(3,3), largeBoard);
			assertTrue(robot.getEnergyRequiredToReach(new Position(7,6)) == 3900);

			robot.turn(Direction.CLOCKWISE);
			assertTrue(robot.getEnergyRequiredToReach(new Position(7,6)) == 3800);

			robot.turn(Direction.CLOCKWISE);
			assertTrue(robot.getEnergyRequiredToReach(new Position(7,6)) == 3700);

			robot.turn(Direction.CLOCKWISE);
			assertTrue(robot.getEnergyRequiredToReach(new Position(7,6)) == 3800);

			robot.turn(Direction.CLOCKWISE);

			new Wall().put(new Position(5,7), largeBoard);

			assertTrue(robot.getEnergyRequiredToReach(new Position(7,6)) == 4900);

			robot.turn(Direction.CLOCKWISE);
			assertTrue(robot.getEnergyRequiredToReach(new Position(5,4)) == 600);

		}
		catch(IllegalPositionException e){
			e.printStackTrace();
		} catch (OutOfDimensionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Test
	public void testGetEnergyRequiredToReach_Orientation_UP() {
		try{
			Robot robot = new Robot(Orientation.UP, new EnergyAmount(15000));
			robot.put(new Position(6,5), largeBoard);
			assertTrue(robot.getEnergyRequiredToReach(new Position(3,2)) == 3100);
			assertTrue(robot.getEnergyRequiredToReach(new Position(3,5)) == 1600);
			assertTrue(robot.getEnergyRequiredToReach(new Position(3,8)) == 3200);
			assertTrue(robot.getEnergyRequiredToReach(new Position(6,2)) == 1500);
			assertTrue(robot.getEnergyRequiredToReach(new Position(6,5)) == 0);
			assertTrue(robot.getEnergyRequiredToReach(new Position(6,8)) == 1700);
			assertTrue(robot.getEnergyRequiredToReach(new Position(9,2)) == 3100);
			assertTrue(robot.getEnergyRequiredToReach(new Position(9,5)) == 1600);
			assertTrue(robot.getEnergyRequiredToReach(new Position(9,8)) == 3200);
		}
		catch(IllegalPositionException e){

		} catch (OutOfDimensionException e) {
			e.printStackTrace();
		} 
	}

	@Test
	public void testGetEnergyRequiredToReach_Orientation_RIGHT() {
		try{
			Robot robot = new Robot(Orientation.RIGHT, new EnergyAmount(15000));
			robot.put(new Position(6,5), largeBoard);

			assertTrue(robot.getEnergyRequiredToReach(new Position(3,2)) == 3200);
			assertTrue(robot.getEnergyRequiredToReach(new Position(3,5)) == 1700);
			assertTrue(robot.getEnergyRequiredToReach(new Position(3,8)) == 3200);
			assertTrue(robot.getEnergyRequiredToReach(new Position(6,2)) == 1600);
			assertTrue(robot.getEnergyRequiredToReach(new Position(6,5)) == 0);
			assertTrue(robot.getEnergyRequiredToReach(new Position(6,8)) == 1600);
			assertTrue(robot.getEnergyRequiredToReach(new Position(9,2)) == 3100);
			assertTrue(robot.getEnergyRequiredToReach(new Position(9,5)) == 1500);
			assertTrue(robot.getEnergyRequiredToReach(new Position(9,8)) == 3100);
		}
		catch(IllegalPositionException e){

		} catch (OutOfDimensionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Test
	public void testGetEnergyRequiredToReach_Orientation_DOWN() {
		try{
			Robot robot = new Robot(Orientation.DOWN, new EnergyAmount(15000));
			robot.put(new Position(6,5), largeBoard);

			assertTrue(robot.getEnergyRequiredToReach(new Position(3,2)) == 3200);
			assertTrue(robot.getEnergyRequiredToReach(new Position(3,5)) == 1600);
			assertTrue(robot.getEnergyRequiredToReach(new Position(3,8)) == 3100);
			assertTrue(robot.getEnergyRequiredToReach(new Position(6,2)) == 1700);
			assertTrue(robot.getEnergyRequiredToReach(new Position(6,5)) == 0);
			assertTrue(robot.getEnergyRequiredToReach(new Position(6,8)) == 1500);
			assertTrue(robot.getEnergyRequiredToReach(new Position(9,2)) == 3200);
			assertTrue(robot.getEnergyRequiredToReach(new Position(9,5)) == 1600);
			assertTrue(robot.getEnergyRequiredToReach(new Position(9,8)) == 3100);
		}
		catch(IllegalPositionException e){

		} catch (OutOfDimensionException e) {
			e.printStackTrace();
		} 
	}

	@Test
	public void testGetEnergyRequiredToReach_Orientation_LEFT() {
		try{
			Robot robot = new Robot(Orientation.LEFT, new EnergyAmount(15000));
			robot.put(new Position(6,5), largeBoard);

			assertTrue(robot.getEnergyRequiredToReach(new Position(3,2)) == 3100);
			assertTrue(robot.getEnergyRequiredToReach(new Position(3,5)) == 1500);
			assertTrue(robot.getEnergyRequiredToReach(new Position(3,8)) == 3100);
			assertTrue(robot.getEnergyRequiredToReach(new Position(6,2)) == 1600);
			assertTrue(robot.getEnergyRequiredToReach(new Position(6,5)) == 0);
			assertTrue(robot.getEnergyRequiredToReach(new Position(6,8)) == 1600);
			assertTrue(robot.getEnergyRequiredToReach(new Position(9,2)) == 3200);
			assertTrue(robot.getEnergyRequiredToReach(new Position(9,5)) == 1700);
			assertTrue(robot.getEnergyRequiredToReach(new Position(9,8)) == 3200);
		}
		catch(IllegalPositionException e){

		} catch (OutOfDimensionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Test
	public void testMoveNextToSufficientEnergy() {
		try{
			testMoveNextTo(new Position(0, 0), Orientation.LEFT, new EnergyAmount(15000), new Position(3, 0), Orientation.LEFT, new EnergyAmount(15000));
			testMoveNextTo(new Position(0, 0), Orientation.LEFT, new EnergyAmount(15000), new Position(0, 3), Orientation.LEFT, new EnergyAmount(15000));
			testMoveNextTo(new Position(0, 0), Orientation.LEFT, new EnergyAmount(15000), new Position(3, 3), Orientation.LEFT, new EnergyAmount(15000));
			testMoveNextTo(new Position(0, 0), Orientation.LEFT, new EnergyAmount(15000), new Position(3, 3), Orientation.RIGHT, new EnergyAmount(15000));
			testMoveNextTo(new Position(0, 0), Orientation.LEFT, new EnergyAmount(15000), new Position(3, 3), Orientation.RIGHT, new EnergyAmount(15000));
			testMoveNextTo(new Position(0, 0), Orientation.DOWN, new EnergyAmount(15000), new Position(3, 0), Orientation.DOWN, new EnergyAmount(15000));
			testMoveNextTo(new Position(0, 0), Orientation.UP, new EnergyAmount(15000), new Position(3, 0), Orientation.UP, new EnergyAmount(15000));
			testMoveNextTo(new Position(0, 0), Orientation.LEFT, new EnergyAmount(15000), new Position(2, 0), Orientation.RIGHT, new EnergyAmount(15000));
			testMoveNextTo(new Position(1, 0), Orientation.DOWN, new EnergyAmount(15000), new Position(0, 2), Orientation.DOWN, new EnergyAmount(15000));
			testMoveNextTo(new Position(0, 0), Orientation.RIGHT, new EnergyAmount(15000), new Position(5, 5), Orientation.UP, new EnergyAmount(15000));
			testMoveNextTo(new Position(0, 0), Orientation.RIGHT, new EnergyAmount(15000), new Position(0, 1), Orientation.UP, new EnergyAmount(15000));
		}
		catch(OutOfDimensionException e){
			e.printStackTrace();
		}
	}

	public void testMoveNextTo(Position initialPositionRobot, Orientation initialOrientationRobot, EnergyAmount initialEnergyRobot, 
			Position initialPositionOther,Orientation initialOrientationOther, EnergyAmount initialEnergyOther){
		Robot robot;
		Robot other;

		Board board = null;
		try {
			board = new Board(50,50);
		} catch (IllegalDimensionException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		Position RobotAfter;
		Position OtherAfter;

		EnergyAmount EnergyRobotAfter;
		EnergyAmount EnergyOtherAfter;

		try {

			robot = new Robot(initialOrientationRobot, initialEnergyRobot);
			other = new Robot(initialOrientationOther, initialEnergyOther);
			robot.put(initialPositionRobot, board);
			other.put(initialPositionOther, board);

			robot.moveNextTo(other);
			assertTrue(Board.getDistanceBetween(robot, other) == 1);
			RobotAfter = robot.getPosition();
			OtherAfter = other.getPosition();

			EnergyRobotAfter = robot.getEnergy();
			EnergyOtherAfter = other.getEnergy();
			robot.terminate();
			other.terminate();

			robot = new Robot(initialOrientationRobot, initialEnergyRobot);
			other = new Robot(initialOrientationOther, initialEnergyOther);
			robot.put(initialPositionRobot, board);
			other.put(initialPositionOther, board);

			assertTrue(EnergyRobotAfter.equals(initialEnergyRobot.subtract(new EnergyAmount(robot.getEnergyRequiredToReach(RobotAfter)))));
			assertTrue(EnergyOtherAfter.equals(initialEnergyOther.subtract(new EnergyAmount(other.getEnergyRequiredToReach(OtherAfter)))));

		} 

		catch(IllegalPositionException e){
			e.printStackTrace();
		}
	}

	@Test
	public void testGetCarriedWeight(){
		try{
			Robot robot = new Robot(Orientation.UP, new EnergyAmount(15000));
			Board board = new Board(30,30);
			robot.put(new Position(5,5), board);

			Battery b1 = new Battery(new EnergyAmount(20), new WeightAmount(5));
			b1.put(new Position(5,5), board);
			Battery b2 = new Battery(new EnergyAmount(19), new WeightAmount(6));
			b2.put(new Position(5,5), board);
			Battery b3 = new Battery(new EnergyAmount(30), new WeightAmount(7));
			b3.put(new Position(5,5), board);
			Battery b4 = new Battery(new EnergyAmount(12), new WeightAmount(8));
			b4.put(new Position(5,5), board);
			robot.pickUp(b1);
			robot.pickUp(b2);
			robot.pickUp(b3);
			robot.pickUp(b4);

			assertTrue(robot.getCarriedWeight().getNumeral() == 26);

		} catch(Exception e){
			e.printStackTrace();
		}
	}

	@Test
	public void testPickUp(){
		try{
			Robot robot = new Robot(Orientation.UP, new EnergyAmount(15000));
			robot.put(new Position(5,5), largeBoard);

			Battery battery1 = new Battery(new EnergyAmount(20), new WeightAmount(5));
			battery1.put(new Position(5,5), largeBoard);
			Battery battery2 = new Battery(new EnergyAmount(19), new WeightAmount(6));
			battery2.put(new Position(5,5), largeBoard);
			Battery battery3 = new Battery(new EnergyAmount(30), new WeightAmount(7));
			battery3.put(new Position(5,5), largeBoard);
			Battery battery4 = new Battery(new EnergyAmount(12), new WeightAmount(8));
			battery4.put(new Position(5,5), largeBoard);
			if(robot.canHaveAsPossession(battery1) && robot.canHaveAsPossession(battery2) 
					&& robot.canHaveAsPossession(battery3) &&  robot.canHaveAsPossession(battery4)){
				robot.pickUp(battery1);
				robot.pickUp(battery2);
				robot.pickUp(battery3);
				robot.pickUp(battery4);
			}
			assertTrue(robot.hasAsPossession(battery1)
					&& robot.hasAsPossession(battery2)
					&& robot.hasAsPossession(battery3)
					&& robot.hasAsPossession(battery4));

			assertTrue(robot.getIthHeaviestPossession(4).getWeight().getNumeral() == 5);
			assertTrue(robot.getIthHeaviestPossession(3).getWeight().getNumeral() == 6);
			assertTrue(robot.getIthHeaviestPossession(2).getWeight().getNumeral() == 7);
			assertTrue(robot.getIthHeaviestPossession(1).getWeight().getNumeral() == 8);

		} catch(Exception e){
			e.printStackTrace();
		}
	}

	@Test
	public void testUse_Battery_Basic(){
		try{
			Robot robot = new Robot(Orientation.UP, new EnergyAmount(0));
			robot.put(new Position(5,5), largeBoard);
			Battery battery = new Battery(new EnergyAmount(2000), new WeightAmount(2000));
			battery.put(new Position(5,5), largeBoard);	
			robot.pickUp(battery);
			robot.use(battery);
			assertTrue(robot.getEnergy().equals(new EnergyAmount(2000)));
		} catch (OutOfDimensionException e){
			e.printStackTrace();
		} catch (IllegalPositionException e){
			e.printStackTrace();
		} 
	}

	@Test
	public void testUse_Battery_MaxReplenishable(){
		try{
			Robot robot = new Robot(Orientation.UP, new EnergyAmount(19000));
			robot.put(new Position(5,5), largeBoard);
			Battery battery = new Battery(new EnergyAmount(2000), new WeightAmount(2000));
			battery.put(new Position(5,5), largeBoard);	
			robot.pickUp(battery);
			robot.use(battery);
			assertTrue(robot.getEnergy().equals(new EnergyAmount(20000)));
			assertTrue(battery.getEnergy().equals(new EnergyAmount(1000)));
		} catch (OutOfDimensionException e){
			e.printStackTrace();
		} catch (IllegalPositionException e){
			e.printStackTrace();
		} 
	}

	@Test
	public void testUse_RepairKit_LowerThanMaxReplenishable(){
		try{
			Robot robot = new Robot(Orientation.UP, new EnergyAmount(6000));
			robot.setMaximumEnergy(new EnergyAmount(7000));
			robot.put(new Position(5,5), largeBoard);
			RepairKit repairKit = new RepairKit(new EnergyAmount(2000), new WeightAmount(0));
			repairKit.put(new Position(5,5), largeBoard);	
			robot.pickUp(repairKit);
			robot.use(repairKit);
			assertEquals(new EnergyAmount(8000), robot.getMaximumEnergy());
			assertTrue(repairKit.isTerminated());
		} catch (OutOfDimensionException e){
			e.printStackTrace();
		} catch (IllegalPositionException e){
			e.printStackTrace();
		}
	}

	@Test
	public void testUse_SurpriseBox_Battery(){
		try{
			Robot robot = new Robot(Orientation.UP, new EnergyAmount(6000));
			robot.put(new Position(5,5), largeBoard);
			SurpriseBox surpriseBox = new SurpriseBox(new WeightAmount(0), Surprise.BATTERY);
			surpriseBox.put(new Position(5,5), largeBoard);	
			robot.pickUp(surpriseBox);

			robot.use(surpriseBox);
			assertEquals(1, robot.getNbPossessions());

			assertTrue(surpriseBox.isTerminated());
		} catch (OutOfDimensionException e){
			e.printStackTrace();
		} catch (IllegalPositionException e){
			e.printStackTrace();
		}
	}

	@Test
	public void testUse_SurpriseBox_SURPRISEBOX(){
		try{
			Robot robot = new Robot(Orientation.UP, new EnergyAmount(6000));
			robot.put(new Position(5,5), largeBoard);
			SurpriseBox surpriseBox = new SurpriseBox(new WeightAmount(0), Surprise.SURPRISEBOX);
			surpriseBox.put(new Position(5,5), largeBoard);	
			robot.pickUp(surpriseBox);

			robot.use(surpriseBox);
			assertEquals(1, robot.getNbPossessions());

			assertTrue(surpriseBox.isTerminated());
		} catch (OutOfDimensionException e){
			e.printStackTrace();
		} catch (IllegalPositionException e){
			e.printStackTrace();
		}
	}

	@Test
	public void testUse_SurpriseBox_REPAIRKIT(){
		try{
			Robot robot = new Robot(Orientation.UP, new EnergyAmount(6000));
			robot.put(new Position(5,5), largeBoard);
			SurpriseBox surpriseBox = new SurpriseBox(new WeightAmount(0), Surprise.REPAIRKIT);
			surpriseBox.put(new Position(5,5), largeBoard);	
			robot.pickUp(surpriseBox);

			robot.use(surpriseBox);
			assertEquals(1, robot.getNbPossessions());

			assertTrue(surpriseBox.isTerminated());
		} catch (OutOfDimensionException e){
			e.printStackTrace();
		} catch (IllegalPositionException e){
			e.printStackTrace();
		}
	}

	@Test
	public void testUse_SurpriseBox_EXPLODE(){
		try{
			Robot robot = new Robot(Orientation.UP, new EnergyAmount(6000));
			robot.put(new Position(5,5), largeBoard);
			SurpriseBox surpriseBox = new SurpriseBox(new WeightAmount(0), Surprise.EXPLODE);
			surpriseBox.put(new Position(5,5), largeBoard);	
			robot.pickUp(surpriseBox);

			robot.use(surpriseBox);
			assertEquals(new EnergyAmount(16000), robot.getMaximumEnergy());

			assertTrue(surpriseBox.isTerminated());
		} catch (OutOfDimensionException e){
			e.printStackTrace();
		} catch (IllegalPositionException e){
			e.printStackTrace();
		}
	}

	@Test
	public void testUse_SurpriseBox_TELEPORT(){
		try{
			Robot robot = new Robot(Orientation.UP, new EnergyAmount(6000));
			robot.put(new Position(5,5), largeBoard);
			SurpriseBox surpriseBox = new SurpriseBox(new WeightAmount(0), Surprise.TELEPORTATION);
			surpriseBox.put(new Position(5,5), largeBoard);	
			robot.pickUp(surpriseBox);

			robot.use(surpriseBox);
			assertFalse(robot.getPosition().equals(new Position(5,5)));

			assertTrue(surpriseBox.isTerminated());
		} catch (OutOfDimensionException e){
			e.printStackTrace();
		} catch (IllegalPositionException e){
			e.printStackTrace();
		}
	}
	@Test
	public void testUse_RepairKit_HigherThanMaxReplenishable(){
		try{
			Robot robot = new Robot(Orientation.UP, new EnergyAmount(5000));
			robot.setMaximumEnergy(new EnergyAmount(18000));
			robot.put(new Position(5,5), largeBoard);
			RepairKit repairKit = new RepairKit(new EnergyAmount(10000), new WeightAmount(0));
			repairKit.put(new Position(5,5), largeBoard);	
			robot.pickUp(repairKit);
			robot.use(repairKit);
			assertEquals(new EnergyAmount(20000), robot.getMaximumEnergy());
		} catch (OutOfDimensionException e){
			e.printStackTrace();
		} catch (IllegalPositionException e){
			e.printStackTrace();
		} 
	}

	@Test
	public void testDrop(){
		try{
			Robot robot = new Robot(Orientation.UP, new EnergyAmount(0));
			robot.put(new Position(5,5), largeBoard);
			Battery battery = new Battery(new EnergyAmount(2000), new WeightAmount(2000));
			battery.put(new Position(5,5), largeBoard);	
			robot.pickUp(battery);
			assertEquals(new WeightAmount(2000), robot.getCarriedWeight());
			assertTrue(largeBoard.canHaveAsEntity(battery));
			assertTrue(largeBoard.isInsideDimensions(robot.getPosition()));
			assertTrue(largeBoard.getNbWallsOnPosition(new Position(5,5)) == 0);
			assertTrue(battery.canBePutOn(largeBoard, robot.getPosition()));
			robot.drop(battery);

		} catch (OutOfDimensionException e){
			e.printStackTrace();
		} catch (IllegalPositionException e){
			e.printStackTrace();
		}
	}

	@Test
	public void testShoot(){
		try{
			Robot robot1 = new Robot(Orientation.RIGHT, new EnergyAmount(5500));
			robot1.put(new Position(5,5), largeBoard);
			Robot robot2 = new Robot(Orientation.UP, new EnergyAmount(2500));
			robot2.put(new Position(8,5), largeBoard);
			robot1.shoot();
			assertTrue(robot2.getMaximumEnergy().equals(new EnergyAmount(16000)));
			robot1.shoot();
			robot1.shoot();
			robot1.shoot();
			robot1.shoot();
			assertTrue(robot2.isTerminated());

		} catch (OutOfDimensionException e){
			e.printStackTrace();
		} catch (IllegalPositionException e){
			e.printStackTrace();
		}
	}

	@Test
	public void testShootSurprise(){
		try{
			Robot robot = new Robot(Orientation.RIGHT, new EnergyAmount(5500));
			robot.put(new Position(0,4), largeBoard);
			new SurpriseBox(new WeightAmount(0)).put(new Position(4,4), largeBoard);
			new SurpriseBox(new WeightAmount(0)).put(new Position(5,4), largeBoard);
			new SurpriseBox(new WeightAmount(0)).put(new Position(5,5), largeBoard);
			new SurpriseBox(new WeightAmount(0)).put(new Position(6,5), largeBoard);
			new SurpriseBox(new WeightAmount(0)).put(new Position(6,6), largeBoard);
			new SurpriseBox(new WeightAmount(0)).put(new Position(6,7), largeBoard);
			new SurpriseBox(new WeightAmount(0)).put(new Position(7,7), largeBoard);
			SurpriseBox surpriseBox = new SurpriseBox(new WeightAmount(0));
			surpriseBox.put(new Position(8,7), largeBoard);

			robot.shoot();
			assertTrue(surpriseBox.isTerminated());

		} catch (OutOfDimensionException e){
			e.printStackTrace();
		} catch (IllegalPositionException e){
			e.printStackTrace();
		} 
	}

	@Test
	public void testTeleport(){
		Robot robot = new Robot(Orientation.RIGHT, new EnergyAmount(5500));
		try {
			robot.put(new Position(4,4), smallBoard);
		} catch (IllegalPositionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (OutOfDimensionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Position oldPosition;
		for(int i = 0; i < 10; i++){
			oldPosition = robot.getPosition();
			robot.teleport();
			assertFalse(robot.getPosition() == oldPosition);
		}
	}

	@Test
	public void testTeleport_Impossible(){
		Robot robot = new Robot(Orientation.RIGHT, new EnergyAmount(5500));
		try {
			Board verySmallBoard = new Board(0,0);
			robot.put(new Position(0,0), verySmallBoard);
			robot.teleport();
			assertEquals(new Position(0,0), robot.getPosition());

		} catch (IllegalDimensionException e1) {
			e1.printStackTrace();
		} catch (OutOfDimensionException e) {
			e.printStackTrace();
		} catch (IllegalPositionException e) {
			e.printStackTrace();
		}
	}
	@Test
	public void testGetIthHeaviestPossesion(){
		try{
			Robot robot = new Robot(Orientation.UP, new EnergyAmount(2500));
			robot.put(new Position(5,5), largeBoard);

			Battery battery1 = new Battery(new EnergyAmount(2000), new WeightAmount(1000));
			battery1.put(new Position(5,5), largeBoard);
			robot.pickUp(battery1);

			Battery battery2 = new Battery(new EnergyAmount(2000), new WeightAmount(2000));
			battery2.put(new Position(5,5), largeBoard);
			robot.pickUp(battery2);

			Battery battery3 = new Battery(new EnergyAmount(2000), new WeightAmount(3000));
			battery3.put(new Position(5,5), largeBoard);
			robot.pickUp(battery3);

			Battery battery4 = new Battery(new EnergyAmount(2000), new WeightAmount(4000));
			battery4.put(new Position(5,5), largeBoard);
			robot.pickUp(battery4);

			Battery battery5 = new Battery(new EnergyAmount(2000), new WeightAmount(5000));
			battery5.put(new Position(5,5), largeBoard);
			robot.pickUp(battery5);

			assertTrue(robot.getIthHeaviestPossession(1).equals(battery5));
			assertTrue(robot.getIthHeaviestPossession(2).equals(battery4));
			assertTrue(robot.getIthHeaviestPossession(3).equals(battery3));
			assertTrue(robot.getIthHeaviestPossession(4).equals(battery2));
			assertTrue(robot.getIthHeaviestPossession(5).equals(battery1));

		} catch (OutOfDimensionException e){
			e.printStackTrace();
		} catch (IllegalPositionException e){
			e.printStackTrace();
		}
	}

	@Test
	public void testProgram_COWBOY(){
		Robot robot = new Robot(Orientation.UP, new EnergyAmount(300));
		try {
			robot.put(new Position(3,3), largeBoard);
		} catch (IllegalPositionException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (OutOfDimensionException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {
			robot.loadProgram("cowboy.prog");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnparsableException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		robot.stepN(1);

		assertEquals(Orientation.RIGHT, robot.getOrientation());
	}

	@Test
	public void testProgram_Creepy(){
		Robot robot = new Robot(Orientation.UP, new EnergyAmount(20000));
		Robot robot2 = new Robot(Orientation.UP, new EnergyAmount(20000));
		try {
			robot.put(new Position(3,3), largeBoard);
			robot2.put(new Position(3,4), largeBoard);
		} catch (IllegalPositionException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (OutOfDimensionException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		try {
			robot.loadProgram("cowboy.prog");
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (UnparsableException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		try {
			robot2.loadProgram("example.prog");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnparsableException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		robot.stepN(1000);
		if(robot2.hasProgram())
			robot2.stepN(1000);
		
		assertTrue(robot2.isTerminated());

	}
}
