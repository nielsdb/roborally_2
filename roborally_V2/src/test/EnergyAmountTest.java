package test;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import roborally.value.EnergyAmount;
import roborally.value.EnergyUnit;

public class EnergyAmountTest {
	@Test
	public void testConversion(){
		assertEquals(new EnergyAmount(5, EnergyUnit.J), new EnergyAmount(5).toUnit(EnergyUnit.J));
		assertEquals(new EnergyAmount(5, EnergyUnit.Ws), new EnergyAmount(5).toUnit(EnergyUnit.Ws));
		assertEquals(new EnergyAmount(.005, EnergyUnit.kJ), new EnergyAmount(5).toUnit(EnergyUnit.kJ));
		
		
		assertEquals(new EnergyAmount(5, EnergyUnit.kJ), new EnergyAmount(5, EnergyUnit.kJ).toUnit(EnergyUnit.kJ));
		assertEquals(new EnergyAmount(5000, EnergyUnit.J), new EnergyAmount(5, EnergyUnit.kJ).toUnit(EnergyUnit.J));
		assertEquals(new EnergyAmount(5000, EnergyUnit.Ws), new EnergyAmount(5, EnergyUnit.kJ).toUnit(EnergyUnit.Ws));

	}
}
