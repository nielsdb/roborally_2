package test;

import static org.junit.Assert.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import org.junit.Test;

import roborally.entity.Entity;
import roborally.entity.Robot;
import roborally.entity.Wall;
import roborally.entity.item.Battery;
import roborally.entity.item.RepairKit;
import roborally.entity.item.SurpriseBox;
import roborally.exception.IllegalDimensionException;
import roborally.exception.IllegalPositionException;
import roborally.exception.OutOfDimensionException;
import roborally.filter.EnergyFilter;
import roborally.filter.EnergyFilterArguments;
import roborally.filter.PositionFilter;
import roborally.position.Board;
import roborally.position.Orientation;
import roborally.position.Position;
import roborally.value.EnergyAmount;
import roborally.value.WeightAmount;

public class BoardTest {
	
	
	@Test
	public void testIsValidDimension(){
		Board board;
		try {
			board = new Board(30,30);
			assertFalse(Board.isValidDimension(-4, 3));
			assertFalse(Board.isValidDimension(2, -3));
			assertTrue(Board.isValidDimension(3, 3));
		} catch (IllegalDimensionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Test
	public void testCanHaveAsEntity()
	{
		Board board;
		try {
			board = new Board(30,30);
			assertFalse(board.canHaveAsEntity(null));
			Wall wall = new Wall();
			wall.terminate();
			assertFalse(board.canHaveAsEntity(wall));
			board.terminate();
			assertTrue(board.canHaveAsEntity(null));
		} catch (IllegalDimensionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Test
	public void testPlacementOfEntities(){
		try{
		Board board = new Board(3,3);
		Wall wall = new Wall();
		wall.put(new Position(3,3), board);
		assertTrue(board.hasAsEntityOnPosition(wall, new Position(3,3)));
		}
		catch (IllegalPositionException e) {
			System.out.println(e.toString());
		} catch (IllegalDimensionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (OutOfDimensionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
	
	
	
	@Test
	public void testGetHeight() {
		try{
			Board board = new Board(30,30);
			assertTrue(board.getHeight() == 30);
		} catch (IllegalDimensionException e) {
			// TODO Auto-generated catch block
		}
	}

	@Test
	public void testGetWidth() {
		try{
			Board board = new Board(30,30);
			assertTrue(board.getWidth() == 30);
		} catch (IllegalDimensionException e) {
			// TODO Auto-generated catch block
		}
	}

	@Test
	public void testGetNbEntitiesOnPosition() {
		try{
			Board board = new Board(30,30);
			try{
				new Battery(new EnergyAmount(3), new WeightAmount(3)).put(new Position(3,3), board);
				new RepairKit(new EnergyAmount(3), new WeightAmount(3)).put(new Position(3,3), board);
				new SurpriseBox(new WeightAmount(3)).put(new Position(3,3), board);
				new Robot(Orientation.UP, Robot.getMaximumMaximumEnergy()).put(new Position(3,3), board);
				new Wall().put(new Position(4,3), board);
			}
			catch(IllegalPositionException e1){
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (OutOfDimensionException e2) {
				// TODO Auto-generated catch block
				e2.printStackTrace();
			}

			try {
				assertEquals(1, board.getNbRepairKitsOnPosition(new Position(3,3)));
				assertEquals(1, board.getNbBatteriesOnPosition(new Position(3,3)));
				assertEquals(1, board.getNbSurpriseBoxesOnPosition(new Position(3,3)));
				assertEquals(4, board.getNbEntitiesOnPosition(new Position(3,3)));
				assertEquals(1, board.getNbWallsOnPosition(new Position(4,3)));
				assertEquals(1, board.getNbRobotsOnPosition(new Position(3,3)));
				assertEquals(3, board.getNbItemsOnPosition(new Position(3,3)));

			} catch (OutOfDimensionException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} catch (IllegalDimensionException e) {
			// TODO Auto-generated catch block
		}
	}

	@Test
	public void isValidDimension_Valid(){
		try{
			Board board = new Board(30,30);
			assertTrue(board.isValidDimension(board.getHeight(),board.getWidth()) == true);
		} catch (IllegalDimensionException e) {
			// TODO Auto-generated catch block
		}
	}

	@Test
	public void isValidDimension_Invalid(){
		try{
			Board board = new Board(-1,-1);
			assertTrue(board.isValidDimension(board.getHeight(),board.getWidth()) == false);
		} catch (IllegalDimensionException e) {
			// TODO Auto-generated catch block
		}
	}

	@Test
	public void testGetBounds(){
		try{
			Board board = new Board(30,30);

			new Wall().put(new Position(3, 3), board);
			new Wall().put(new Position(4, 3), board);
			new Wall().put(new Position(6, 6), board);
			new Wall().put(new Position(4, 5), board);
			assertTrue(board.getBoundsOfPositionsInUse()[0].equals(new Position(2,2)));
			assertTrue(board.getBoundsOfPositionsInUse()[1].equals(new Position(7,7)));
			new Wall().put(new Position(4, 8), board);
			assertTrue(board.getBoundsOfPositionsInUse()[0].equals(new Position(2,2)));
			assertTrue(board.getBoundsOfPositionsInUse()[1].equals(new Position(7,9)));
		}
		catch(IllegalPositionException e){
			e.printStackTrace();
		} catch (OutOfDimensionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalDimensionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Test
	public void testMerge(){
		try {
			Board board = new Board(20,20);
			Board other = new Board(30,30);
			new Wall().put(new Position(0,0), board);
			new Wall().put(new Position(1,1), board);
			new Wall().put(new Position(2,2), board);
			new Wall().put(new Position(3,3), board);
			new Wall().put(new Position(4,4), board);
			new Wall().put(new Position(5,5), board);
			new Wall().put(new Position(0,0), other);
			new Wall().put(new Position(1,1), other);
			new Wall().put(new Position(2,2), other);
			new Wall().put(new Position(3,3), other);
			new Wall().put(new Position(4,4), other);
			Wall getMoved = new Wall();
			getMoved.put(new Position(2,3), other);
			new Wall().put(new Position(25,25), other);
			board.merge(other);
			assertTrue(other.isTerminated());
			assertTrue(board.isPositionInUse(new Position(25,25)) == false);
			assertTrue(board.getNbWallsOnPosition(new Position(0,0)) == 1);
			assertTrue(board.getNbWallsOnPosition(new Position(2,3)) == 1);

		} catch (IllegalDimensionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalPositionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (OutOfDimensionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@Test
	public void testIterator(){
		try {
			Board board = new Board(20,20);

			Entity e1 = new Wall();
			e1.put(new Position(0,0), board);
			Entity e2 = new Wall();
			e2.put(new Position(1,1), board);
			Entity e3 = new Wall();
			e3.put(new Position(2,2), board);
			Entity e4 = new Wall();
			e4.put(new Position(3,3), board);
			Entity e5 = new Wall();
			e5.put(new Position(4,4), board);
			Entity e6 = new Wall();
			e6.put(new Position(5,5), board);
			Entity e7 = new Battery(new EnergyAmount(8), new WeightAmount(3));
			e7.put(new Position(4,5), board);
			Entity e8 = new Battery(new EnergyAmount(7), new WeightAmount(6));
			e8.put(new Position(1,5), board);
			Entity e9 = new Battery(new EnergyAmount(6), new WeightAmount(4));
			e9.put(new Position(3,5), board);
			Entity e10 = new Battery(new EnergyAmount(5), new WeightAmount(8));
			e10.put(new Position(6,5), board);
			Entity e11 = new Battery(new EnergyAmount(4), new WeightAmount(2));
			e11.put(new Position(7,5), board);
			Entity e12 = new Robot(Orientation.UP, new EnergyAmount(3));
			e12.put(new Position(1,2), board);

			Set<Entity> fullBoard = new HashSet<Entity>();
			Set<Entity> expected = new HashSet<Entity>();
			expected.add(e1);expected.add(e2);expected.add(e3);expected.add(e4);expected.add(e5);expected.add(e6);expected.add(e7);expected.add(e8);expected.add(e9);
			expected.add(e10);expected.add(e11);expected.add(e12);
			Iterator<Entity> it = board.iterator(new PositionFilter(board));
			while(it.hasNext()){
				fullBoard.add(it.next());
			}
			assertEquals(expected, fullBoard);

			expected.clear();
			expected.add(e3);expected.add(e4);expected.add(e5);expected.add(e6);expected.add(e7);expected.add(e9);
			Set<Entity> subSection = new HashSet<Entity>();
			Iterator<Entity> it2 = board.iterator(new PositionFilter(new Position(2, 2), new Position(5, 5)));
			while(it2.hasNext()){
				subSection.add(it2.next());
			}
			assertEquals(expected, subSection);

			expected.clear();
			expected.add(e11);expected.add(e12);
			Set<Entity> lowerThanFive = new HashSet<Entity>();
			Iterator<Entity> it3 = board.iterator(new EnergyFilter(EnergyFilterArguments.LOWER, new EnergyAmount(5)));
			while(it3.hasNext()){
				lowerThanFive.add(it3.next());
			}
			assertEquals(expected, lowerThanFive);
			
			expected.clear();
			expected.add(e10);
			Set<Entity> equalToFive = new HashSet<Entity>();
			Iterator<Entity> it4 = board.iterator(new EnergyFilter(EnergyFilterArguments.EQUAL, new EnergyAmount(5)));
			while(it4.hasNext()){
				equalToFive.add(it4.next());
			}
			assertEquals(expected, equalToFive);


			expected.clear();
			expected.add(e7);
			expected.add(e8);
			expected.add(e9);
			Set<Entity> higherThanFive = new HashSet<Entity>();
			Iterator<Entity> it5 = board.iterator(new EnergyFilter(EnergyFilterArguments.HIGHER, new EnergyAmount(5)));
			while(it5.hasNext()){
				higherThanFive.add(it5.next());
			}
			assertEquals(expected, higherThanFive);

		} catch (IllegalDimensionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalPositionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (OutOfDimensionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@Test
	public void testGetDistanceBetween(){
		Board board = null;
		try {
			board = new Board(20,20);
		} catch (IllegalDimensionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Wall entity1 = new Wall();
		Wall entity2 = new Wall();
		try {
			entity1.put(new Position(2,2), board);
			entity2.put(new Position(4,3), board);
		} catch (IllegalPositionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (OutOfDimensionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		assertEquals(3, Board.getDistanceBetween(entity1, entity2));
	}

	public void testGetAllEntitiesOfTypeOnPosition(){
		try{
			Board board = new Board(30,30);
			try{
				new Battery(new EnergyAmount(3), new WeightAmount(3)).put(new Position(3,3), board);
				new RepairKit(new EnergyAmount(3), new WeightAmount(3)).put(new Position(3,3), board);
				new SurpriseBox(new WeightAmount(3)).put(new Position(3,3), board);
				new Robot(Orientation.UP, Robot.getMaximumMaximumEnergy()).put(new Position(3,3), board);
				new Wall().put(new Position(4,3), board);
			}
			catch(IllegalPositionException e1){
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (OutOfDimensionException e2) {
				// TODO Auto-generated catch block
				e2.printStackTrace();
			}

			assertEquals(1, board.getAllEntitiesOfType(Battery.class).size());
		} catch (IllegalDimensionException e) {
			// TODO Auto-generated catch block
		}
	}
}